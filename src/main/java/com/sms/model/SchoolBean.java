package com.sms.model;

/**
 * Created by Steve on 11/20/2016.
 */
public class SchoolBean {
		private String name;
		private String initials;
		private String location;
		private String address;
		private int capacity;
		private String logo;
		private String motto;
		private String vision;
		private String uuid;

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getInitials() {
				return initials;
		}

		public void setInitials(String initials) {
				this.initials = initials;
		}

		public String getLocation() {
				return location;
		}

		public void setLocation(String location) {
				this.location = location;
		}

		public String getAddress() {
				return address;
		}

		public void setAddress(String address) {
				this.address = address;
		}

		public int getCapacity() {
				return capacity;
		}

		public void setCapacity(int capacity) {
				this.capacity = capacity;
		}

		public String getLogo() {
				return logo;
		}

		public void setLogo(String logo) {
				this.logo = logo;
		}

		public String getMotto() {
				return motto;
		}

		public void setMotto(String motto) {
				this.motto = motto;
		}

		public String getVision() {
				return vision;
		}

		public void setVision(String vision) {
				this.vision = vision;
		}

		public String getUuid() {
				return uuid;
		}

		public void setUuid(String uuid) {
				this.uuid = uuid;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				SchoolBean that = (SchoolBean) o;

				if (capacity != that.capacity) return false;
				if (name != null ? !name.equals(that.name) : that.name != null) return false;
				if (initials != null ? !initials.equals(that.initials) : that.initials != null) return false;
				if (location != null ? !location.equals(that.location) : that.location != null) return false;
				if (address != null ? !address.equals(that.address) : that.address != null) return false;
				if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
				if (motto != null ? !motto.equals(that.motto) : that.motto != null) return false;
				if (vision != null ? !vision.equals(that.vision) : that.vision != null) return false;
				if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = name != null ? name.hashCode() : 0;
				result = 31 * result + (initials != null ? initials.hashCode() : 0);
				result = 31 * result + (location != null ? location.hashCode() : 0);
				result = 31 * result + (address != null ? address.hashCode() : 0);
				result = 31 * result + capacity;
				result = 31 * result + (logo != null ? logo.hashCode() : 0);
				result = 31 * result + (motto != null ? motto.hashCode() : 0);
				result = 31 * result + (vision != null ? vision.hashCode() : 0);
				result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
				return result;
		}
}
