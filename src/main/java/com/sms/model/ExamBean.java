package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class ExamBean {
		private int examId;
		private int termId;
		private String name;
		private String description;
		private Timestamp dateCreated;
		private int createdBy;
		private Date wef;
		private Date wet;
		private String status;
		private Collection<ExamResultBean> examresultsesByExamId;
		private TermBean termsByTermId;
		private UserBean usersByCreatedBy;
		private Collection<ExamSettingsBean> examsettingsesByExamId;
		private Collection<GradeScaleBean> gradescalesByExamId;

		public int getExamId() {
				return examId;
		}

		public void setExamId(int examId) {
				this.examId = examId;
		}

		public int getTermId() {
				return termId;
		}

		public void setTermId(int termId) {
				this.termId = termId;
		}

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getDescription() {
				return description;
		}

		public void setDescription(String description) {
				this.description = description;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ExamBean examBean = (ExamBean) o;

				if (examId != examBean.examId) return false;
				if (termId != examBean.termId) return false;
				if (createdBy != examBean.createdBy) return false;
				if (name != null ? !name.equals(examBean.name) : examBean.name != null) return false;
				if (description != null ? !description.equals(examBean.description) : examBean.description != null)
						return false;
				if (dateCreated != null ? !dateCreated.equals(examBean.dateCreated) : examBean.dateCreated != null)
						return false;
				if (wef != null ? !wef.equals(examBean.wef) : examBean.wef != null) return false;
				if (wet != null ? !wet.equals(examBean.wet) : examBean.wet != null) return false;
				if (status != null ? !status.equals(examBean.status) : examBean.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = examId;
				result = 31 * result + termId;
				result = 31 * result + (name != null ? name.hashCode() : 0);
				result = 31 * result + (description != null ? description.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public Collection<ExamResultBean> getExamresultsesByExamId() {
				return examresultsesByExamId;
		}

		public void setExamresultsesByExamId(Collection<ExamResultBean> examresultsesByExamId) {
				this.examresultsesByExamId = examresultsesByExamId;
		}

		public TermBean getTermsByTermId() {
				return termsByTermId;
		}

		public void setTermsByTermId(TermBean termsByTermId) {
				this.termsByTermId = termsByTermId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public Collection<ExamSettingsBean> getExamsettingsesByExamId() {
				return examsettingsesByExamId;
		}

		public void setExamsettingsesByExamId(Collection<ExamSettingsBean> examsettingsesByExamId) {
				this.examsettingsesByExamId = examsettingsesByExamId;
		}

		public Collection<GradeScaleBean> getGradescalesByExamId() {
				return gradescalesByExamId;
		}

		public void setGradescalesByExamId(Collection<GradeScaleBean> gradescalesByExamId) {
				this.gradescalesByExamId = gradescalesByExamId;
		}
}
