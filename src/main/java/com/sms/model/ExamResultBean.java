package com.sms.model;

import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class ExamResultBean {
		private int id;
		private int examId;
		private int classId;
		private int studentId;
		private int subjectId;
		private int mark;
		private Timestamp dateCreated;
		private int createdBy;
		private String status;
		private String remarks;
		private ExamBean examsByExamId;
		private ClassBean classesByClassId;
		private StudentBean studentByStudentId;
		private SubjectBean subjectBySubjectId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getExamId() {
				return examId;
		}

		public void setExamId(int examId) {
				this.examId = examId;
		}

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getStudentId() {
				return studentId;
		}

		public void setStudentId(int studentId) {
				this.studentId = studentId;
		}

		public int getSubjectId() {
				return subjectId;
		}

		public void setSubjectId(int subjectId) {
				this.subjectId = subjectId;
		}

		public int getMark() {
				return mark;
		}

		public void setMark(int mark) {
				this.mark = mark;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public String getRemarks() {
				return remarks;
		}

		public void setRemarks(String remarks) {
				this.remarks = remarks;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ExamResultBean that = (ExamResultBean) o;

				if (id != that.id) return false;
				if (examId != that.examId) return false;
				if (classId != that.classId) return false;
				if (studentId != that.studentId) return false;
				if (subjectId != that.subjectId) return false;
				if (mark != that.mark) return false;
				if (createdBy != that.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (remarks != null ? !remarks.equals(that.remarks) : that.remarks != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + examId;
				result = 31 * result + classId;
				result = 31 * result + studentId;
				result = 31 * result + subjectId;
				result = 31 * result + mark;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
				return result;
		}

		public ExamBean getExamsByExamId() {
				return examsByExamId;
		}

		public void setExamsByExamId(ExamBean examsByExamId) {
				this.examsByExamId = examsByExamId;
		}

		public ClassBean getClassesByClassId() {
				return classesByClassId;
		}

		public void setClassesByClassId(ClassBean classesByClassId) {
				this.classesByClassId = classesByClassId;
		}

		public StudentBean getStudentByStudentId() {
				return studentByStudentId;
		}

		public void setStudentByStudentId(StudentBean studentByStudentId) {
				this.studentByStudentId = studentByStudentId;
		}

		public SubjectBean getSubjectBySubjectId() {
				return subjectBySubjectId;
		}

		public void setSubjectBySubjectId(SubjectBean subjectBySubjectId) {
				this.subjectBySubjectId = subjectBySubjectId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
