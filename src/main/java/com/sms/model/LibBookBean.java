package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class LibBookBean {
		private int id;
		private Integer isbnNo;
		private String title;
		private Integer edition;
		private Integer bookNo;
		private String author;
		private String status;
		private String publisher;
		private int categoryId;
		private Integer noOfCopies;
		private int shelfNo;
		private int position;
		private Integer cost;
		private String language;
		private String condition;
		private Timestamp dateCreated;
		private int createdBy;


		private Date wef;
		private Date wet;
		private LibCategoryBean libCategoryByCategoryId;
		private Collection<LibIssuedBookBean> libIssuedBooksesByBookId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public Integer getIsbnNo() {
				return isbnNo;
		}

		public void setIsbnNo(Integer isbnNo) {
				this.isbnNo = isbnNo;
		}

		public String getTitle() {
				return title;
		}

		public void setTitle(String title) {
				this.title = title;
		}

		public Integer getEdition() {
				return edition;
		}

		public void setEdition(Integer edition) {
				this.edition = edition;
		}

		public Integer getBookNo() {
				return bookNo;
		}

		public void setBookNo(Integer bookNo) {
				this.bookNo = bookNo;
		}

		public String getAuthor() {
				return author;
		}

		public void setAuthor(String author) {
				this.author = author;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public String getPublisher() {
				return publisher;
		}

		public void setPublisher(String publisher) {
				this.publisher = publisher;
		}

		public int getCategoryId() {
				return categoryId;
		}

		public void setCategoryId(int categoryId) {
				this.categoryId = categoryId;
		}

		public Integer getNoOfCopies() {
				return noOfCopies;
		}

		public void setNoOfCopies(Integer noOfCopies) {
				this.noOfCopies = noOfCopies;
		}

		public int getShelfNo() {
				return shelfNo;
		}

		public void setShelfNo(int shelfNo) {
				this.shelfNo = shelfNo;
		}

		public int getPosition() {
				return position;
		}

		public void setPosition(int position) {
				this.position = position;
		}

		public Integer getCost() {
				return cost;
		}

		public void setCost(Integer cost) {
				this.cost = cost;
		}

		public String getLanguage() {
				return language;
		}

		public void setLanguage(String language) {
				this.language = language;
		}

		public String getCondition() {
				return condition;
		}

		public void setCondition(String condition) {
				this.condition = condition;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				LibBookBean that = (LibBookBean) o;

				if (id != that.id) return false;
				if (categoryId != that.categoryId) return false;
				if (shelfNo != that.shelfNo) return false;
				if (position != that.position) return false;
				if (createdBy != that.createdBy) return false;
				if (isbnNo != null ? !isbnNo.equals(that.isbnNo) : that.isbnNo != null) return false;
				if (title != null ? !title.equals(that.title) : that.title != null) return false;
				if (edition != null ? !edition.equals(that.edition) : that.edition != null) return false;
				if (bookNo != null ? !bookNo.equals(that.bookNo) : that.bookNo != null) return false;
				if (author != null ? !author.equals(that.author) : that.author != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (publisher != null ? !publisher.equals(that.publisher) : that.publisher != null) return false;
				if (noOfCopies != null ? !noOfCopies.equals(that.noOfCopies) : that.noOfCopies != null) return false;
				if (cost != null ? !cost.equals(that.cost) : that.cost != null) return false;
				if (language != null ? !language.equals(that.language) : that.language != null) return false;
				if (condition != null ? !condition.equals(that.condition) : that.condition != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (isbnNo != null ? isbnNo.hashCode() : 0);
				result = 31 * result + (title != null ? title.hashCode() : 0);
				result = 31 * result + (edition != null ? edition.hashCode() : 0);
				result = 31 * result + (bookNo != null ? bookNo.hashCode() : 0);
				result = 31 * result + (author != null ? author.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
				result = 31 * result + categoryId;
				result = 31 * result + (noOfCopies != null ? noOfCopies.hashCode() : 0);
				result = 31 * result + shelfNo;
				result = 31 * result + position;
				result = 31 * result + (cost != null ? cost.hashCode() : 0);
				result = 31 * result + (language != null ? language.hashCode() : 0);
				result = 31 * result + (condition != null ? condition.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				return result;
		}

		public LibCategoryBean getLibCategoryByCategoryId() {
				return libCategoryByCategoryId;
		}

		public void setLibCategoryByCategoryId(LibCategoryBean libCategoryByCategoryId) {
				this.libCategoryByCategoryId = libCategoryByCategoryId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public Collection<LibIssuedBookBean> getLibIssuedBooksesByBookId() {
				return libIssuedBooksesByBookId;
		}

		public void setLibIssuedBooksesByBookId(Collection<LibIssuedBookBean> libIssuedBooksesByBookId) {
				this.libIssuedBooksesByBookId = libIssuedBooksesByBookId;
		}
}
