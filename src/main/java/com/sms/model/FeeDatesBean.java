package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeeDatesBean {
		private int id;
		private int subCategoryId;
		private Date startDate;
		private Date dueDate;
		private Date endDate;
		private int createdBy;
		private Timestamp dateCreated;
		private Date wef;
		private Date wet;
		private String status;
		private FeeSubcategoryBean feessubcategoryBySubCategoryId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getSubCategoryId() {
				return subCategoryId;
		}

		public void setSubCategoryId(int subCategoryId) {
				this.subCategoryId = subCategoryId;
		}

		public Date getStartDate() {
				return startDate;
		}

		public void setStartDate(Date startDate) {
				this.startDate = startDate;
		}

		public Date getDueDate() {
				return dueDate;
		}

		public void setDueDate(Date dueDate) {
				this.dueDate = dueDate;
		}

		public Date getEndDate() {
				return endDate;
		}

		public void setEndDate(Date endDate) {
				this.endDate = endDate;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeeDatesBean that = (FeeDatesBean) o;

				if (id != that.id) return false;
				if (subCategoryId != that.subCategoryId) return false;
				if (createdBy != that.createdBy) return false;
				if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
				if (dueDate != null ? !dueDate.equals(that.dueDate) : that.dueDate != null) return false;
				if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + subCategoryId;
				result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
				result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
				result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public FeeSubcategoryBean getFeessubcategoryBySubCategoryId() {
				return feessubcategoryBySubCategoryId;
		}

		public void setFeessubcategoryBySubCategoryId(FeeSubcategoryBean feessubcategoryBySubCategoryId) {
				this.feessubcategoryBySubCategoryId = feessubcategoryBySubCategoryId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
