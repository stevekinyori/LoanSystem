package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Steve on 11/20/2016.
 */
public class UserBean {
		private int id;
		private String password;
		private String secretQuestion;
		private String secretAnswer;
		private String username;
		private int roleId;
		private Date dateCreated;
		@JsonIgnore
		private int createdBy;
		@JsonIgnore
		private Collection<AcademicYearBean> academicyearsesById;
		@JsonIgnore
		private Collection<ClassBean> classesById;
		@JsonIgnore
		private Collection<ClassBean> classesById_0;
		@JsonIgnore
		private Collection<ClassHierarchyBean> classhiearchiesById;
		@JsonIgnore
		private Collection<ClassSubjectBean> classsubjectsById;
		@JsonIgnore
		private Collection<ClassSubjectBean> classsubjectsById_0;
		@JsonIgnore
		private Collection<ExamResultBean> examresultsesById;
		@JsonIgnore
		private Collection<ExamBean> examsesById;
		@JsonIgnore
		private Collection<ExamSettingsBean> examsettingsesById;
		@JsonIgnore
		private Collection<FeeAllocationBean> feeallocationsById;
		@JsonIgnore
		private Collection<FeeApplicationLevelBean> feeapplicationlevelsesById;
		@JsonIgnore
		private Collection<FeeDatesBean> feedatesById;
		@JsonIgnore
		private Collection<FeesCategoryBean> feescategoriesById;
		@JsonIgnore
		private Collection<FeeSubcategoryBean> feessubcategoriesById;
		@JsonIgnore
		private Collection<FeeWaiverBean> feewaiversesById;
		@JsonIgnore
		private Collection<GradeScaleBean> gradescalesById;
		@JsonIgnore
		private Collection<GuardianBean> guardienById;
		@JsonIgnore
		private Collection<GuardianBean> guardienById_0;
		@JsonIgnore
		private Collection<LibBookBean> libBooksesById;
		@JsonIgnore
		private Collection<LibCategoryBean> libCategoriesById;
		@JsonIgnore
		private Collection<LibIssuedBookBean> libIssuedBooksesById;
		@JsonIgnore
		private Collection<LibIssuedBookBean> libIssuedBooksesById_0;
		@JsonIgnore
		private Collection<ParentBean> parentsById;
		@JsonIgnore
		private Collection<ParentBean> parentsById_0;
		@JsonIgnore
		private Collection<StudentBean> studentsById;
		@JsonIgnore
		private Collection<StudentBean> studentsById_0;
		@JsonIgnore
		private Collection<SubjectBean> subjectsById;
		@JsonIgnore
		private Collection<SubjectBean> subjectsById_0;
		@JsonIgnore
		private Collection<TeacherBean> teachersesById;
		@JsonIgnore
		private Collection<TeacherBean> teachersesById_0;
		@JsonIgnore
		private Collection<TermBean> termsesById;
		private UserRoleBean userRolesByRoleId;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public String getPassword() {
				return password;
		}

		public void setPassword(String password) {
				this.password = password;
		}

		public String getSecretQuestion() {
				return secretQuestion;
		}

		public void setSecretQuestion(String secretQuestion) {
				this.secretQuestion = secretQuestion;
		}

		public String getSecretAnswer() {
				return secretAnswer;
		}

		public void setSecretAnswer(String secretAnswer) {
				this.secretAnswer = secretAnswer;
		}

		public String getUsername() {
				return username;
		}

		public void setUsername(String username) {
				this.username = username;
		}

		public int getRoleId() {
				return roleId;
		}

		public void setRoleId(int roleId) {
				this.roleId = roleId;
		}

		public Date getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Date dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				UserBean userBean = (UserBean) o;

				if (id != userBean.id) return false;
				if (roleId != userBean.roleId) return false;
				if (createdBy != userBean.createdBy) return false;
				if (password != null ? !password.equals(userBean.password) : userBean.password != null) return false;
				if (secretQuestion != null ? !secretQuestion.equals(userBean.secretQuestion) : userBean.secretQuestion != null)
						return false;
				if (secretAnswer != null ? !secretAnswer.equals(userBean.secretAnswer) : userBean.secretAnswer != null)
						return false;
				if (username != null ? !username.equals(userBean.username) : userBean.username != null) return false;
				if (dateCreated != null ? !dateCreated.equals(userBean.dateCreated) : userBean.dateCreated != null)
						return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (password != null ? password.hashCode() : 0);
				result = 31 * result + (secretQuestion != null ? secretQuestion.hashCode() : 0);
				result = 31 * result + (secretAnswer != null ? secretAnswer.hashCode() : 0);
				result = 31 * result + (username != null ? username.hashCode() : 0);
				result = 31 * result + roleId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				return result;
		}

		public Collection<AcademicYearBean> getAcademicyearsesById() {
				return academicyearsesById;
		}

		public void setAcademicyearsesById(Collection<AcademicYearBean> academicyearsesById) {
				this.academicyearsesById = academicyearsesById;
		}

		public Collection<ClassBean> getClassesById() {
				return classesById;
		}

		public void setClassesById(Collection<ClassBean> classesById) {
				this.classesById = classesById;
		}

		public Collection<ClassBean> getClassesById_0() {
				return classesById_0;
		}

		public void setClassesById_0(Collection<ClassBean> classesById_0) {
				this.classesById_0 = classesById_0;
		}

		public Collection<ClassHierarchyBean> getClasshiearchiesById() {
				return classhiearchiesById;
		}

		public void setClasshiearchiesById(Collection<ClassHierarchyBean> classhiearchiesById) {
				this.classhiearchiesById = classhiearchiesById;
		}

		public Collection<ClassSubjectBean> getClasssubjectsById() {
				return classsubjectsById;
		}

		public void setClasssubjectsById(Collection<ClassSubjectBean> classsubjectsById) {
				this.classsubjectsById = classsubjectsById;
		}

		public Collection<ClassSubjectBean> getClasssubjectsById_0() {
				return classsubjectsById_0;
		}

		public void setClasssubjectsById_0(Collection<ClassSubjectBean> classsubjectsById_0) {
				this.classsubjectsById_0 = classsubjectsById_0;
		}

		public Collection<ExamResultBean> getExamresultsesById() {
				return examresultsesById;
		}

		public void setExamresultsesById(Collection<ExamResultBean> examresultsesById) {
				this.examresultsesById = examresultsesById;
		}

		public Collection<ExamBean> getExamsesById() {
				return examsesById;
		}

		public void setExamsesById(Collection<ExamBean> examsesById) {
				this.examsesById = examsesById;
		}

		public Collection<ExamSettingsBean> getExamsettingsesById() {
				return examsettingsesById;
		}

		public void setExamsettingsesById(Collection<ExamSettingsBean> examsettingsesById) {
				this.examsettingsesById = examsettingsesById;
		}

		public Collection<FeeAllocationBean> getFeeallocationsById() {
				return feeallocationsById;
		}

		public void setFeeallocationsById(Collection<FeeAllocationBean> feeallocationsById) {
				this.feeallocationsById = feeallocationsById;
		}

		public Collection<FeeApplicationLevelBean> getFeeapplicationlevelsesById() {
				return feeapplicationlevelsesById;
		}

		public void setFeeapplicationlevelsesById(Collection<FeeApplicationLevelBean> feeapplicationlevelsesById) {
				this.feeapplicationlevelsesById = feeapplicationlevelsesById;
		}

		public Collection<FeeDatesBean> getFeedatesById() {
				return feedatesById;
		}

		public void setFeedatesById(Collection<FeeDatesBean> feedatesById) {
				this.feedatesById = feedatesById;
		}

		public Collection<FeesCategoryBean> getFeescategoriesById() {
				return feescategoriesById;
		}

		public void setFeescategoriesById(Collection<FeesCategoryBean> feescategoriesById) {
				this.feescategoriesById = feescategoriesById;
		}

		public Collection<FeeSubcategoryBean> getFeessubcategoriesById() {
				return feessubcategoriesById;
		}

		public void setFeessubcategoriesById(Collection<FeeSubcategoryBean> feessubcategoriesById) {
				this.feessubcategoriesById = feessubcategoriesById;
		}

		public Collection<FeeWaiverBean> getFeewaiversesById() {
				return feewaiversesById;
		}

		public void setFeewaiversesById(Collection<FeeWaiverBean> feewaiversesById) {
				this.feewaiversesById = feewaiversesById;
		}

		public Collection<GradeScaleBean> getGradescalesById() {
				return gradescalesById;
		}

		public void setGradescalesById(Collection<GradeScaleBean> gradescalesById) {
				this.gradescalesById = gradescalesById;
		}

		public Collection<GuardianBean> getGuardienById() {
				return guardienById;
		}

		public void setGuardienById(Collection<GuardianBean> guardienById) {
				this.guardienById = guardienById;
		}

		public Collection<GuardianBean> getGuardienById_0() {
				return guardienById_0;
		}

		public void setGuardienById_0(Collection<GuardianBean> guardienById_0) {
				this.guardienById_0 = guardienById_0;
		}

		public Collection<LibBookBean> getLibBooksesById() {
				return libBooksesById;
		}

		public void setLibBooksesById(Collection<LibBookBean> libBooksesById) {
				this.libBooksesById = libBooksesById;
		}

		public Collection<LibCategoryBean> getLibCategoriesById() {
				return libCategoriesById;
		}

		public void setLibCategoriesById(Collection<LibCategoryBean> libCategoriesById) {
				this.libCategoriesById = libCategoriesById;
		}

		public Collection<LibIssuedBookBean> getLibIssuedBooksesById() {
				return libIssuedBooksesById;
		}

		public void setLibIssuedBooksesById(Collection<LibIssuedBookBean> libIssuedBooksesById) {
				this.libIssuedBooksesById = libIssuedBooksesById;
		}

		public Collection<LibIssuedBookBean> getLibIssuedBooksesById_0() {
				return libIssuedBooksesById_0;
		}

		public void setLibIssuedBooksesById_0(Collection<LibIssuedBookBean> libIssuedBooksesById_0) {
				this.libIssuedBooksesById_0 = libIssuedBooksesById_0;
		}

		public Collection<ParentBean> getParentsById() {
				return parentsById;
		}

		public void setParentsById(Collection<ParentBean> parentsById) {
				this.parentsById = parentsById;
		}

		public Collection<ParentBean> getParentsById_0() {
				return parentsById_0;
		}

		public void setParentsById_0(Collection<ParentBean> parentsById_0) {
				this.parentsById_0 = parentsById_0;
		}

		public Collection<StudentBean> getStudentsById() {
				return studentsById;
		}

		public void setStudentsById(Collection<StudentBean> studentsById) {
				this.studentsById = studentsById;
		}

		public Collection<StudentBean> getStudentsById_0() {
				return studentsById_0;
		}

		public void setStudentsById_0(Collection<StudentBean> studentsById_0) {
				this.studentsById_0 = studentsById_0;
		}

		public Collection<SubjectBean> getSubjectsById() {
				return subjectsById;
		}

		public void setSubjectsById(Collection<SubjectBean> subjectsById) {
				this.subjectsById = subjectsById;
		}

		public Collection<SubjectBean> getSubjectsById_0() {
				return subjectsById_0;
		}

		public void setSubjectsById_0(Collection<SubjectBean> subjectsById_0) {
				this.subjectsById_0 = subjectsById_0;
		}

		public Collection<TeacherBean> getTeachersesById() {
				return teachersesById;
		}

		public void setTeachersesById(Collection<TeacherBean> teachersesById) {
				this.teachersesById = teachersesById;
		}

		public Collection<TeacherBean> getTeachersesById_0() {
				return teachersesById_0;
		}

		public void setTeachersesById_0(Collection<TeacherBean> teachersesById_0) {
				this.teachersesById_0 = teachersesById_0;
		}

		public Collection<TermBean> getTermsesById() {
				return termsesById;
		}

		public void setTermsesById(Collection<TermBean> termsesById) {
				this.termsesById = termsesById;
		}

		public UserRoleBean getUserRolesByRoleId() {
				return userRolesByRoleId;
		}

		public void setUserRolesByRoleId(UserRoleBean userRolesByRoleId) {
				this.userRolesByRoleId = userRolesByRoleId;
		}
}
