package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class StudentBean {
		private int studentId;
		private String firstName;
		private String middleName;
		private String surname;
		private String initials;
		private Date dob;
		private Timestamp dateCreated;
		private Date wef;
		private Date wet;
		private String gender;
		private String bloodGroup;
		private String religion;
		private String presentAddress;
		private String permanentAddress;
		private String city;
		private String phone;
		private String email;
		private String mobile;
		private String prevSchool;
		private String schoolAddress;
		private String qualifications;
		private int entryClass;
		private int createdBy;
		private byte updated;
		private Integer updatedBy;
		private Date dateUpdated;
		private String status;
		private Collection<ExamResultBean> examresultsesByStudentId;
		private Collection<GuardianBean> guardienByStudentId;
		private Collection<LibIssuedBookBean> libIssuedBooksesByStudentId;
		private Collection<ParentBean> parentsByStudentId;
		private ClassBean classesByEntryClass;
		private UserBean usersByCreatedBy;
		private UserBean usersByUpdatedBy;

		public int getStudentId() {
				return studentId;
		}

		public void setStudentId(int studentId) {
				this.studentId = studentId;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getMiddleName() {
				return middleName;
		}

		public void setMiddleName(String middleName) {
				this.middleName = middleName;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getInitials() {
				return initials;
		}

		public void setInitials(String initials) {
				this.initials = initials;
		}

		public Date getDob() {
				return dob;
		}

		public void setDob(Date dob) {
				this.dob = dob;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getGender() {
				return gender;
		}

		public void setGender(String gender) {
				this.gender = gender;
		}

		public String getBloodGroup() {
				return bloodGroup;
		}

		public void setBloodGroup(String bloodGroup) {
				this.bloodGroup = bloodGroup;
		}

		public String getReligion() {
				return religion;
		}

		public void setReligion(String religion) {
				this.religion = religion;
		}

		public String getPresentAddress() {
				return presentAddress;
		}

		public void setPresentAddress(String presentAddress) {
				this.presentAddress = presentAddress;
		}

		public String getPermanentAddress() {
				return permanentAddress;
		}

		public void setPermanentAddress(String permanentAddress) {
				this.permanentAddress = permanentAddress;
		}

		public String getCity() {
				return city;
		}

		public void setCity(String city) {
				this.city = city;
		}

		public String getPhone() {
				return phone;
		}

		public void setPhone(String phone) {
				this.phone = phone;
		}

		public String getEmail() {
				return email;
		}

		public void setEmail(String email) {
				this.email = email;
		}

		public String getMobile() {
				return mobile;
		}

		public void setMobile(String mobile) {
				this.mobile = mobile;
		}

		public String getPrevSchool() {
				return prevSchool;
		}

		public void setPrevSchool(String prevSchool) {
				this.prevSchool = prevSchool;
		}

		public String getSchoolAddress() {
				return schoolAddress;
		}

		public void setSchoolAddress(String schoolAddress) {
				this.schoolAddress = schoolAddress;
		}

		public String getQualifications() {
				return qualifications;
		}

		public void setQualifications(String qualifications) {
				this.qualifications = qualifications;
		}

		public int getEntryClass() {
				return entryClass;
		}

		public void setEntryClass(int entryClass) {
				this.entryClass = entryClass;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				StudentBean that = (StudentBean) o;

				if (studentId != that.studentId) return false;
				if (entryClass != that.entryClass) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
				if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
				if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;
				if (initials != null ? !initials.equals(that.initials) : that.initials != null) return false;
				if (dob != null ? !dob.equals(that.dob) : that.dob != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (gender != null ? !gender.equals(that.gender) : that.gender != null) return false;
				if (bloodGroup != null ? !bloodGroup.equals(that.bloodGroup) : that.bloodGroup != null) return false;
				if (religion != null ? !religion.equals(that.religion) : that.religion != null) return false;
				if (presentAddress != null ? !presentAddress.equals(that.presentAddress) : that.presentAddress != null)
						return false;
				if (permanentAddress != null ? !permanentAddress.equals(that.permanentAddress) : that.permanentAddress != null)
						return false;
				if (city != null ? !city.equals(that.city) : that.city != null) return false;
				if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
				if (email != null ? !email.equals(that.email) : that.email != null) return false;
				if (mobile != null ? !mobile.equals(that.mobile) : that.mobile != null) return false;
				if (prevSchool != null ? !prevSchool.equals(that.prevSchool) : that.prevSchool != null) return false;
				if (schoolAddress != null ? !schoolAddress.equals(that.schoolAddress) : that.schoolAddress != null)
						return false;
				if (qualifications != null ? !qualifications.equals(that.qualifications) : that.qualifications != null)
						return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = studentId;
				result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
				result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
				result = 31 * result + (surname != null ? surname.hashCode() : 0);
				result = 31 * result + (initials != null ? initials.hashCode() : 0);
				result = 31 * result + (dob != null ? dob.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (gender != null ? gender.hashCode() : 0);
				result = 31 * result + (bloodGroup != null ? bloodGroup.hashCode() : 0);
				result = 31 * result + (religion != null ? religion.hashCode() : 0);
				result = 31 * result + (presentAddress != null ? presentAddress.hashCode() : 0);
				result = 31 * result + (permanentAddress != null ? permanentAddress.hashCode() : 0);
				result = 31 * result + (city != null ? city.hashCode() : 0);
				result = 31 * result + (phone != null ? phone.hashCode() : 0);
				result = 31 * result + (email != null ? email.hashCode() : 0);
				result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
				result = 31 * result + (prevSchool != null ? prevSchool.hashCode() : 0);
				result = 31 * result + (schoolAddress != null ? schoolAddress.hashCode() : 0);
				result = 31 * result + (qualifications != null ? qualifications.hashCode() : 0);
				result = 31 * result + entryClass;
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public Collection<ExamResultBean> getExamresultsesByStudentId() {
				return examresultsesByStudentId;
		}

		public void setExamresultsesByStudentId(Collection<ExamResultBean> examresultsesByStudentId) {
				this.examresultsesByStudentId = examresultsesByStudentId;
		}

		public Collection<GuardianBean> getGuardienByStudentId() {
				return guardienByStudentId;
		}

		public void setGuardienByStudentId(Collection<GuardianBean> guardienByStudentId) {
				this.guardienByStudentId = guardienByStudentId;
		}

		public Collection<LibIssuedBookBean> getLibIssuedBooksesByStudentId() {
				return libIssuedBooksesByStudentId;
		}

		public void setLibIssuedBooksesByStudentId(Collection<LibIssuedBookBean> libIssuedBooksesByStudentId) {
				this.libIssuedBooksesByStudentId = libIssuedBooksesByStudentId;
		}

		public Collection<ParentBean> getParentsByStudentId() {
				return parentsByStudentId;
		}

		public void setParentsByStudentId(Collection<ParentBean> parentsByStudentId) {
				this.parentsByStudentId = parentsByStudentId;
		}

		public ClassBean getClassesByEntryClass() {
				return classesByEntryClass;
		}

		public void setClassesByEntryClass(ClassBean classesByEntryClass) {
				this.classesByEntryClass = classesByEntryClass;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}
}
