package com.sms.model;

import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class ClassHierarchyBean {
		private int hierachyId;
		private int levelId;
		private int fromClassId;
		private int toClassId;
		private byte repeatAllowed;
		private int passMark;
		private Timestamp dateCreated;
		private int createdBy;
		private String status;
		private ClassBean classesByFromClassId;
		private ClassBean classesByToClassId;
		private UserBean usersByCreatedBy;

		public int getHierachyId() {
				return hierachyId;
		}

		public void setHierachyId(int hierachyId) {
				this.hierachyId = hierachyId;
		}

		public int getLevelId() {
				return levelId;
		}

		public void setLevelId(int levelId) {
				this.levelId = levelId;
		}

		public int getFromClassId() {
				return fromClassId;
		}

		public void setFromClassId(int fromClassId) {
				this.fromClassId = fromClassId;
		}

		public int getToClassId() {
				return toClassId;
		}

		public void setToClassId(int toClassId) {
				this.toClassId = toClassId;
		}

		public byte getRepeatAllowed() {
				return repeatAllowed;
		}

		public void setRepeatAllowed(byte repeatAllowed) {
				this.repeatAllowed = repeatAllowed;
		}

		public int getPassMark() {
				return passMark;
		}

		public void setPassMark(int passMark) {
				this.passMark = passMark;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ClassHierarchyBean that = (ClassHierarchyBean) o;

				if (hierachyId != that.hierachyId) return false;
				if (levelId != that.levelId) return false;
				if (fromClassId != that.fromClassId) return false;
				if (toClassId != that.toClassId) return false;
				if (repeatAllowed != that.repeatAllowed) return false;
				if (passMark != that.passMark) return false;
				if (createdBy != that.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = hierachyId;
				result = 31 * result + levelId;
				result = 31 * result + fromClassId;
				result = 31 * result + toClassId;
				result = 31 * result + (int) repeatAllowed;
				result = 31 * result + passMark;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public ClassBean getClassesByFromClassId() {
				return classesByFromClassId;
		}

		public void setClassesByFromClassId(ClassBean classesByFromClassId) {
				this.classesByFromClassId = classesByFromClassId;
		}

		public ClassBean getClassesByToClassId() {
				return classesByToClassId;
		}

		public void setClassesByToClassId(ClassBean classesByToClassId) {
				this.classesByToClassId = classesByToClassId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
