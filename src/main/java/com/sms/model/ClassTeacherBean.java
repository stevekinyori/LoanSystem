package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class ClassTeacherBean {
		private int classId;
		private int teacherId;
		private Timestamp dateCreated;
		private int createdBy;
		private Date dateUpdated;
		private Integer updatedBy;
		private String status;
		private ClassBean classesByClassId;
		private TeacherBean teachersByTeacherId;

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getTeacherId() {
				return teacherId;
		}

		public void setTeacherId(int teacherId) {
				this.teacherId = teacherId;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ClassTeacherBean that = (ClassTeacherBean) o;

				if (classId != that.classId) return false;
				if (teacherId != that.teacherId) return false;
				if (createdBy != that.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = classId;
				result = 31 * result + teacherId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public ClassBean getClassesByClassId() {
				return classesByClassId;
		}

		public void setClassesByClassId(ClassBean classesByClassId) {
				this.classesByClassId = classesByClassId;
		}

		public TeacherBean getTeachersByTeacherId() {
				return teachersByTeacherId;
		}

		public void setTeachersByTeacherId(TeacherBean teachersByTeacherId) {
				this.teachersByTeacherId = teachersByTeacherId;
		}
}
