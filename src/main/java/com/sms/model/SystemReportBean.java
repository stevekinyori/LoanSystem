package com.sms.model;

import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class SystemReportBean {
		private int reportId;
		private String reportName;
		private int reportCode;
		private String reportDescription;
		private String templateLocation;
		private byte templateRequired;
		private int createdBy;
		private Timestamp dateCreated;
		private Timestamp lastRun;

		public int getReportId() {
				return reportId;
		}

		public void setReportId(int reportId) {
				this.reportId = reportId;
		}

		public String getReportName() {
				return reportName;
		}

		public void setReportName(String reportName) {
				this.reportName = reportName;
		}

		public int getReportCode() {
				return reportCode;
		}

		public void setReportCode(int reportCode) {
				this.reportCode = reportCode;
		}

		public String getReportDescription() {
				return reportDescription;
		}

		public void setReportDescription(String reportDescription) {
				this.reportDescription = reportDescription;
		}

		public String getTemplateLocation() {
				return templateLocation;
		}

		public void setTemplateLocation(String templateLocation) {
				this.templateLocation = templateLocation;
		}

		public byte getTemplateRequired() {
				return templateRequired;
		}

		public void setTemplateRequired(byte templateRequired) {
				this.templateRequired = templateRequired;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Timestamp getLastRun() {
				return lastRun;
		}

		public void setLastRun(Timestamp lastRun) {
				this.lastRun = lastRun;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				SystemReportBean that = (SystemReportBean) o;

				if (reportId != that.reportId) return false;
				if (reportCode != that.reportCode) return false;
				if (templateRequired != that.templateRequired) return false;
				if (createdBy != that.createdBy) return false;
				if (reportName != null ? !reportName.equals(that.reportName) : that.reportName != null) return false;
				if (reportDescription != null ? !reportDescription.equals(that.reportDescription) : that.reportDescription != null)
						return false;
				if (templateLocation != null ? !templateLocation.equals(that.templateLocation) : that.templateLocation != null)
						return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (lastRun != null ? !lastRun.equals(that.lastRun) : that.lastRun != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = reportId;
				result = 31 * result + (reportName != null ? reportName.hashCode() : 0);
				result = 31 * result + reportCode;
				result = 31 * result + (reportDescription != null ? reportDescription.hashCode() : 0);
				result = 31 * result + (templateLocation != null ? templateLocation.hashCode() : 0);
				result = 31 * result + (int) templateRequired;
				result = 31 * result + createdBy;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (lastRun != null ? lastRun.hashCode() : 0);
				return result;
		}
}
