package com.sms.model.Common;

/**
 * Created by Steve on 1/13/2017.
 */
public class Config {
		public int level;
		public String name;
		public String desc;
		public boolean status;

		public int getLevel() {
				return level;
		}

		public void setLevel(int level) {
				this.level = level;
		}

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getDesc() {
				return desc;
		}

		public void setDesc(String desc) {
				this.desc = desc;
		}

		public boolean isStatus() {
				return status;
		}

		public void setStatus(boolean status) {
				this.status = status;
		}
}
