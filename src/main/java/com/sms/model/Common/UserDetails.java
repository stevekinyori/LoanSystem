package com.sms.model.Common;

import com.sms.model.UserRoleBean;

/**
 * Created by Steve on 1/13/2017.
 */
public class UserDetails {
		public String username;
		public String password;
		public UserRoleBean userRoles;
		public String token;

		public String getUsername() {
				return username;
		}

		public void setUsername(String username) {
				this.username = username;
		}

		public String getPassword() {
				return password;
		}

		public void setPassword(String password) {
				this.password = password;
		}

		public UserRoleBean getUserRoles() {
				return userRoles;
		}

		public void setUserRoles(UserRoleBean userRoles) {
				this.userRoles = userRoles;
		}

		public String getToken() {
				return token;
		}

		public void setToken(String token) {
				this.token = token;
		}

}
