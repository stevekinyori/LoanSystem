package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeeSubcategoryBean {
		private int id;
		private int categoryId;
		private String subcategoryName;
		private int amount;
		private int feeType;
		private Timestamp dateCreated;
		private int createdBy;
		private Date wef;
		private Date wet;
		private String status;
		private Collection<FeeAllocationBean> feeallocationsById;
		private Collection<FeeDatesBean> feedatesById;
		private FeesCategoryBean feescategoryByCategoryId;
		private UserBean usersByCreatedBy;
		private Collection<FeeWaiverBean> feewaiversesById;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getCategoryId() {
				return categoryId;
		}

		public void setCategoryId(int categoryId) {
				this.categoryId = categoryId;
		}

		public String getSubcategoryName() {
				return subcategoryName;
		}

		public void setSubcategoryName(String subcategoryName) {
				this.subcategoryName = subcategoryName;
		}

		public int getAmount() {
				return amount;
		}

		public void setAmount(int amount) {
				this.amount = amount;
		}

		public int getFeeType() {
				return feeType;
		}

		public void setFeeType(int feeType) {
				this.feeType = feeType;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeeSubcategoryBean that = (FeeSubcategoryBean) o;

				if (id != that.id) return false;
				if (categoryId != that.categoryId) return false;
				if (amount != that.amount) return false;
				if (feeType != that.feeType) return false;
				if (createdBy != that.createdBy) return false;
				if (subcategoryName != null ? !subcategoryName.equals(that.subcategoryName) : that.subcategoryName != null)
						return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + categoryId;
				result = 31 * result + (subcategoryName != null ? subcategoryName.hashCode() : 0);
				result = 31 * result + amount;
				result = 31 * result + feeType;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public Collection<FeeAllocationBean> getFeeallocationsById() {
				return feeallocationsById;
		}

		public void setFeeallocationsById(Collection<FeeAllocationBean> feeallocationsById) {
				this.feeallocationsById = feeallocationsById;
		}

		public Collection<FeeDatesBean> getFeedatesById() {
				return feedatesById;
		}

		public void setFeedatesById(Collection<FeeDatesBean> feedatesById) {
				this.feedatesById = feedatesById;
		}

		public FeesCategoryBean getFeescategoryByCategoryId() {
				return feescategoryByCategoryId;
		}

		public void setFeescategoryByCategoryId(FeesCategoryBean feescategoryByCategoryId) {
				this.feescategoryByCategoryId = feescategoryByCategoryId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public Collection<FeeWaiverBean> getFeewaiversesById() {
				return feewaiversesById;
		}

		public void setFeewaiversesById(Collection<FeeWaiverBean> feewaiversesById) {
				this.feewaiversesById = feewaiversesById;
		}
}
