package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class ClassSubjectBean {
		private int id;
		private int classId;
		private int subjectId;
		private int teacherId;
		private Timestamp dateCreated;
		private Date wef;
		private Date wet;
		private int createdBy;
		private byte updated;
		private Integer updatedBy;
		private Date dateUpdated;
		private ClassBean classesByClassId;
		private SubjectBean subjectBySubjectId;
		private TeacherBean teachersByTeacherId;
		private UserBean usersByCreatedBy;
		private UserBean usersByUpdatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getSubjectId() {
				return subjectId;
		}

		public void setSubjectId(int subjectId) {
				this.subjectId = subjectId;
		}

		public int getTeacherId() {
				return teacherId;
		}

		public void setTeacherId(int teacherId) {
				this.teacherId = teacherId;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ClassSubjectBean that = (ClassSubjectBean) o;

				if (id != that.id) return false;
				if (classId != that.classId) return false;
				if (subjectId != that.subjectId) return false;
				if (teacherId != that.teacherId) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + classId;
				result = 31 * result + subjectId;
				result = 31 * result + teacherId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				return result;
		}

		public ClassBean getClassesByClassId() {
				return classesByClassId;
		}

		public void setClassesByClassId(ClassBean classesByClassId) {
				this.classesByClassId = classesByClassId;
		}

		public SubjectBean getSubjectBySubjectId() {
				return subjectBySubjectId;
		}

		public void setSubjectBySubjectId(SubjectBean subjectBySubjectId) {
				this.subjectBySubjectId = subjectBySubjectId;
		}

		public TeacherBean getTeachersByTeacherId() {
				return teachersByTeacherId;
		}

		public void setTeachersByTeacherId(TeacherBean teachersByTeacherId) {
				this.teachersByTeacherId = teachersByTeacherId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}
}
