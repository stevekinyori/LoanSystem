package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class GradeScaleBean {
		private int id;
		private int examId;
		private int subjectId;
		private int classId;
		private String grade;
		private int lowerLimit;
		private int upperLimit;
		private String limitType;
		private Date wef;
		private Date wet;
		private int createdBy;
		private Timestamp dateCreated;
		private String status;
		private ExamBean examsByExamId;
		private SubjectBean subjectBySubjectId;
		private ClassBean classesByClassId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getExamId() {
				return examId;
		}

		public void setExamId(int examId) {
				this.examId = examId;
		}

		public int getSubjectId() {
				return subjectId;
		}

		public void setSubjectId(int subjectId) {
				this.subjectId = subjectId;
		}

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public String getGrade() {
				return grade;
		}

		public void setGrade(String grade) {
				this.grade = grade;
		}

		public int getLowerLimit() {
				return lowerLimit;
		}

		public void setLowerLimit(int lowerLimit) {
				this.lowerLimit = lowerLimit;
		}

		public int getUpperLimit() {
				return upperLimit;
		}

		public void setUpperLimit(int upperLimit) {
				this.upperLimit = upperLimit;
		}

		public String getLimitType() {
				return limitType;
		}

		public void setLimitType(String limitType) {
				this.limitType = limitType;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				GradeScaleBean that = (GradeScaleBean) o;

				if (id != that.id) return false;
				if (examId != that.examId) return false;
				if (subjectId != that.subjectId) return false;
				if (classId != that.classId) return false;
				if (lowerLimit != that.lowerLimit) return false;
				if (upperLimit != that.upperLimit) return false;
				if (createdBy != that.createdBy) return false;
				if (grade != null ? !grade.equals(that.grade) : that.grade != null) return false;
				if (limitType != null ? !limitType.equals(that.limitType) : that.limitType != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + examId;
				result = 31 * result + subjectId;
				result = 31 * result + classId;
				result = 31 * result + (grade != null ? grade.hashCode() : 0);
				result = 31 * result + lowerLimit;
				result = 31 * result + upperLimit;
				result = 31 * result + (limitType != null ? limitType.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public ExamBean getExamsByExamId() {
				return examsByExamId;
		}

		public void setExamsByExamId(ExamBean examsByExamId) {
				this.examsByExamId = examsByExamId;
		}

		public SubjectBean getSubjectBySubjectId() {
				return subjectBySubjectId;
		}

		public void setSubjectBySubjectId(SubjectBean subjectBySubjectId) {
				this.subjectBySubjectId = subjectBySubjectId;
		}

		public ClassBean getClassesByClassId() {
				return classesByClassId;
		}

		public void setClassesByClassId(ClassBean classesByClassId) {
				this.classesByClassId = classesByClassId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
