package com.sms.model;

import java.sql.Date;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class TermBean {
		private int termId;
		private int academicYearId;
		private Date dateCreated;
		private int createdBy;
		private Date wef;
		private Date wet;
		private String status;
		private Collection<ExamBean> examsesByTermId;
		private Collection<ExamSettingsBean> examsettingsesByTermId;
		private AcademicYearBean academicyearsByAcademicYearId;
		private UserBean usersByCreatedBy;

		public int getTermId() {
				return termId;
		}

		public void setTermId(int termId) {
				this.termId = termId;
		}

		public int getAcademicYearId() {
				return academicYearId;
		}

		public void setAcademicYearId(int academicYearId) {
				this.academicYearId = academicYearId;
		}

		public Date getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Date dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				TermBean termBean = (TermBean) o;

				if (termId != termBean.termId) return false;
				if (academicYearId != termBean.academicYearId) return false;
				if (createdBy != termBean.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(termBean.dateCreated) : termBean.dateCreated != null)
						return false;
				if (wef != null ? !wef.equals(termBean.wef) : termBean.wef != null) return false;
				if (wet != null ? !wet.equals(termBean.wet) : termBean.wet != null) return false;
				if (status != null ? !status.equals(termBean.status) : termBean.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = termId;
				result = 31 * result + academicYearId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public Collection<ExamBean> getExamsesByTermId() {
				return examsesByTermId;
		}

		public void setExamsesByTermId(Collection<ExamBean> examsesByTermId) {
				this.examsesByTermId = examsesByTermId;
		}

		public Collection<ExamSettingsBean> getExamsettingsesByTermId() {
				return examsettingsesByTermId;
		}

		public void setExamsettingsesByTermId(Collection<ExamSettingsBean> examsettingsesByTermId) {
				this.examsettingsesByTermId = examsettingsesByTermId;
		}

		public AcademicYearBean getAcademicyearsByAcademicYearId() {
				return academicyearsByAcademicYearId;
		}

		public void setAcademicyearsByAcademicYearId(AcademicYearBean academicyearsByAcademicYearId) {
				this.academicyearsByAcademicYearId = academicyearsByAcademicYearId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
