package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class SubjectBean {
		private int subjectId;
		private String subjectName;
		private String subjectCode;
		private String description;
		private Timestamp dateCreated;
		private int createdBy;
		private byte updated;
		private Date dateUpdated;
		private int updatedBy;
		private Collection<ClassSubjectBean> classsubjectsBySubjectId;
		private Collection<ExamResultBean> examresultsesBySubjectId;
		private Collection<ExamSettingsBean> examsettingsesBySubjectId;
		private Collection<GradeScaleBean> gradescalesBySubjectId;
		private UserBean usersByCreatedBy;
		private UserBean usersByUpdatedBy;

		public int getSubjectId() {
				return subjectId;
		}

		public void setSubjectId(int subjectId) {
				this.subjectId = subjectId;
		}

		public String getSubjectName() {
				return subjectName;
		}

		public void setSubjectName(String subjectName) {
				this.subjectName = subjectName;
		}

		public String getSubjectCode() {
				return subjectCode;
		}

		public void setSubjectCode(String subjectCode) {
				this.subjectCode = subjectCode;
		}

		public String getDescription() {
				return description;
		}

		public void setDescription(String description) {
				this.description = description;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public int getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(int updatedBy) {
				this.updatedBy = updatedBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				SubjectBean that = (SubjectBean) o;

				if (subjectId != that.subjectId) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (updatedBy != that.updatedBy) return false;
				if (subjectName != null ? !subjectName.equals(that.subjectName) : that.subjectName != null) return false;
				if (subjectCode != null ? !subjectCode.equals(that.subjectCode) : that.subjectCode != null) return false;
				if (description != null ? !description.equals(that.description) : that.description != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = subjectId;
				result = 31 * result + (subjectName != null ? subjectName.hashCode() : 0);
				result = 31 * result + (subjectCode != null ? subjectCode.hashCode() : 0);
				result = 31 * result + (description != null ? description.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + updatedBy;
				return result;
		}

		public Collection<ClassSubjectBean> getClasssubjectsBySubjectId() {
				return classsubjectsBySubjectId;
		}

		public void setClasssubjectsBySubjectId(Collection<ClassSubjectBean> classsubjectsBySubjectId) {
				this.classsubjectsBySubjectId = classsubjectsBySubjectId;
		}

		public Collection<ExamResultBean> getExamresultsesBySubjectId() {
				return examresultsesBySubjectId;
		}

		public void setExamresultsesBySubjectId(Collection<ExamResultBean> examresultsesBySubjectId) {
				this.examresultsesBySubjectId = examresultsesBySubjectId;
		}

		public Collection<ExamSettingsBean> getExamsettingsesBySubjectId() {
				return examsettingsesBySubjectId;
		}

		public void setExamsettingsesBySubjectId(Collection<ExamSettingsBean> examsettingsesBySubjectId) {
				this.examsettingsesBySubjectId = examsettingsesBySubjectId;
		}

		public Collection<GradeScaleBean> getGradescalesBySubjectId() {
				return gradescalesBySubjectId;
		}

		public void setGradescalesBySubjectId(Collection<GradeScaleBean> gradescalesBySubjectId) {
				this.gradescalesBySubjectId = gradescalesBySubjectId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}
}
