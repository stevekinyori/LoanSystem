package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class AcademicYearBean {
		private int id;
		private Date wef;
		private Date wet;
		private String status;
		private int createdBy;
		private Timestamp dateCreated;
		private UserBean usersByCreatedBy;
		private Collection<TermBean> termsesById;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				AcademicYearBean that = (AcademicYearBean) o;

				if (id != that.id) return false;
				if (createdBy != that.createdBy) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				return result;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public Collection<TermBean> getTermsesById() {
				return termsesById;
		}

		public void setTermsesById(Collection<TermBean> termsesById) {
				this.termsesById = termsesById;
		}
}
