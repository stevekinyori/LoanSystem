package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class StreamBean {
		private int streamId;
		private String streamName;
		private String streamInitials;
		private String description;
		@JsonIgnore
		private int createdBy;
		private Timestamp dateCreated;
		private String status;
		private String uuid;
		@JsonIgnore
		private Collection<ClassBean> classesByStreamId;

		public int getStreamId() {
				return streamId;
		}

		public void setStreamId(int streamId) {
				this.streamId = streamId;
		}

		public String getStreamName() {
				return streamName;
		}

		public void setStreamName(String streamName) {
				this.streamName = streamName;
		}

		public String getStreamInitials() {
				return streamInitials;
		}

		public void setStreamInitials(String streamInitials) {
				this.streamInitials = streamInitials;
		}

		public String getDescription() {
				return description;
		}

		public void setDescription(String description) {
				this.description = description;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public String getUuid() {
				return uuid;
		}

		public void setUuid(String uuid) {
				this.uuid = uuid;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				StreamBean that = (StreamBean) o;

				if (streamId != that.streamId) return false;
				if (createdBy != that.createdBy) return false;
				if (streamName != null ? !streamName.equals(that.streamName) : that.streamName != null) return false;
				if (streamInitials != null ? !streamInitials.equals(that.streamInitials) : that.streamInitials != null)
						return false;
				if (description != null ? !description.equals(that.description) : that.description != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = streamId;
				result = 31 * result + (streamName != null ? streamName.hashCode() : 0);
				result = 31 * result + (streamInitials != null ? streamInitials.hashCode() : 0);
				result = 31 * result + (description != null ? description.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
				return result;
		}

		public Collection<ClassBean> getClassesByStreamId() {
				return classesByStreamId;
		}

		public void setClassesByStreamId(Collection<ClassBean> classesByStreamId) {
				this.classesByStreamId = classesByStreamId;
		}
}
