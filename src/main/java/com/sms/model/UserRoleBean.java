package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class UserRoleBean {
		private String id;
		private String name;
		private String desc;
		private Timestamp dateCreated;
		@JsonIgnore
		private int createdBy;
		private String status;
		private Collection<RolePrivilegeBean> roleprevilagesById;
		@JsonIgnore
		private Collection<UserBean> usersesById;

		public String getId() {
				return id;
		}

		public void setId(String id) {
				this.id = id;
		}

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getDesc() {
				return desc;
		}

		public void setDesc(String desc) {
				this.desc = desc;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				UserRoleBean that = (UserRoleBean) o;

				if (id != that.id) return false;
				if (name != that.name) return false;
				if (createdBy != that.createdBy) return false;
				if (desc != null ? !desc.equals(that.desc) : that.desc != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}


		public Collection<RolePrivilegeBean> getRoleprevilagesById() {
				return roleprevilagesById;
		}

		public void setRoleprevilagesById(Collection<RolePrivilegeBean> roleprevilagesById) {
				this.roleprevilagesById = roleprevilagesById;
		}

		public Collection<UserBean> getUsersesById() {
				return usersesById;
		}

		public void setUsersesById(Collection<UserBean> usersesById) {
				this.usersesById = usersesById;
		}
}
