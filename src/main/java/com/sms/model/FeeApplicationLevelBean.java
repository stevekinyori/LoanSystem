package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeeApplicationLevelBean {
		private int levelId;
		private int name;
		private int description;
		private Timestamp dateCreated;
		private int createdBy;
		private Date wef;
		private Date wet;
		private String status;
		private Collection<FeeAllocationBean> feeallocationsByLevelId;
		private UserBean usersByCreatedBy;

		public int getLevelId() {
				return levelId;
		}

		public void setLevelId(int levelId) {
				this.levelId = levelId;
		}

		public int getName() {
				return name;
		}

		public void setName(int name) {
				this.name = name;
		}

		public int getDescription() {
				return description;
		}

		public void setDescription(int description) {
				this.description = description;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeeApplicationLevelBean that = (FeeApplicationLevelBean) o;

				if (levelId != that.levelId) return false;
				if (name != that.name) return false;
				if (description != that.description) return false;
				if (createdBy != that.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = levelId;
				result = 31 * result + name;
				result = 31 * result + description;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public Collection<FeeAllocationBean> getFeeallocationsByLevelId() {
				return feeallocationsByLevelId;
		}

		public void setFeeallocationsByLevelId(Collection<FeeAllocationBean> feeallocationsByLevelId) {
				this.feeallocationsByLevelId = feeallocationsByLevelId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
