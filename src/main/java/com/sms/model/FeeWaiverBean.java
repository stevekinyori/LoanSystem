package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeeWaiverBean {
		private int id;
		private int feeCategoryId;
		private int feeSubCategoryId;
		private String waiverType;
		private Timestamp dateCreated;
		private String gender;
		private int rate;
		private String rateType;
		private Date wef;
		private Date wet;
		private int createdBy;
		private FeesCategoryBean feescategoryByFeeCategoryId;
		private FeeSubcategoryBean feessubcategoryByFeeSubCategoryId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getFeeCategoryId() {
				return feeCategoryId;
		}

		public void setFeeCategoryId(int feeCategoryId) {
				this.feeCategoryId = feeCategoryId;
		}

		public int getFeeSubCategoryId() {
				return feeSubCategoryId;
		}

		public void setFeeSubCategoryId(int feeSubCategoryId) {
				this.feeSubCategoryId = feeSubCategoryId;
		}

		public String getWaiverType() {
				return waiverType;
		}

		public void setWaiverType(String waiverType) {
				this.waiverType = waiverType;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public String getGender() {
				return gender;
		}

		public void setGender(String gender) {
				this.gender = gender;
		}

		public int getRate() {
				return rate;
		}

		public void setRate(int rate) {
				this.rate = rate;
		}

		public String getRateType() {
				return rateType;
		}

		public void setRateType(String rateType) {
				this.rateType = rateType;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeeWaiverBean that = (FeeWaiverBean) o;

				if (id != that.id) return false;
				if (feeCategoryId != that.feeCategoryId) return false;
				if (feeSubCategoryId != that.feeSubCategoryId) return false;
				if (rate != that.rate) return false;
				if (createdBy != that.createdBy) return false;
				if (waiverType != null ? !waiverType.equals(that.waiverType) : that.waiverType != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (gender != null ? !gender.equals(that.gender) : that.gender != null) return false;
				if (rateType != null ? !rateType.equals(that.rateType) : that.rateType != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + feeCategoryId;
				result = 31 * result + feeSubCategoryId;
				result = 31 * result + (waiverType != null ? waiverType.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (gender != null ? gender.hashCode() : 0);
				result = 31 * result + rate;
				result = 31 * result + (rateType != null ? rateType.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + createdBy;
				return result;
		}

		public FeesCategoryBean getFeescategoryByFeeCategoryId() {
				return feescategoryByFeeCategoryId;
		}

		public void setFeescategoryByFeeCategoryId(FeesCategoryBean feescategoryByFeeCategoryId) {
				this.feescategoryByFeeCategoryId = feescategoryByFeeCategoryId;
		}

		public FeeSubcategoryBean getFeessubcategoryByFeeSubCategoryId() {
				return feessubcategoryByFeeSubCategoryId;
		}

		public void setFeessubcategoryByFeeSubCategoryId(FeeSubcategoryBean feessubcategoryByFeeSubCategoryId) {
				this.feessubcategoryByFeeSubCategoryId = feessubcategoryByFeeSubCategoryId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
