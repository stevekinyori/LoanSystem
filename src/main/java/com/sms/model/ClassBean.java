package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class ClassBean {
		private int classId;
		private int streamId;
		private int classCapacity;
		private int classNo;
		private Timestamp dateCreated;
		@JsonIgnore
		private int createdBy;
		private byte updated;
		@JsonIgnore
		private Integer updatedBy;
		private Date dateUpdated;
		private String status;
		private StreamBean streamsByStreamId;
		@JsonIgnore
		private UserBean usersByCreatedBy;
		@JsonIgnore
		private UserBean usersByUpdatedBy;
		private Collection<ClassHierarchyBean> classhiearchiesByClassId;
		@JsonIgnore
		private Collection<ClassHierarchyBean> classhiearchiesByClassId_0;
		private Collection<ClassSubjectBean> classsubjectsByClassId;
		private Collection<ClassTeacherBean> classteachersesByClassId;
		private Collection<ExamResultBean> examresultsesByClassId;
		private Collection<ExamSettingsBean> examsettingsesByClassId;
		private Collection<GradeScaleBean> gradescalesByClassId;
		private Collection<StudentBean> studentsByClassId;

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getStreamId() {
				return streamId;
		}

		public void setStreamId(int streamId) {
				this.streamId = streamId;
		}

		public int getClassCapacity() {
				return classCapacity;
		}

		public void setClassCapacity(int classCapacity) {
				this.classCapacity = classCapacity;
		}

		public int getClassNo() {
				return classNo;
		}

		public void setClassNo(int classNo) {
				this.classNo = classNo;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ClassBean that = (ClassBean) o;

				if (classId != that.classId) return false;
				if (streamId != that.streamId) return false;
				if (classCapacity != that.classCapacity) return false;
				if (classNo != that.classNo) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = classId;
				result = 31 * result + streamId;
				result = 31 * result + classCapacity;
				result = 31 * result + classNo;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public StreamBean getStreamsByStreamId() {
				return streamsByStreamId;
		}

		public void setStreamsByStreamId(StreamBean streamsByStreamId) {
				this.streamsByStreamId = streamsByStreamId;
		}

		@JsonIgnore
		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		@JsonIgnore
		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		@JsonIgnore
		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		@JsonIgnore
		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}

		public Collection<ClassHierarchyBean> getClasshiearchiesByClassId() {
				return classhiearchiesByClassId;
		}

		public void setClasshiearchiesByClassId(Collection<ClassHierarchyBean> classhiearchiesByClassId) {
				this.classhiearchiesByClassId = classhiearchiesByClassId;
		}

		public Collection<ClassHierarchyBean> getClasshiearchiesByClassId_0() {
				return classhiearchiesByClassId_0;
		}

		public void setClasshiearchiesByClassId_0(Collection<ClassHierarchyBean> classhiearchiesByClassId_0) {
				this.classhiearchiesByClassId_0 = classhiearchiesByClassId_0;
		}

		public Collection<ClassSubjectBean> getClasssubjectsByClassId() {
				return classsubjectsByClassId;
		}

		public void setClasssubjectsByClassId(Collection<ClassSubjectBean> classsubjectsByClassId) {
				this.classsubjectsByClassId = classsubjectsByClassId;
		}

		public Collection<ClassTeacherBean> getClassteachersesByClassId() {
				return classteachersesByClassId;
		}

		public void setClassteachersesByClassId(Collection<ClassTeacherBean> classteachersesByClassId) {
				this.classteachersesByClassId = classteachersesByClassId;
		}

		public Collection<ExamResultBean> getExamresultsesByClassId() {
				return examresultsesByClassId;
		}

		public void setExamresultsesByClassId(Collection<ExamResultBean> examresultsesByClassId) {
				this.examresultsesByClassId = examresultsesByClassId;
		}

		public Collection<ExamSettingsBean> getExamsettingsesByClassId() {
				return examsettingsesByClassId;
		}

		public void setExamsettingsesByClassId(Collection<ExamSettingsBean> examsettingsesByClassId) {
				this.examsettingsesByClassId = examsettingsesByClassId;
		}

		public Collection<GradeScaleBean> getGradescalesByClassId() {
				return gradescalesByClassId;
		}

		public void setGradescalesByClassId(Collection<GradeScaleBean> gradescalesByClassId) {
				this.gradescalesByClassId = gradescalesByClassId;
		}

		public Collection<StudentBean> getStudentsByClassId() {
				return studentsByClassId;
		}

		public void setStudentsByClassId(Collection<StudentBean> studentsByClassId) {
				this.studentsByClassId = studentsByClassId;
		}
}
