package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class RolePrivilegeBean {
		private int id;
		private int roleId;
		private int previlageId;
		private Timestamp dateCreated;
		@JsonIgnore
		private int createdBy;
		@JsonIgnore
		private UserRoleBean userRolesByRoleId;
		private PrivilegeBean previlagesByPrevilageId;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getRoleId() {
				return roleId;
		}

		public void setRoleId(int roleId) {
				this.roleId = roleId;
		}

		public int getPrevilageId() {
				return previlageId;
		}

		public void setPrevilageId(int previlageId) {
				this.previlageId = previlageId;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				RolePrivilegeBean that = (RolePrivilegeBean) o;

				if (id != that.id) return false;
				if (roleId != that.roleId) return false;
				if (previlageId != that.previlageId) return false;
				if (createdBy != that.createdBy) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + roleId;
				result = 31 * result + previlageId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				return result;
		}

		public UserRoleBean getUserRolesByRoleId() {
				return userRolesByRoleId;
		}

		public void setUserRolesByRoleId(UserRoleBean userRolesByRoleId) {
				this.userRolesByRoleId = userRolesByRoleId;
		}

		public PrivilegeBean getPrevilagesByPrevilageId() {
				return previlagesByPrevilageId;
		}

		public void setPrevilagesByPrevilageId(PrivilegeBean previlagesByPrevilageId) {
				this.previlagesByPrevilageId = previlagesByPrevilageId;
		}
}
