package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class LibIssuedBookBean {
		private int id;
		private String userType;
		private int userId;
		private int bookId;
		private Timestamp dateCreated;
		private Date dueDate;
		private String status;
		private String issueCondition;
		private String returnCondition;
		private Date returnDate;
		private String issueComments;
		private int issuedBy;

		private Integer receivedBy;
		private String returnComments;
		private StudentBean studentByUserId;
		private LibBookBean bookByBookId;
		private UserBean usersByIssuedBy;
		private UserBean usersByReceivedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public String getUserType() {
				return userType;
		}

		public void setUserType(String userType) {
				this.userType = userType;
		}

		public int getUserId() {
				return userId;
		}

		public void setUserId(int userId) {
				this.userId = userId;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Date getDueDate() {
				return dueDate;
		}

		public void setDueDate(Date dueDate) {
				this.dueDate = dueDate;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public String getIssueCondition() {
				return issueCondition;
		}

		public void setIssueCondition(String issueCondition) {
				this.issueCondition = issueCondition;
		}

		public String getReturnCondition() {
				return returnCondition;
		}

		public void setReturnCondition(String returnCondition) {
				this.returnCondition = returnCondition;
		}

		public Date getReturnDate() {
				return returnDate;
		}

		public void setReturnDate(Date returnDate) {
				this.returnDate = returnDate;
		}

		public String getIssueComments() {
				return issueComments;
		}

		public void setIssueComments(String issueComments) {
				this.issueComments = issueComments;
		}

		public int getIssuedBy() {
				return issuedBy;
		}

		public void setIssuedBy(int issuedBy) {
				this.issuedBy = issuedBy;
		}

		public Integer getReceivedBy() {
				return receivedBy;
		}

		public void setReceivedBy(Integer receivedBy) {
				this.receivedBy = receivedBy;
		}

		public String getReturnComments() {
				return returnComments;
		}

		public void setReturnComments(String returnComments) {
				this.returnComments = returnComments;
		}

		public int getBookId() {
				return bookId;
		}

		public void setBookId(int bookId) {
				this.bookId = bookId;
		}

		public LibBookBean getBookByBookId() {
				return bookByBookId;
		}

		public void setBookByBookId(LibBookBean bookByBookId) {
				this.bookByBookId = bookByBookId;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				LibIssuedBookBean that = (LibIssuedBookBean) o;

				if (id != that.id) return false;
				if (userId != that.userId) return false;
				if (issuedBy != that.issuedBy) return false;
				if (userType != null ? !userType.equals(that.userType) : that.userType != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (dueDate != null ? !dueDate.equals(that.dueDate) : that.dueDate != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (issueCondition != null ? !issueCondition.equals(that.issueCondition) : that.issueCondition != null)
						return false;
				if (returnCondition != null ? !returnCondition.equals(that.returnCondition) : that.returnCondition != null)
						return false;
				if (returnDate != null ? !returnDate.equals(that.returnDate) : that.returnDate != null) return false;
				if (issueComments != null ? !issueComments.equals(that.issueComments) : that.issueComments != null)
						return false;
				if (receivedBy != null ? !receivedBy.equals(that.receivedBy) : that.receivedBy != null) return false;
				if (returnComments != null ? !returnComments.equals(that.returnComments) : that.returnComments != null)
						return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (userType != null ? userType.hashCode() : 0);
				result = 31 * result + userId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + (issueCondition != null ? issueCondition.hashCode() : 0);
				result = 31 * result + (returnCondition != null ? returnCondition.hashCode() : 0);
				result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
				result = 31 * result + (issueComments != null ? issueComments.hashCode() : 0);
				result = 31 * result + issuedBy;
				result = 31 * result + (receivedBy != null ? receivedBy.hashCode() : 0);
				result = 31 * result + (returnComments != null ? returnComments.hashCode() : 0);
				return result;
		}

		public StudentBean getStudentByUserId() {
				return studentByUserId;
		}

		public void setStudentByUserId(StudentBean studentByUserId) {
				this.studentByUserId = studentByUserId;
		}

		public UserBean getUsersByIssuedBy() {
				return usersByIssuedBy;
		}

		public void setUsersByIssuedBy(UserBean usersByIssuedBy) {
				this.usersByIssuedBy = usersByIssuedBy;
		}

		public UserBean getUsersByReceivedBy() {
				return usersByReceivedBy;
		}

		public void setUsersByReceivedBy(UserBean usersByReceivedBy) {
				this.usersByReceivedBy = usersByReceivedBy;
		}
}
