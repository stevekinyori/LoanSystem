package com.sms.model;

import java.sql.Date;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class TeacherBean {
		private int teacherId;
		private String firstName;
		private String middleName;
		private String initials;
		private String surname;
		private String otherNames;
		private Date dateOfBirth;
		private Integer pinNo;
		private String address;
		private String mobileNo;
		private String email;
		private Date dateOfHiring;
		private String status;
		private Date dateCreated;
		private int createdBy;
		private byte updated;
		private Date dateUpdated;
		private Integer updatedBy;
		private Collection<ClassSubjectBean> classsubjectsByTeacherId;
		private Collection<ClassTeacherBean> classteachersesByTeacherId;
		private UserBean usersByCreatedBy;
		private UserBean usersByUpdatedBy;

		public int getTeacherId() {
				return teacherId;
		}

		public void setTeacherId(int teacherId) {
				this.teacherId = teacherId;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getMiddleName() {
				return middleName;
		}

		public void setMiddleName(String middleName) {
				this.middleName = middleName;
		}

		public String getInitials() {
				return initials;
		}

		public void setInitials(String initials) {
				this.initials = initials;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getOtherNames() {
				return otherNames;
		}

		public void setOtherNames(String otherNames) {
				this.otherNames = otherNames;
		}

		public Date getDateOfBirth() {
				return dateOfBirth;
		}

		public void setDateOfBirth(Date dateOfBirth) {
				this.dateOfBirth = dateOfBirth;
		}

		public Integer getPinNo() {
				return pinNo;
		}

		public void setPinNo(Integer pinNo) {
				this.pinNo = pinNo;
		}

		public String getAddress() {
				return address;
		}

		public void setAddress(String address) {
				this.address = address;
		}

		public String getMobileNo() {
				return mobileNo;
		}

		public void setMobileNo(String mobileNo) {
				this.mobileNo = mobileNo;
		}

		public String getEmail() {
				return email;
		}

		public void setEmail(String email) {
				this.email = email;
		}

		public Date getDateOfHiring() {
				return dateOfHiring;
		}

		public void setDateOfHiring(Date dateOfHiring) {
				this.dateOfHiring = dateOfHiring;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		public Date getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Date dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				TeacherBean that = (TeacherBean) o;

				if (teacherId != that.teacherId) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
				if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
				if (initials != null ? !initials.equals(that.initials) : that.initials != null) return false;
				if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;
				if (otherNames != null ? !otherNames.equals(that.otherNames) : that.otherNames != null) return false;
				if (dateOfBirth != null ? !dateOfBirth.equals(that.dateOfBirth) : that.dateOfBirth != null) return false;
				if (pinNo != null ? !pinNo.equals(that.pinNo) : that.pinNo != null) return false;
				if (address != null ? !address.equals(that.address) : that.address != null) return false;
				if (mobileNo != null ? !mobileNo.equals(that.mobileNo) : that.mobileNo != null) return false;
				if (email != null ? !email.equals(that.email) : that.email != null) return false;
				if (dateOfHiring != null ? !dateOfHiring.equals(that.dateOfHiring) : that.dateOfHiring != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = teacherId;
				result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
				result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
				result = 31 * result + (initials != null ? initials.hashCode() : 0);
				result = 31 * result + (surname != null ? surname.hashCode() : 0);
				result = 31 * result + (otherNames != null ? otherNames.hashCode() : 0);
				result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
				result = 31 * result + (pinNo != null ? pinNo.hashCode() : 0);
				result = 31 * result + (address != null ? address.hashCode() : 0);
				result = 31 * result + (mobileNo != null ? mobileNo.hashCode() : 0);
				result = 31 * result + (email != null ? email.hashCode() : 0);
				result = 31 * result + (dateOfHiring != null ? dateOfHiring.hashCode() : 0);
				result = 31 * result + (status != null ? status.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				return result;
		}

		public Collection<ClassSubjectBean> getClasssubjectsByTeacherId() {
				return classsubjectsByTeacherId;
		}

		public void setClasssubjectsByTeacherId(Collection<ClassSubjectBean> classsubjectsByTeacherId) {
				this.classsubjectsByTeacherId = classsubjectsByTeacherId;
		}

		public Collection<ClassTeacherBean> getClassteachersesByTeacherId() {
				return classteachersesByTeacherId;
		}

		public void setClassteachersesByTeacherId(Collection<ClassTeacherBean> classteachersesByTeacherId) {
				this.classteachersesByTeacherId = classteachersesByTeacherId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}
}
