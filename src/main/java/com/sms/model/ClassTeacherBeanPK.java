package com.sms.model;

import java.io.Serializable;

/**
 * Created by Steve on 11/20/2016.
 */
public class ClassTeacherBeanPK implements Serializable {
		private int classId;
		private int teacherId;

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getTeacherId() {
				return teacherId;
		}

		public void setTeacherId(int teacherId) {
				this.teacherId = teacherId;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ClassTeacherBeanPK that = (ClassTeacherBeanPK) o;

				if (classId != that.classId) return false;
				if (teacherId != that.teacherId) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = classId;
				result = 31 * result + teacherId;
				return result;
		}
}
