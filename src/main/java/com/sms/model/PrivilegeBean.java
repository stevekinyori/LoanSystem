package com.sms.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class PrivilegeBean {
		private int id;
		private String name;
		private String code;
		private String desc;
		private Timestamp dateCreated;
		@JsonIgnore
		private int createdBy;
		@JsonIgnore
		private Collection<RolePrivilegeBean> roleprevilagesById;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getCode() {
				return code;
		}

		public void setCode(String code) {
				this.code = code;
		}

		public String getDesc() {
				return desc;
		}

		public void setDesc(String desc) {
				this.desc = desc;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				PrivilegeBean that = (PrivilegeBean) o;

				if (id != that.id) return false;
				if (createdBy != that.createdBy) return false;
				if (name != null ? !name.equals(that.name) : that.name != null) return false;
				if (code != null ? !code.equals(that.code) : that.code != null) return false;
				if (desc != null ? !desc.equals(that.desc) : that.desc != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (name != null ? name.hashCode() : 0);
				result = 31 * result + (code != null ? code.hashCode() : 0);
				result = 31 * result + (desc != null ? desc.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				return result;
		}

		public Collection<RolePrivilegeBean> getRoleprevilagesById() {
				return roleprevilagesById;
		}

		public void setRoleprevilagesById(Collection<RolePrivilegeBean> roleprevilagesById) {
				this.roleprevilagesById = roleprevilagesById;
		}
}
