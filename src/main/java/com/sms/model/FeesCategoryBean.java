package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeesCategoryBean {
		private int id;
		private String name;
		private String receiptPrefix;
		private String description;
		private Timestamp dateCreated;
		private Date wef;
		private Date wet;
		private int createdBy;
		private Collection<FeeAllocationBean> feeallocationsById;
		private UserBean usersByCreatedBy;
		private Collection<FeeSubcategoryBean> feessubcategoriesById;
		private Collection<FeeWaiverBean> feewaiversesById;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public String getName() {
				return name;
		}

		public void setName(String name) {
				this.name = name;
		}

		public String getReceiptPrefix() {
				return receiptPrefix;
		}

		public void setReceiptPrefix(String receiptPrefix) {
				this.receiptPrefix = receiptPrefix;
		}

		public String getDescription() {
				return description;
		}

		public void setDescription(String description) {
				this.description = description;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeesCategoryBean that = (FeesCategoryBean) o;

				if (id != that.id) return false;
				if (createdBy != that.createdBy) return false;
				if (name != null ? !name.equals(that.name) : that.name != null) return false;
				if (receiptPrefix != null ? !receiptPrefix.equals(that.receiptPrefix) : that.receiptPrefix != null)
						return false;
				if (description != null ? !description.equals(that.description) : that.description != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (name != null ? name.hashCode() : 0);
				result = 31 * result + (receiptPrefix != null ? receiptPrefix.hashCode() : 0);
				result = 31 * result + (description != null ? description.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + createdBy;
				return result;
		}

		public Collection<FeeAllocationBean> getFeeallocationsById() {
				return feeallocationsById;
		}

		public void setFeeallocationsById(Collection<FeeAllocationBean> feeallocationsById) {
				this.feeallocationsById = feeallocationsById;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public Collection<FeeSubcategoryBean> getFeessubcategoriesById() {
				return feessubcategoriesById;
		}

		public void setFeessubcategoriesById(Collection<FeeSubcategoryBean> feessubcategoriesById) {
				this.feessubcategoriesById = feessubcategoriesById;
		}

		public Collection<FeeWaiverBean> getFeewaiversesById() {
				return feewaiversesById;
		}

		public void setFeewaiversesById(Collection<FeeWaiverBean> feewaiversesById) {
				this.feewaiversesById = feewaiversesById;
		}
}
