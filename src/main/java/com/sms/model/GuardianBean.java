package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class GuardianBean {
		private int id;
		private String firstName;
		private String middleName;
		private String initials;
		private String surname;
		private String mobile;
		private String email;
		private String address;
		private String occupation;
		private String phoneNo;
		private int studentId;
		private Timestamp dateCreated;
		private int createdBy;
		private byte updated;
		private Date dateUpdated;
		private Integer updatedBy;
		private String gender;
		private StudentBean studentByStudentId;
		private UserBean usersByCreatedBy;
		private UserBean usersByUpdatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getMiddleName() {
				return middleName;
		}

		public void setMiddleName(String middleName) {
				this.middleName = middleName;
		}

		public String getInitials() {
				return initials;
		}

		public void setInitials(String initials) {
				this.initials = initials;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getMobile() {
				return mobile;
		}

		public void setMobile(String mobile) {
				this.mobile = mobile;
		}

		public String getEmail() {
				return email;
		}

		public void setEmail(String email) {
				this.email = email;
		}

		public String getAddress() {
				return address;
		}

		public void setAddress(String address) {
				this.address = address;
		}

		public String getOccupation() {
				return occupation;
		}

		public void setOccupation(String occupation) {
				this.occupation = occupation;
		}

		public String getPhoneNo() {
				return phoneNo;
		}

		public void setPhoneNo(String phoneNo) {
				this.phoneNo = phoneNo;
		}

		public int getStudentId() {
				return studentId;
		}

		public void setStudentId(int studentId) {
				this.studentId = studentId;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public byte getUpdated() {
				return updated;
		}

		public void setUpdated(byte updated) {
				this.updated = updated;
		}

		public Date getDateUpdated() {
				return dateUpdated;
		}

		public void setDateUpdated(Date dateUpdated) {
				this.dateUpdated = dateUpdated;
		}

		public Integer getUpdatedBy() {
				return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
				this.updatedBy = updatedBy;
		}

		public String getGender() {
				return gender;
		}

		public void setGender(String gender) {
				this.gender = gender;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				GuardianBean that = (GuardianBean) o;

				if (id != that.id) return false;
				if (studentId != that.studentId) return false;
				if (createdBy != that.createdBy) return false;
				if (updated != that.updated) return false;
				if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
				if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
				if (initials != null ? !initials.equals(that.initials) : that.initials != null) return false;
				if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;
				if (mobile != null ? !mobile.equals(that.mobile) : that.mobile != null) return false;
				if (email != null ? !email.equals(that.email) : that.email != null) return false;
				if (address != null ? !address.equals(that.address) : that.address != null) return false;
				if (occupation != null ? !occupation.equals(that.occupation) : that.occupation != null) return false;
				if (phoneNo != null ? !phoneNo.equals(that.phoneNo) : that.phoneNo != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (dateUpdated != null ? !dateUpdated.equals(that.dateUpdated) : that.dateUpdated != null) return false;
				if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
				if (gender != null ? !gender.equals(that.gender) : that.gender != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
				result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
				result = 31 * result + (initials != null ? initials.hashCode() : 0);
				result = 31 * result + (surname != null ? surname.hashCode() : 0);
				result = 31 * result + (mobile != null ? mobile.hashCode() : 0);
				result = 31 * result + (email != null ? email.hashCode() : 0);
				result = 31 * result + (address != null ? address.hashCode() : 0);
				result = 31 * result + (occupation != null ? occupation.hashCode() : 0);
				result = 31 * result + (phoneNo != null ? phoneNo.hashCode() : 0);
				result = 31 * result + studentId;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (int) updated;
				result = 31 * result + (dateUpdated != null ? dateUpdated.hashCode() : 0);
				result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
				result = 31 * result + (gender != null ? gender.hashCode() : 0);
				return result;
		}

		public StudentBean getStudentByStudentId() {
				return studentByStudentId;
		}

		public void setStudentByStudentId(StudentBean studentByStudentId) {
				this.studentByStudentId = studentByStudentId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}

		public UserBean getUsersByUpdatedBy() {
				return usersByUpdatedBy;
		}

		public void setUsersByUpdatedBy(UserBean usersByUpdatedBy) {
				this.usersByUpdatedBy = usersByUpdatedBy;
		}
}
