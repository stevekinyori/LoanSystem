package com.sms.model;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class ExamSettingsBean {
		private int id;
		private int examId;
		private int classId;
		private int termId;
		private int subjectId;
		private Date examDate;
		private Time startTime;
		private Time endTime;
		private int maxMark;
		private int passMark;
		private Timestamp dateCreated;
		private int createdBy;
		private String status;
		private ExamBean examsByExamId;
		private ClassBean classesByClassId;
		private TermBean termsByTermId;
		private SubjectBean subjectBySubjectId;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getExamId() {
				return examId;
		}

		public void setExamId(int examId) {
				this.examId = examId;
		}

		public int getClassId() {
				return classId;
		}

		public void setClassId(int classId) {
				this.classId = classId;
		}

		public int getTermId() {
				return termId;
		}

		public void setTermId(int termId) {
				this.termId = termId;
		}

		public int getSubjectId() {
				return subjectId;
		}

		public void setSubjectId(int subjectId) {
				this.subjectId = subjectId;
		}

		public Date getExamDate() {
				return examDate;
		}

		public void setExamDate(Date examDate) {
				this.examDate = examDate;
		}

		public Time getStartTime() {
				return startTime;
		}

		public void setStartTime(Time startTime) {
				this.startTime = startTime;
		}

		public Time getEndTime() {
				return endTime;
		}

		public void setEndTime(Time endTime) {
				this.endTime = endTime;
		}

		public int getMaxMark() {
				return maxMark;
		}

		public void setMaxMark(int maxMark) {
				this.maxMark = maxMark;
		}

		public int getPassMark() {
				return passMark;
		}

		public void setPassMark(int passMark) {
				this.passMark = passMark;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				ExamSettingsBean that = (ExamSettingsBean) o;

				if (id != that.id) return false;
				if (examId != that.examId) return false;
				if (classId != that.classId) return false;
				if (termId != that.termId) return false;
				if (subjectId != that.subjectId) return false;
				if (maxMark != that.maxMark) return false;
				if (passMark != that.passMark) return false;
				if (createdBy != that.createdBy) return false;
				if (examDate != null ? !examDate.equals(that.examDate) : that.examDate != null) return false;
				if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;
				if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + examId;
				result = 31 * result + classId;
				result = 31 * result + termId;
				result = 31 * result + subjectId;
				result = 31 * result + (examDate != null ? examDate.hashCode() : 0);
				result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
				result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
				result = 31 * result + maxMark;
				result = 31 * result + passMark;
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public ExamBean getExamsByExamId() {
				return examsByExamId;
		}

		public void setExamsByExamId(ExamBean examsByExamId) {
				this.examsByExamId = examsByExamId;
		}

		public ClassBean getClassesByClassId() {
				return classesByClassId;
		}

		public void setClassesByClassId(ClassBean classesByClassId) {
				this.classesByClassId = classesByClassId;
		}

		public TermBean getTermsByTermId() {
				return termsByTermId;
		}

		public void setTermsByTermId(TermBean termsByTermId) {
				this.termsByTermId = termsByTermId;
		}

		public SubjectBean getSubjectBySubjectId() {
				return subjectBySubjectId;
		}

		public void setSubjectBySubjectId(SubjectBean subjectBySubjectId) {
				this.subjectBySubjectId = subjectBySubjectId;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
