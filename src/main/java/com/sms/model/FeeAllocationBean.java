package com.sms.model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Steve on 11/20/2016.
 */
public class FeeAllocationBean {
		private int id;
		private int feeCategory;
		private int feeSubCategory;
		private int applicationlevel;
		private Date wef;
		private Date wet;
		private Timestamp dateCreated;
		private int createdBy;
		private String status;
		private FeesCategoryBean feescategoryByFeeCategory;
		private FeeSubcategoryBean feessubcategoryByFeeSubCategory;
		private FeeApplicationLevelBean feeapplicationlevelsByApplicationlevel;
		private UserBean usersByCreatedBy;

		public int getId() {
				return id;
		}

		public void setId(int id) {
				this.id = id;
		}

		public int getFeeCategory() {
				return feeCategory;
		}

		public void setFeeCategory(int feeCategory) {
				this.feeCategory = feeCategory;
		}

		public int getFeeSubCategory() {
				return feeSubCategory;
		}

		public void setFeeSubCategory(int feeSubCategory) {
				this.feeSubCategory = feeSubCategory;
		}

		public int getApplicationlevel() {
				return applicationlevel;
		}

		public void setApplicationlevel(int applicationlevel) {
				this.applicationlevel = applicationlevel;
		}

		public Date getWef() {
				return wef;
		}

		public void setWef(Date wef) {
				this.wef = wef;
		}

		public Date getWet() {
				return wet;
		}

		public void setWet(Date wet) {
				this.wet = wet;
		}

		public Timestamp getDateCreated() {
				return dateCreated;
		}

		public void setDateCreated(Timestamp dateCreated) {
				this.dateCreated = dateCreated;
		}

		public int getCreatedBy() {
				return createdBy;
		}

		public void setCreatedBy(int createdBy) {
				this.createdBy = createdBy;
		}

		public String getStatus() {
				return status;
		}

		public void setStatus(String status) {
				this.status = status;
		}

		@Override
		public boolean equals(Object o) {
				if (this == o) return true;
				if (o == null || getClass() != o.getClass()) return false;

				FeeAllocationBean that = (FeeAllocationBean) o;

				if (id != that.id) return false;
				if (feeCategory != that.feeCategory) return false;
				if (feeSubCategory != that.feeSubCategory) return false;
				if (applicationlevel != that.applicationlevel) return false;
				if (createdBy != that.createdBy) return false;
				if (wef != null ? !wef.equals(that.wef) : that.wef != null) return false;
				if (wet != null ? !wet.equals(that.wet) : that.wet != null) return false;
				if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
				if (status != null ? !status.equals(that.status) : that.status != null) return false;

				return true;
		}

		@Override
		public int hashCode() {
				int result = id;
				result = 31 * result + feeCategory;
				result = 31 * result + feeSubCategory;
				result = 31 * result + applicationlevel;
				result = 31 * result + (wef != null ? wef.hashCode() : 0);
				result = 31 * result + (wet != null ? wet.hashCode() : 0);
				result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
				result = 31 * result + createdBy;
				result = 31 * result + (status != null ? status.hashCode() : 0);
				return result;
		}

		public FeesCategoryBean getFeescategoryByFeeCategory() {
				return feescategoryByFeeCategory;
		}

		public void setFeescategoryByFeeCategory(FeesCategoryBean feescategoryByFeeCategory) {
				this.feescategoryByFeeCategory = feescategoryByFeeCategory;
		}

		public FeeSubcategoryBean getFeessubcategoryByFeeSubCategory() {
				return feessubcategoryByFeeSubCategory;
		}

		public void setFeessubcategoryByFeeSubCategory(FeeSubcategoryBean feessubcategoryByFeeSubCategory) {
				this.feessubcategoryByFeeSubCategory = feessubcategoryByFeeSubCategory;
		}

		public FeeApplicationLevelBean getFeeapplicationlevelsByApplicationlevel() {
				return feeapplicationlevelsByApplicationlevel;
		}

		public void setFeeapplicationlevelsByApplicationlevel(FeeApplicationLevelBean feeapplicationlevelsByApplicationlevel) {
				this.feeapplicationlevelsByApplicationlevel = feeapplicationlevelsByApplicationlevel;
		}

		public UserBean getUsersByCreatedBy() {
				return usersByCreatedBy;
		}

		public void setUsersByCreatedBy(UserBean usersByCreatedBy) {
				this.usersByCreatedBy = usersByCreatedBy;
		}
}
