package com.sms.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Created by Steve on 1/15/2017.
 */
public class SMSJsonMapper extends ObjectMapper {
		public SMSJsonMapper() {
				this.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		}
}
