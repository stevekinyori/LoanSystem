package com.sms.Controllers;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.model.SchoolBean;
import com.sms.service.SchoolService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.sms.Controllers.Constants.UrlConstants.*;

/**
 * Created by Steve on 1/13/2017.
 */
@Controller
public class SchoolController {
		Logger logger = Logger.getLogger(SchoolController.class);
		SessionFactory factory;
		@Autowired
		private SchoolService schoolService;

		@RequestMapping(value = CREATE_SCHOOL,
						consumes = {"application/json"},
						produces = {"application/json"},
						method = RequestMethod.POST)
		public
		@ResponseBody
		SchoolBean saveInitialSchoolDetails(@RequestBody SchoolBean schoolBean) throws SmsCustomException {
				logger.info("SchoolController : saveInitialSchoolDetails() method Invoked");
				SchoolBean school;
				try {
						school = schoolService.createSchool(schoolBean);
				} catch (Exception ex) {
						throw new SmsCustomException("System error! Creating school",
										HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
				return school;
		}

		@RequestMapping(value = UPDATE_SCHOOL,
						consumes = {"application/json"},
						produces = {"application/json"},
						method = RequestMethod.POST)
		public
		@ResponseBody
		SchoolBean updateSchoolDetails(@RequestBody SchoolBean schoolBean) throws SmsCustomException {
				logger.info("SchoolController : updateSchoolDetails() method Invoked");
				SchoolBean school;
				try {
						school = schoolService.updateSchoolDetails(schoolBean);
				} catch (Exception ex) {
						throw new SmsCustomException("System error! updating school details ", HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
				return school;
		}

		@RequestMapping(value = GET_SCHOOL_DETAILS,
						produces = {"application/json"},
						method = RequestMethod.GET)
		public
		@ResponseBody
		SchoolBean getSchoolDetails() throws SmsCustomException {
				logger.info("SchoolController : getSchoolDetails() method Invoked");
				SchoolBean school;
				try {
						school = schoolService.getSchoolDetails();
				} catch (Exception ex) {
						throw new SmsCustomException("System error! Fetching school details ", HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
				return school;
		}
}
