package com.sms.Controllers;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.model.Common.Config;
import com.sms.model.RolePrivilegeBean;
import com.sms.model.SchoolBean;
import com.sms.model.UserBean;
import com.sms.model.UserRoleBean;
import com.sms.service.RoleService;
import com.sms.service.SchoolService;
import com.sms.service.UserService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;
import java.util.ArrayList;
import java.util.List;

import static com.sms.Controllers.Constants.UrlConstants.GET_CONFIG_STATUS;

/**
 * Created by Steve on 1/13/2017.
 */
@Controller
public class ConfigController {
		Logger logger = Logger.getLogger(ClassController.class);
		SessionFactory factory;

		@Autowired
		private HttpServletRequest request;
		@Autowired
		private SchoolService schoolService;
		@Autowired
		private UserService userService;
		@Autowired
		private RoleService roleService;

		@RequestMapping(value = GET_CONFIG_STATUS, produces = {"application/json"}, method = RequestMethod.GET)
		public
		@ResponseBody
		List<Config> getConfigStatus() throws HTTPException, SmsCustomException {
				logger.info("ConfigController : getUsers() method Invoked");
				List<Config> configs = new ArrayList<>();
				try {

						Config schoolConfig = new Config();
						schoolConfig.setLevel(1);
						schoolConfig.setName("School Config");
						schoolConfig.setDesc("Complete School configuration check");

						SchoolBean schoolBean = schoolService.getSchoolDetails();

						if (schoolBean.getName() == null) {
								schoolConfig.setStatus(false);
						} else {
								schoolConfig.setStatus(true);
						}


						configs.add(schoolConfig);


						Config privilegeConfig = new Config();
						privilegeConfig.setLevel(2);
						privilegeConfig.setName("Privileges Config");
						privilegeConfig.setDesc("Check for defined rights in the system");

						List<RolePrivilegeBean> privileges = roleService.getAllPrivileges();
						if (privileges.isEmpty()) {
								privilegeConfig.setStatus(false);
						} else {
								privilegeConfig.setStatus(true);
						}
						configs.add(privilegeConfig);


						Config rolesConfig = new Config();
						rolesConfig.setLevel(3);
						rolesConfig.setName("Roles Config");
						rolesConfig.setDesc("Check for roles defined in the system");
						List<UserRoleBean> roles = roleService.getAllUserRoles();
						if (roles.isEmpty()) {
								rolesConfig.setStatus(false);
						} else {
								rolesConfig.setStatus(true);
						}
						configs.add(rolesConfig);

						Config userConfig = new Config();
						userConfig.setLevel(4);
						userConfig.setName("User Config");
						userConfig.setDesc("Check for default user");

						List<UserBean> users = userService.getAllUsers();
						if (users.isEmpty()) {
								userConfig.setStatus(false);
						} else {
								userConfig.setStatus(true);
						}


						configs.add(userConfig);
				} catch (Exception ex) {
						throw new SmsCustomException("System error! Fetching current configurations", HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
				return configs;
		}
}
