package com.sms.Controllers;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.dao.interfaces.Submodels.User;
import com.sms.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Steve on 1/13/2017.
 */
@Controller
public class AuthController {
		Logger logger = Logger.getLogger(UserController.class);
		@Autowired
		private UserService userService;


		@RequestMapping(
						value = "/login",
						consumes = {"application/json"},
						produces = {"application/json"},
						method = RequestMethod.POST
		)
		public
		@ResponseBody
		Boolean authenticate(@RequestBody User user) throws SmsCustomException {
				logger.info("UserController : authenticate() method Invoked");
				try {
						return userService.validateUserPassword(user.getUsername(), user.getPassword());
				} catch (Exception ex) {
						throw new SmsCustomException("Error validating username and password provided", HttpStatus.FORBIDDEN.value());
				}
		}
}
