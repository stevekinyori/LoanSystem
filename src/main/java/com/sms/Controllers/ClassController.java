package com.sms.Controllers;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.model.ClassBean;
import com.sms.service.ClassService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;
import java.util.List;

import static com.sms.Controllers.Constants.UrlConstants.*;

/**
 * Created by Steve on 1/13/2017.
 */
@Controller
public class ClassController {
		Logger logger = Logger.getLogger(ClassController.class);
		SessionFactory factory;
		@Autowired
		private ClassService classService;
		@Autowired
		private HttpServletRequest request;


		@RequestMapping(value = GET_ALL_CLASSES,
						produces = {"application/json"},
						method = RequestMethod.GET)
		public
		@ResponseBody
		List<ClassBean> getClasses() throws SmsCustomException {
				logger.info("ClassController : getClasses() method Invoked");
				try {

						return classService.getAllClasses();
				} catch (Exception ex) {
						logger.error(ex);
						throw new SmsCustomException("Error fetching classes",
										HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
		}

		@RequestMapping(value = CREATE_CLASS,
						produces = {"application/json"},
						consumes = {"application/json"},
						method = RequestMethod.POST)
		public
		@ResponseBody
		ClassBean createClass(@RequestBody ClassBean classBean) throws SmsCustomException {
				logger.info("ClassController : createClass() method Invoked");
				try {
						return classService.createClass(classBean);
				} catch (Exception ex) {
						logger.error(ex);
						throw new SmsCustomException("Error creating class",
										HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
		}

		@RequestMapping(value = UPDATE_CLASS,
						produces = {"application/json"},
						consumes = {"application/json"},
						method = RequestMethod.POST)
		public
		@ResponseBody
		ClassBean updateClass(@RequestBody ClassBean classBean) throws HTTPException, SmsCustomException {
				logger.info("ClassController : updateClass() method Invoked");
				try {
						return classService.updateClass(classBean);
				} catch (Exception ex) {
						logger.error(ex);
						throw new SmsCustomException("Error updating class details",
										HttpStatus.INTERNAL_SERVER_ERROR.value());
				}
		}

		@RequestMapping(value = GET_CLASS_BY_ID,
						produces = {"application/json"},
						method = RequestMethod.GET)
		public
		@ResponseBody
		ClassBean getClassById(@PathVariable int id) throws SmsCustomException {
				logger.info("ClassController : getClassById() method Invoked");
				try {
						return classService.getClassById(id);
				} catch (Exception ex) {
						logger.error(ex);
						throw new SmsCustomException("Class Not found",
										HttpStatus.NOT_FOUND.value());
				}
		}

		@RequestMapping(value = DELETE_CLASS,
						produces = {"application/json"},
						method = RequestMethod.POST)
		public
		@ResponseBody
		boolean deleteClass(@PathVariable int id) throws  SmsCustomException {
				logger.info("ClassController : deleteClass() method Invoked");
				try {
						return classService.deleteClass(id);
				} catch (Exception ex) {
						logger.error(ex.getMessage());
						throw new SmsCustomException("Class Not found",
										HttpStatus.NOT_FOUND.value());
				}
		}
}
