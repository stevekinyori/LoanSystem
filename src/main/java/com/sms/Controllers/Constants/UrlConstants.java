package com.sms.Controllers.Constants;

/**
 * Created by Steve on 1/13/2017.
 */
public class UrlConstants {
		//config
		public static final String GET_CONFIG_STATUS = "/config/status";
		// School details
		public static final String CREATE_SCHOOL = "/school/create";
		public static final String UPDATE_SCHOOL = "/school/update";
		public static final String GET_SCHOOL_DETAILS = "/school/details";

		//users
		public static final String GET_ALL_USERS = "/users/list";
		public static final String GET_USER_BY_ID = "/users/get/{id}";
		public static final String CREATE_USER = "/users/new";
		public static final String GET_USER_ROLES = "/users/roles";
		public static final String DELETE_USER = "/users/delete/{id}";
		public static final String UPDATE_USER = "/users/update";

		//Privileges
		public static final String CREATE_PRIVILEGE = "/privilege/new";
		public static final String UPDATE_PRIVILEGE = "/privilege/update";
		public static final String DELETE_PRIVILEGE = "/privilege/delete/{id}";
		public static final String GET_ALL_PRIVILEGES = "/privilege/list";

		// ROLE PRIVILEGE
		public static final String ADD_ROLE_PRIVILEGE = "/role_privilege/new";
		public static final String UPDATE_ROLE_PRIVILEGE = "/role_privilege/update";
		public static final String DELETE_ROLE_PRIVILEGE = "/role_privilege/delete/{id}";
		public static final String GET_ALL_ROLE_PRIVILEGES = "/role_privilege/get/all";
		public static final String GET_ROLE_PRIVILEGES_BY_ROLE_ID = "/role_privilege/get/{roleId}";

		//roles
		public static final String CREATE_ROLE = "/role/create";
		public static final String UPDATE_ROLE = "/role/update";
		public static final String DELETE_ROLE = "/role/delete/{id}";
		public static final String GET_ALL_ROLES = "/role/get";
		public static final String GET_ROLES_BY_USER_ID = "/role/list/get/{userId]";

		//CLASS TEACHERS

		public static final String GET_ALL_CLASS_TEACHERS = "/class_teachers/list";
		public static final String ADD_CLASS_TEACHER = "/class_teachers/new";
		public static final String DELETE_CLASS_TEACHER = "/class_teachers/delete/{classId}/{teacherId}";
		public static final String UPDATE_CLASS_TEACHER = "/class_teachers/update";
		public static final String GET_CLASS_TEACHER_BY_CLASS_ID = "/class_teachers/list/get/{classId}";

		//Exams
		public static final String GET_ALl_SCHOOL_EXAMS = "/exams/list";
		public static final String ADD_EXAM = "/exams/new";
		public static final String UPDATE_EXAM = "/exams/update";
		public static final String DELETE_EXAM = "/exams/delete/{examId}";


		public static final String GET_ALl_SCHOOL_EXAMS_BY_STATUS = "/exams/list/get/{status}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID = "/exams/results/list/get/{examId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_TERM_ID = "/exams/results/list/get/{examId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID = "/exams/results/list/get/{examId}/{classId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID_TERM_ID = "/exams/results/list/get/{examId}/{classId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID_STUDENT_ID = "/exams/results/list/get/{examId}/{classId}/{studentId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID_STUDENT_ID_TERM_ID = "/exams/results/list/get/{examId}/{classId}/{studentId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID_STUDENT_ID_SUBJECT_ID = "/exams/results/list/get/{examId}/{classId}//{studentId}//{subjectId}";
		public static final String GET_EXAM_RESULTS_BY_EXAM_ID_CLASS_ID_STUDENT_ID_SUBJECT_ID_TERM_ID = "/exams/results/list/get/{examId}/{classId}//{studentId}//{subjectId}/{termId}";
		public static final String ADD_EXAM_RESULT = "/exams/results/new";
		public static final String UPDATE_EXAM_RESULT = "/exams/results/update";
		public static final String DELETE_EXAM_RESULT = "/exams/results/delete/{examResultId}";
		public static final String GET_EXAM_RESULTS_BY_STUDENT_ID = "/exams/results/list/get/{studentId}";
		public static final String GET_EXAM_RESULTS_BY_STUDENT_ID_TERM_ID = "/exams/results/list/get/{studentId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID = "/exams/results/list/get/{classId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID_TERM_ID = "/exams/results/list/get/{classId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_SUBJECT_ID = "/exams/results/list/get/{subjectId}";
		public static final String GET_EXAM_RESULTS_BY_SUBJECT_ID_TERM_ID = "/exams/results/list/get/{subjectId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_SUBJECT_ID_STUDENT_ID = "/exams/results/list/get/{subjectId}/{studentId}";
		public static final String GET_EXAM_RESULTS_BY_SUBJECT_ID_STUDENT_ID_TERM_ID = "/exams/results/list/get/{subjectId}/{studentId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID_SUBJECT_ID = "/exams/results/list/get/{classId}/{subjectId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID_SUBJECT_ID_TERM_ID = "/exams/results/list/get/{classId}/{subjectId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID_STUDENT_ID = "/exams/results/list/get/{classId}/{studentId}";
		public static final String GET_EXAM_RESULTS_BY_CLASS_ID_STUDENT_ID_TERM_ID = "/exams/results/list/get/{classId}/{studentId}/{termId}";
		public static final String GET_EXAM_RESULTS_BY_TERM_ID = "/exams/results/list/get/{termId}";


		//classes
		public static final String GET_ALL_CLASSES = "/classes/list";
		public static final String CREATE_CLASS = "/classes/new";
		public static final String UPDATE_CLASS = "/classes/update";
		public static final String DELETE_CLASS = "/classes/delete/{id}";
		public static final String GET_CLASS_BY_ID = "/classes/list/get/{id}";

		//		CLASS_HIERARCHIES
		public static final String GET_CLASS_HIERARCHIES = "/classHierarchy/list";
		public static final String UPDATE_CLASS_HIERARCHY = "/classHierarchy/update";
		public static final String CREATE_CLASS_HIERARCHY = "/classHierarchy/new";
		public static final String DELETE_CLASS_HIERARCHY = "/classHierarchy/delete/{id}";
		public static final String GET_CLASS_HIERARCHY_BY_LEVEL_ID = "/classHierarchy/list/get/{levelId}";

		//		CLASS_SUBJECTS
		public static final String GET_ALL_CLASS_SUBJECTS = "/class_subjects/list";
		public static final String ADD_CLASS_SUBJECT = "/class_subject/new";
		public static final String UPDATE_CLASS_SUBJECT = "/class_subject/update";
		public static final String DELETE_CLASS_SUBJECTS = "/class_subjects/delete/{id}";
		public static final String GET_ALL_CLASS_SUBJECTS_BY_CLASS_ID = "/class_subjects/list/get/{classId}";
		public static final String GET_ALL_CLASSES_BY_SUBJECT_ID = "/class_subject/classes/list/get/{subjectId}";
		public static final String GET_CLASS_SUBJECTS_BY_TEACHER_ID_CLASS_ID = "/class_subjects/list/get/{teacherId}/{class_id}";
		public static final String GET_ALL_SUBJECTS_BY_TEACHER_ID = "/class_subjects/subjects/list/get/{teacherId}";

		//Exam Settings
		public static final String GET_ALL_EXAM_SETTINGS = "/exam_settings/list";
		public static final String GET_EXAM_SETTINGS_BY_EXAM_ID = "/exam_settings/list/get/{examId}";
		public static final String GET_EXAM_SETTINGS_BY_TERM_ID = "/exam_settings/list/get/{termId}";
		public static final String GET_EXAM_SETTINGS_BY_CLASS_ID = "/exam_settings/list/get/{classId}";
		public static final String GET_EXAM_SETTINGS_BY_SUBJECT_ID = "/exam_settings/list/get/{subjectId}";
		public static final String GET_EXAM_SETTINGS_BY_CLASS_ID_TERM_ID = "/exam_settings/list/get/{classId}/{termId}";
		public static final String GET_EXAM_SETTINGS_BY_CLASS_ID_TERM_ID_EXAM_ID = "/exam_settings/list/get/{classId}/{termId}/{examId}";
		public static final String GET_EXAM_SETTINGS_BY_CLASS_ID_TERM_ID_EXAM_ID_SUBJECT_ID = "/exam_settings/list/get/{classId}/{termId}/{examId}/{subjectId}";

		//FeeCategory
		public static final String GET_ALL_FEE_CATEGORIES = "/fee_category/list";
		public static final String CREATE_FEE_CATEGORIES = "/fee_category/new";
		public static final String UPDATE_FEE_CATEGORIES = "/fee_category/update";
		public static final String DELETE_FEE_CATEGORIES = "/fee_category/delete/{id}";
		public static final String GET_FEE_CATEGORY_BY_ID = "/fee_category/list/get/{id}";

		//Fee sub category
		public static final String CREATE_FEE_SUB_CATEGORY = "/fee_sub_category/new";
		public static final String UPDATE_FEE_SUB_CATEGORY = "/fee_sub_category/update";
		public static final String DELETE_FEE_SUB_CATEGORY = "/fee_sub_category/delete/{id}";
		public static final String GET_ALL_FEE_SUB_CATEGORIES = "/fee_sub_category/list";
		public static final String GET_ALL_FEE_SUB_CATEGORIES_BY_CATEGORY_ID = "/fee_sub_category/list/get/{categoryId}";
		public static final String GET_ALL_FEE_SUB_CATEGORIES_BY_NAME = "/fee_sub_category/list/get/{subcategoryName}";

		//fee waiver
		public static final String GET_ALL_FEE_WAIVERS = "/fee_waiver/list";
		public static final String CREATE_FEE_WAIVERS = "/fee_waiver/new";
		public static final String UPDATE_FEE_WAIVERS = "/fee_waiver/update";
		public static final String DELETE_FEE_WAIVERS = "/fee_waiver/delete/{id}";
		public static final String GET_ALL_FEE_WAIVERS_BY_FEECATEGORY_ID_SUBCATEGORY_ID = "/fee_waiver/list/get/{categoryId}/{subCategoryId}";
		public static final String GET_ALL_FEE_WAIVERS_BY_FEECATEGORY_ID = "/fee_waiver/list/get/{categoryId}";
		public static final String GET_ALL_FEE_WAIVERS_BY_FEESUBCATEGORY_ID = "/fee_waiver/list/get/{subCategoryId}";
		public static final String GET_ALL_FEE_WAIVERS_BY_WAIVER_TYPE = "/fee_waiver/list/get/{waiverType}";
		public static final String GET_ALL_FEE_WAIVERS_BY_RATE_TYPE = "/fee_waiver/list/get/{rateType}";

		//fee Dates
		public static final String GET_ALL_FEE_DATES = "/fee_dates/list";
		public static final String CREATE_FEE_DATE = "/fee_dates/new";
		public static final String UPDATE_FEE_DATE = "/fee_dates/update";
		public static final String DELETE_FEE_DATE = "/fee_dates/delete/{id}";
		public static final String GET_ALL_ACTIVE_FEE_DATES = "/fee_dates/list/active";
		public static final String GET_ALL_FEE_DATES_BY_SUB_CATEGORY_ID = "/fee_dates/list/get/{subCategoryId}";
		//fee application levels
		public static final String CREATE_FEE_APPLICATION_LEVEL = "/fee_application_levels/new";
		public static final String UPDATE_FEE_APPLICATION_LEVEL = "/fee_application_levels/update";
		public static final String DELETE_FEE_APPLICATION_LEVEL = "/fee_application_levels/delete/{id}";
		public static final String GET_ALL_FEE_APPLICATION_LEVELS = "/fee_application_levels/list";
		public static final String GET_FEE_APPLICATION_LEVEL_BY_lEVEL_ID = "/fee_application_level/get/{id}";
		public static final String GET_ALL_ACTIVE_FEE_APPLICATION_LEVELS = "/fee_application_levels/list/get/active";


		// feel allocation

		public static final String CREATE_ADD_FEE_ALLOCATION = "/fee_allocation/new";
		public static final String UPDATE_FEE_ALLOCATION = "/fee_allocation/update";
		public static final String DELETE_FEE_ALLOCATION = "/fee_allocation/delete/{id}";
		public static final String GET_ALL_FEE_ALLOCATIONS = "/fee_allocations/list";
		public static final String GET_FEE_ALLOCATION_BY_ID = "/fee_allocations/get/{id}";
		public static final String GET_FEE_ALLOCATION_BY_FEE_SUB_CATEGORY = "/fee_allocations/get/{subcategoryId}";
		public static final String GET_FEE_ALLOCATION_BY_FEE_CATEGORY = "/fee_allocations/get/{feeCategoryId}";
		public static final String GET_FEE_ALLOCATION_BY_APPLICATION_LEVEL = "/fee_allocations/get/{applicationLevel}";
		public static final String GET_FEE_ALLOCATION_BY_APPLICATION_LEVEL_SUBCATEGORY_ID = "/fee_allocations/get/{applicationLevel}/{subcategoryId}";
		public static final String GET_ALL_ACTIVE_FEE_ALLOCATIONS = "/fee_allocations/list/get/active";

		//Grade Scale
		public static final String GET_ALL_GRADE_SCALES = "/grade_scale/list";
		public static final String CREATE_GRADE_SCALES = "/grade_scale/new";
		public static final String UPDATE_GRADE_SCALES = "/grade_scale/update";
		public static final String DELETE_GRADE_SCALES = "/grade_scale/delete/{id}";
		public static final String GET_GRADE_SCALES_BY_CLASS_ID = "/grade_scale/list/get/{classId}";
		public static final String GET_GRADE_SCALES_BY_EXAM_ID = "/grade_scale/list/get/{examId}";
		public static final String GET_GRADE_SCALES_BY_SUBJECT_ID = "/grade_scale/list/get/{subjectId}";
		public static final String GET_GRADE_SCALES_BY_CLASS_ID_EXAM_ID_SUBJECT_ID = "/grade_scale/list/get/{classId}/{examId}/{subjectId}";


		//guardian
		public static final String GET_ALL_GUARDIANS = "/guardians/list";
		public static final String CREATE_GUARDIANS = "/guardians/new";
		public static final String UPDATE_GUARDIANS = "/guardians/update";
		public static final String DELETE_GUARDIANS = "/guardians/delete/{id}";
		public static final String GET_GUARDIANS_BY_STUDENT_ID = "/guardians/list/get/{studentId}";

		//LIBRARY
		public static final String GET_ALL_LIB_BOOKS = "/library/books/list";
		public static final String CREATE_LIB_BOOK = "/library/books/new";
		public static final String UPDATE_LIB_BOOK = "/library/books/update";
		public static final String DELETE_LIB_BOOK = "/library/books/delete/{id}";
		public static final String GET_LIB_BOOK_BY_ISBN_NO = "/library/books/list/get/{isbnNo}";
		public static final String GET_LIB_BOOKS_BY_CATEGORY_ID = "/library/books/list/get/{categoryId}";

		public static final String GET_LIB_BOOK_CATEGORIES = "/library/categories/list";
		public static final String GET_LIB_BOOK_CATEGORY_BY_ID = "/library/categories/list/get/{id}";
		public static final String GET_LIB_BOOK_CATEGORIES_BY_CODE = "/library/categories/list/get/{code}";
		public static final String CREATE_LIB_BOOK_CATEGORY = "/library/categories/new";
		public static final String UPDATE_LIB_BOOK_CATEGORY = "/library/categories/update";
		public static final String DELETE_LIB_BOOK_CATEGORY = "/library/categories/delete/{id}";

		//lib_management
		public static final String GET_ALL_ISSUED_BOOKS = "/library/issued/list";
		public static final String GET_ALL_AVAILABLE_BOOKS = "/library/available/list";
		public static final String GET_ALL_OVERDUE_BOOKS = "/library/overdue/list";
		public static final String ISSUE_LIB_BOOK = "/library/issued/new";
		public static final String RETURN_LIB_BOOK = "/library/returned/new";
		public static final String DELETE_ISSUED_BOOK_RECORD_BY_ID = "/library/issued/delete/{id}";
		public static final String UPDATE_ISSUED_BOOK_RECORD = "/library/issued/update";

		//Parents

		public static final String GET_ALL_PARENTS = "/parents/list";
		public static final String CREATE_PARENT = "/parents/new";
		public static final String UPDATE_PARENT = "/parents/update";
		public static final String DELETE_PARENT = "/parents/delete/{id}";
		public static final String GET_PARENTS_BY_STUDENT_ID = "/parents/list/get/{studentId}";

		//streams
		public static final String GET_ALL_STREAMS = "/streams/list";
		public static final String CREATE_STREAM = "/streams/new";
		public static final String UPDATE_STREAM = "/streams/update";
		public static final String DELETE_STREAM = "/streams/delete/{id}";
		public static final String GET_STREAM_BY_ID = "/streams/list/get/{id}";
		public static final String GET_CLASSES_BY_STREAM_ID = "/streams/list/get/{streamId}";

		//Students

		public static final String GET_ALL_STUDENTS = "/students/list";
		public static final String CREATE_STUDENT = "/students/new";
		public static final String UPDATE_STUDENT = "/students/update";
		public static final String DELETE_STUDENT = "/students/delete/{id}";
		public static final String GET_STUDENTS_BY_CLASS_ID = "/students/list/get/{classId}";
		public static final String GET_STUDENTS_BY_STREAM_ID = "/students/list/get/{streamId}";

		//subjects

		public static final String GET_ALL_SUBJECTS = "/subjects/list";
		public static final String CREATE_SUBJECT = "/subjects/new";
		public static final String UPDATE_SUBJECT = "/subjects/update";
		public static final String DELETE_SUBJECT = "/subjects/delete/{id}";
		public static final String GET_SUBJECTS_BY_CLASS_ID = "/subjects/list/get/{classId}";
		public static final String GET_SUBJECTS_BY_STREAM_ID = "/subjects/list/get/{streamId}";
		public static final String GET_SUBJECTS_BY_SUBJECT_NAME = "/subjects/list/get/{subjectName}";
		public static final String GET_SUBJECT_BY_SUBJECT_ID = "/subjects/list/get/{subjectId}";
		public static final String GET_SUBJECT_BY_SUBJECT_CODE = "/subjects/list/get/{subjectCode}";

		// reports

		public static final String GET_ALL_REPORTS = "/reports/list";
		public static final String CREATE_REPORT = "/reports/new";
		public static final String UPDATE_REPORT = "/reports/update";
		public static final String DELETE_REPORT = "/reports/delete/{id}";
		public static final String GET_REPORTS_BY_REPORT_ID = "/reports/list/get/{Id}";
		public static final String GET_REPORTS_BY_REPORT_CODE = "/reports/list/get/{reportCode}";
		public static final String GET_REPORTS_BY_REPORT_NAME = "/reports/list/get/{reportName}";
		// teachers

		public static final String GET_ALL_TEACHERS = "/teachers/list";
		public static final String CREATE_TEACHER = "/teachers/new";
		public static final String UPDATE_TEACHER = "/teachers/update";
		public static final String DELETE_TEACHER = "/teachers/delete/{id}";
		public static final String GET_TEACHER_BY_TEACHER_ID = "/teachers/list/get/{teacherId}";
		public static final String GET_TEACHERS_BY_SUBJECT_ID = "/teachers/list/get/{subjectId}";
		public static final String GET_TEACHERS_BY_CLASS_ID = "/teachers/list/get/{classId}";

		//academic terms
		public static final String GET_ALL_TERMS = "/terms/list";
		public static final String CREATE_TERM = "/terms/new";
		public static final String UPDATE_TERM = "/terms/update";
		public static final String DELETE_TERM = "/terms/delete/{id}";
		public static final String GET_TERM_BY_ID = "/terms/list/get/{id}";
		public static final String GET_TERMS_BY_ACADEMIC_YEAR_ID = "/terms/list/get/{academicYearId}";
		public static final String GET_TERMS_BETWEEN_DATES = "/terms/list/get/{wef}/{wet}";
		public static final String GET_ACADEMIC_YEAR_BY_TERM_ID = "/terms/list/get/{termId}";

		//		Academic Years
		public static final String GET_ALL_ACADEMIC_YEARS = "/academic_years/list";
		public static final String CREATE_ACADEMIC_YEAR = "/academic_years/new";
		public static final String UPDATE_ACADEMIC_YEAR = "/academic_years/update";
		public static final String DELETE_ACADEMIC_YEAR = "/academic_years/delete/{id}";
		public static final String GET_ACADEMIC_YEAR_BY_ID = "/academic_years/list/get/{id}";

}
