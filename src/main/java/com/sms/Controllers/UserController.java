package com.sms.Controllers;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.model.UserBean;
import com.sms.model.UserRoleBean;
import com.sms.service.RoleService;
import com.sms.service.UserService;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static com.sms.Controllers.Constants.UrlConstants.*;

/**
 * Created by Steve on 1/8/2017.
 */
@Controller
public class UserController {
		Logger logger = Logger.getLogger(UserController.class);
		SessionFactory factory;
		@Autowired
		private UserService userService;
		@Autowired
		private RoleService roleService;
		@Autowired
		private HttpServletRequest request;


		@RequestMapping(value = GET_ALL_USERS, produces = {"application/json"}, method = RequestMethod.GET)
		public
		@ResponseBody
		List<UserBean> getUsers() throws HTTPException {
				logger.info("UserController : getUsers() method Invoked");
				List<UserBean> users = new ArrayList<>();
				try {
						users = userService.getAllUsers();
				} catch (Exception ex) {
						ex.printStackTrace();
				}
				return users;
		}

		@RequestMapping(value = GET_USER_ROLES, produces = {"application/json"}, method = RequestMethod.GET)
		public
		@ResponseBody
		List<UserRoleBean> getAllRoles() throws HTTPException {
				logger.info("UserController : getAllRoles() method Invoked");
				List<UserRoleBean> userRoles = new ArrayList<>();
				try {
						userRoles = roleService.getAllUserRoles();
				} catch (Exception ex) {
						ex.printStackTrace();
				}
				return userRoles;
		}

		@RequestMapping(
						value = CREATE_USER,
						method = RequestMethod.POST,
						consumes = {"application/json"},
						produces = {"application/json"})
		public
		@ResponseBody
		UserBean createUser(@RequestBody UserBean user) throws NoSuchAlgorithmException {
				logger.info("UserController : createUser() method Invoked");
				UserBean createdUser = new UserBean();
				try {
						createdUser = userService.createUser(user);
				} catch (Exception ex) {
						ex.printStackTrace();
				}
				return createdUser;
		}

		@RequestMapping(
						value = UPDATE_USER,
						method = RequestMethod.POST,
						consumes = {"application/json"},
						produces = {"application/json"})
		public
		@ResponseBody
		UserBean updateUser(@RequestBody UserBean user) throws NoSuchAlgorithmException {
				logger.info("UserController : updateUser() method Invoked");
				UserBean updatedUser = new UserBean();
				try {
						updatedUser = userService.updateUser(user);
				} catch (Exception ex) {
						ex.printStackTrace();
				}
				return updatedUser;
		}

		@RequestMapping(
						value = DELETE_USER,
						method = RequestMethod.POST,
						produces = {"application/json"})
		public
		@ResponseBody
		Boolean deleteUser(@PathVariable int id) {
				logger.info("UserController : deleteUser() method Invoked");
				Boolean deleted = false;
				try {
						deleted = userService.deleteUser(id);
				} catch (Exception ex) {
						ex.printStackTrace();
				}
				return deleted;
		}

		@RequestMapping(
						value = GET_USER_BY_ID,
						method = RequestMethod.GET,
						produces = {"application/json"})
		public
		@ResponseBody
		UserBean userBean(@PathVariable int id) throws SmsCustomException {
				UserBean userBean;
				try {
						userBean = userService.getUserById(id);
						return userBean;
				} catch (Exception ex) {
						throw new SmsCustomException("Error fetching user details", HttpStatus.FORBIDDEN.value());

				}
		}
}

