package com.sms.ExceptionHandlers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Steve on 1/13/2017.
 */
@ControllerAdvice
public class SmsCustomExceptionsControllerAdviser {
		@ExceptionHandler(SmsCustomException.class)
		public ResponseEntity<CustomExceptionResponse> exceptionHandler(SmsCustomException ex) {
				CustomExceptionResponse error = new CustomExceptionResponse();
				error.setErrorCode(ex.getExceptionCode());
				error.setMessage(ex.getExceptionMessage());
				return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		@ExceptionHandler(SmsDatabaseExceptions.class)
		public ResponseEntity<DbMessage> exceptionHandler(SmsDatabaseExceptions ex) {
				DbMessage error = new DbMessage();
				error.setError(ex.getError());
				error.setExceptionMessage(ex.getMessage());
				return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		@ExceptionHandler(Exception.class)
		public ResponseEntity<String> exceptionHandler(Exception ex) {
				return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
}
