package com.sms.ExceptionHandlers;

/**
 * Created by Steve on 1/13/2017.
 */

public class DbMessage {

		String exceptionMessage;
		String error;

		public String getError() {
				return error;
		}

		public void setError(String error) {
				this.error = error;
		}

		public String getExceptionMessage() {
				return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
				this.exceptionMessage = exceptionMessage;
		}

}
