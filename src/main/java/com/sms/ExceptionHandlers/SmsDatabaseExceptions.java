package com.sms.ExceptionHandlers;

/**
 * Created by Steve on 1/17/2017.
 */
public class SmsDatabaseExceptions extends Exception {
		private static final long serialVersionUID = -6513666078709813858L;
		private String exceptionMessage;
		private String error;

		public SmsDatabaseExceptions(String exceptionMessage, String error) {
				super();
				this.exceptionMessage = exceptionMessage;
				this.error = error;
		}

		public String getError() {
				return error;
		}

		public void setError(String error) {
				this.error = error;
		}

		public String getExceptionMessage() {
				return exceptionMessage;
		}


		public void setExceptionMessage(String exceptionMessage) {
				this.exceptionMessage = exceptionMessage;
		}
}
