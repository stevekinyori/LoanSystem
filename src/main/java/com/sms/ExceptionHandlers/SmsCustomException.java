package com.sms.ExceptionHandlers;

/**
 * Created by Steve on 1/13/2017.
 */
public class SmsCustomException extends Exception {
		private static final long serialVersionUID = -6513666078709813858L;
		private String exceptionMessage;
		private int exceptionCode;


		public SmsCustomException(String exceptionMessage, int exceptionCode) {
				this.exceptionMessage = exceptionMessage;
				this.exceptionCode = exceptionCode;
		}


		public String getExceptionMessage() {
				return exceptionMessage;
		}


		public void setExceptionMessage(String exceptionMessage) {
				this.exceptionMessage = exceptionMessage;
		}

		public int getExceptionCode() {
				return exceptionCode;
		}

		public void setExceptionCode(int exceptionCode) {
				this.exceptionCode = exceptionCode;
		}

}
