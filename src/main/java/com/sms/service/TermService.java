package com.sms.service;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.AcademicYearBean;
import com.sms.model.TermBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface TermService {
		TermBean createTerm(TermBean termBean);

		TermBean updateTerm(TermBean termBean);

		boolean deleteTermbean(int termId);

		List<TermBean> getAllTerms();

		TermBean getTermByTermId(int termId);

		List<TermBean> getTermByAcademicYearId(int academicYearId);

		List<TermBean> getTermsBetweenDates(Date wef, Date wet);

		List<AcademicYearBean> getAcademicYearsByTermId(int termId);

		List<AcademicYearBean> getAllAcademicyears();

		AcademicYearBean createAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions;

		boolean deleteAcademicYear(int id) throws SmsDatabaseExceptions;

		AcademicYearBean updateAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions;

		AcademicYearBean getAcademicYearById(Integer id) throws SmsDatabaseExceptions;

		List<AcademicYearBean> getAcademicYearsBetweenTwoDates(Date startDate, Date endDate) throws SmsDatabaseExceptions;

}
