package com.sms.service;

import com.sms.model.RolePrivilegeBean;
import com.sms.model.UserRoleBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/11/2017.
 */

@Component
public interface RoleService {
		List<UserRoleBean> getAllUserRoles();

		List<RolePrivilegeBean> getAllPrivileges();

		List<RolePrivilegeBean> getAllRolePrivileges();

		List<RolePrivilegeBean> getAllRolePrivilegesByRoleId(int roleId);

		RolePrivilegeBean addRolePrivilege(RolePrivilegeBean rolePrivilegeBean);

		RolePrivilegeBean updateRolePrivilege(RolePrivilegeBean rolePrivilegeBean);

		Boolean deleteRolePrivilege(int id);

		List<UserRoleBean> getAllUserRolesByUserId();

		UserRoleBean createRole(UserRoleBean userRoleBean);

		UserRoleBean updateUserRole(UserRoleBean userRoleBean);

		Boolean deleteUserRole(int id);

}
