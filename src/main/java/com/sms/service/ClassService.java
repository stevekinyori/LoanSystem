package com.sms.service;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/13/2017.
 */

@Component
public interface ClassService {
		List<ClassBean> getAllClasses() throws SmsDatabaseExceptions;

		ClassBean updateClass(ClassBean classBean) throws SmsDatabaseExceptions;

		ClassBean createClass(ClassBean classBean) throws SmsDatabaseExceptions;

		boolean deleteClass(int id) throws SmsDatabaseExceptions;

		List<ClassBean> getClassesByStatus(String status) throws SmsCustomException, SmsDatabaseExceptions;

		ClassBean getClassById(Integer classId) throws SmsDatabaseExceptions;

		List<ClassHierarchyBean> getClassHierarchies() throws SmsDatabaseExceptions;

		List<ClassHierarchyBean> getClassHierarchiesBylevelId(int levelId) throws SmsDatabaseExceptions;

		ClassHierarchyBean createClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions;

		ClassHierarchyBean updateClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions;

		boolean deleteClassHierarchy(int id) throws SmsDatabaseExceptions;

		List<ClassSubjectBean> getAllClassSubjects();

		List<ClassSubjectBean> getAllClassSubjectsBySubjectId(int subjectId);

		List<ClassSubjectBean> getAllClassSubjectsByClassId(int classId);

		List<ClassSubjectBean> getAllClassSubjectsByTeacherId(int teacherId);

		List<ClassSubjectBean> getAllClassSubjectsByTeacherIdClassId(int teacherId, int classId);

		ClassSubjectBean addClassSubject(ClassSubjectBean classSubjectBean);

		ClassSubjectBean updateClassSubject(ClassSubjectBean classSubjectBean);

		boolean deleteClassSubject(int id);

		List<ClassTeacherBean> getAllClassTeachers();

		List<ClassTeacherBean> getAllClassTeachersByClassId(int classId);

		ClassTeacherBean addClassTeacher(ClassTeacherBean classTeacherBean);

		ClassTeacherBean updateClassTeacher(ClassTeacherBean classTeacherBean);

		boolean deleteClassTeacher(int id);

		StreamBean createStream(StreamBean streamBean);

		StreamBean updateStream(StreamBean streamBean);

		boolean deleteStream(int streamId);

		StreamBean getStreamById(int streamId);

		List<ClassBean> getAllClassesByStreamId(int streamId);

		List<StreamBean> getAllStreams();
}
