package com.sms.service;

import com.sms.model.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface FeeService {
		FeeAllocationBean addFeeAllocaton(FeeAllocationBean feeAllocationBean);

		FeeAllocationBean updateFeeAllocaton(FeeAllocationBean feeAllocationBean);

		boolean deleteFeeAllocation(int id);

		List<FeeAllocationBean> getAllFeeAllocations();

		List<FeeAllocationBean> getAllActiveFeeAllocations();

		FeeAllocationBean getFeeAllocationById(int id);

		List<FeeAllocationBean> getFeeAllocationByFeeSubCategoryId(int subCategoryId);

		List<FeeAllocationBean> getFeeAllocationByFeeCategoryId(int categoryId);

		List<FeeAllocationBean> getFeeAllocationByApplicationLevelId(int applicationLevelId);

		List<FeeAllocationBean> getFeeAllocationByApplicationLevelIdSubCategoryId(int applicationLevelId, int subCategoryId);

		FeeApplicationLevelBean createFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean);

		FeeApplicationLevelBean updateFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean);

		boolean deleteFeeApplicationLevel(int levelId);

		List<FeeApplicationLevelBean> getAllFeeApplicationLevels();

		FeeApplicationLevelBean getFeeApplicationLevelByLevelId(int levelId);

		List<FeeApplicationLevelBean> getAllActiveFeeApplicationLevels();

		List<FeeDatesBean> getAllFeeDates();

		List<FeeDatesBean> getAllActiveFeeDates();

		List<FeeDatesBean> getAllFeeDatesBySubCategoryId(int subCategoryId);

		FeeDatesBean createFeedate(FeeDatesBean feeDatesBean);

		FeeDatesBean updateFeedate(FeeDatesBean feeDatesBean);

		boolean deleteFeeDates(int id);

		List<FeesCategoryBean> getAllFeeCategories();

		FeesCategoryBean createFeeCategory(FeesCategoryBean feesCategoryBean);

		FeesCategoryBean updateFeeCategory(FeesCategoryBean feesCategoryBean);

		boolean deleteFeeCategory(int id);

		FeesCategoryBean getFeeCategoryById(int id);

		FeeSubcategoryBean createFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean);

		FeeSubcategoryBean updateFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean);

		boolean deleteFeeSubCategory(int id);

		List<FeeSubcategoryBean> getAllFeeSubCategories();

		List<FeeSubcategoryBean> getAllFeeSubCategoriesByCategoryId(int id);

		List<FeeSubcategoryBean> getAllFeeSubCategoriesByName(String name);

		List<FeeWaiverBean> getAllFeeWaivers();

		FeeWaiverBean createFeeWaiver(FeeWaiverBean feeWaiverBean);

		FeeWaiverBean updateFeeWaiver(FeeWaiverBean feeWaiverBean);

		boolean deleteFeeWaiver(int id);

		List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryId(int categoryId);

		List<FeeWaiverBean> getAllFeeWaiversByRateType(String rateType);

		List<FeeWaiverBean> getAllFeeWaiversByWaiverType(String waiverType);

		List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryIdFeeSubCategoryId(int categoryId, int subCategoryId);

		List<FeeWaiverBean> getAllFeeWaiversByFeeSubCategoryId(int subCategoryId);
}
