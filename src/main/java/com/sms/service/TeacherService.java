package com.sms.service;

import com.sms.model.TeacherBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface TeacherService {
		TeacherBean createTeacher(TeacherBean teacherBean);

		TeacherBean updateTeacher(TeacherBean teacherBean);

		boolean deleteTeacher(int teacherId);

		List<TeacherBean> getAllTeachers();

		TeacherBean getTeacherByTeacherId(int teacherId);

		List<TeacherBean> getTeacherBySubjectId(int subjectId);

		TeacherBean getTeacherByClassId(int classId);
}
