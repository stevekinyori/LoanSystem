package com.sms.service;

import com.sms.model.ExamBean;
import com.sms.model.ExamResultBean;
import com.sms.model.ExamSettingsBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface ExamService {
		List<ExamBean> getAllSchoolExams();

		List<ExamBean> getAllSchoolExamsByStatus(String status);

		ExamBean createExam(ExamBean examBean);

		ExamBean updateExam(ExamBean examBean);

		boolean deleteExam(int examId);

		ExamResultBean addExamResult(ExamResultBean examResultBean);

		ExamResultBean updateExamResult(ExamResultBean examResultBean);

		boolean deleteExamResult(int id);

		List<ExamResultBean> getExamResultsByExamId(int examId);

		List<ExamResultBean> getExamResultsByExamIdTermId(int examId, int termId);

		List<ExamResultBean> getExamResultsByExamIdClassId(int examId, int classId);

		List<ExamResultBean> getExamResultsByExamIdClassIdTermId(int examId, int classId, int termId);

		List<ExamResultBean> getExamResultsByExamIdClassIdStudentId(int examId, int classId, int studentId);

		List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdTermId(int examId, int classId, int studentId, int termId);

		List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectId(int examId, int classId, int studentId, int subjectId);

		List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectIdTermId(int examId, int classId, int studentId, int subjectId, int termId);

		List<ExamResultBean> getExamResultsByStudentId(int studentId);

		List<ExamResultBean> getExamResultsByClassId(int classId);

		List<ExamResultBean> getExamResultsByClassIdStudentId(int classId, int studentId);

		List<ExamResultBean> getExamResultsByClassIdSubjectId(int classId, int subjectId);

		List<ExamResultBean> getExamResultsByClassIdSubjectIdTermId(int classId, int subjectId, int termId);

		List<ExamResultBean> getExamResultsByClassIdTermId(int classId, int termId);

		List<ExamResultBean> getExamResultsByStudentIdTermId(int studentId, int termId);

		List<ExamResultBean> getExamResultsBySubjectId(int subjectId);

		List<ExamResultBean> getExamResultsBySubjetIdTermId(int subjectId, int termId);

		List<ExamResultBean> getExamResultsBySubjectIdStudentId(int subjectId, int studentId);

		List<ExamResultBean> getExamResultsBySubjetIdStudentIdTermId(int subjectId, int studentId, int termId);

		List<ExamResultBean> getExamResultsByClassIdStudentIdTermId(int classId, int studentId, int termId);

		List<ExamResultBean> getExamResultsByTermId(int termId);

		List<ExamSettingsBean> getAllExamSettings();

		List<ExamSettingsBean> getAllExamSettingsByExamId(int examId);

		List<ExamSettingsBean> getAllExamSettingsByTermId(int termId);

		List<ExamSettingsBean> getAllExamSettingsByClassId(int classId);

		List<ExamSettingsBean> getAllExamSettingsBySubjectId(int subjectId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermId(int classId, int termId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamId(int classId, int termId, int examId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamIdSubjectId(int classId, int termId, int examId, int subjectId);

}
