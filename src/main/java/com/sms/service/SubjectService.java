package com.sms.service;

import com.sms.model.SubjectBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface SubjectService {
		SubjectBean createSubject(SubjectBean subjectBean);

		SubjectBean updateSubject(SubjectBean subjectBean);

		boolean deleteSubject(int subjectId);

		List<SubjectBean> getAllSubjects();

		List<SubjectBean> getSubjectsByClassId(int classId);

		List<SubjectBean> getSubjectsByStreamId(int streamId);

		List<SubjectBean> getSubjectsBySubjectName(String subjectName);

		List<SubjectBean> getSubjectsBySubjectID(int subjectId);

		List<SubjectBean> getSubjectsBySubjectCode(String subjectCode);
}
