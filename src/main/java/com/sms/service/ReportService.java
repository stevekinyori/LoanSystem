package com.sms.service;

import com.sms.model.SystemReportBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface ReportService {
		SystemReportBean createReport(SystemReportBean systemReportBean);

		SystemReportBean updateReport(SystemReportBean systemReportBean);

		boolean deleteSystemReport(int reportId);

		List<SystemReportBean> getAllSystemReports();

		List<SystemReportBean> getAllSystemReportsByReportId(int reportId);

		List<SystemReportBean> getAllSystemReportsByReportCode(int reportCode);

		List<SystemReportBean> getAllSystemReportsByReportName(int reportName);
}
