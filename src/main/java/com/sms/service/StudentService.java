package com.sms.service;

import com.sms.model.StudentBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface StudentService {
		StudentBean createStudent(StudentBean studentBean);

		StudentBean updateStudent(StudentBean studentBean);

		boolean deleteStudent(int studentId);

		List<StudentBean> getAllStudents();

		List<StudentBean> getStudentsByClassId(int classId);

		List<StudentBean> getStudentsByStreamId(int streamId);
}
