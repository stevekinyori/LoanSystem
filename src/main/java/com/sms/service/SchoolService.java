package com.sms.service;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.model.SchoolBean;
import org.springframework.stereotype.Component;

/**
 * Created by Steve on 1/13/2017.
 */
@Component
public interface SchoolService {
		SchoolBean createSchool(SchoolBean schoolBean) throws SmsCustomException;

		SchoolBean updateSchoolDetails(SchoolBean schoolBean) throws SmsCustomException;

		SchoolBean getSchoolDetails() throws SmsCustomException;
}
