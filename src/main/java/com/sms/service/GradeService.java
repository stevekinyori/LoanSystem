package com.sms.service;

import com.sms.model.GradeScaleBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface GradeService {
		List<GradeScaleBean> getAllGradeScales();

		GradeScaleBean createGradeScale(GradeScaleBean gradeScaleBean);

		GradeScaleBean updateGradeScale(GradeScaleBean gradeScaleBean);

		boolean deleteGradeScale(int id);

		List<GradeScaleBean> getGradeScalesByClassId(int classId);

		List<GradeScaleBean> getGradeScalesByExamId(int examId);

		List<GradeScaleBean> getGradeScalesBySubjectId(int subjectId);

		List<GradeScaleBean> getGradeScalesByClassIdExamIdSubjectId(int classId, int examId, int subjectId);

}
