package com.sms.service.ServiceImpl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.UserDao;
import com.sms.model.ClassBean;
import com.sms.model.Common.UserDetails;
import com.sms.model.UserBean;
import com.sms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by dhis on 1/5/16.
 */
@Service
public class UserServiceImpl implements UserService {
		@Autowired
		private UserDao userDao;

		public void setUserDao(UserDao userDao) {
				this.userDao = userDao;
		}

		@Override
		public List<UserBean> getAllUsers() {
				return userDao.getAllUsers();
		}

		@Override
		public UserBean getUserByUsername(String userName) {
				return userDao.getUserByUsername(userName);
		}

		@Override
		public UserBean getUserById(int Id) {
				return userDao.getUserById(Id);
		}

		@Override
		public ClassBean getUserClass(BigDecimal userId) {
				return userDao.getUserClass(userId);
		}

		@Override
		public Boolean validateUserPassword(String Username, String Password) throws NoSuchAlgorithmException, SmsDatabaseExceptions {
				return userDao.validateUserPassword(Username, Password);
		}

		@Override
		public Boolean validateUserToken(String Username, String token) {
				return userDao.validateUserToken(Username, token);
		}

		@Override
		public UserBean createUser(UserBean userBean) throws NoSuchAlgorithmException {
				return userDao.createUser(userBean);
		}

		@Override
		public boolean deleteUser(int id) {
				return userDao.deleteUser(id);
		}

		@Override
		public UserDetails loadUserByUsername(String username) {
				return userDao.loadUserByUsername(username);
		}

		@Override
		public UserBean updateUser(UserBean userBean) {
				return userDao.updateUser(userBean);
		}
}
