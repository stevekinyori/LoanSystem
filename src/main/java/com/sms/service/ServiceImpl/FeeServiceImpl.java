package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.*;
import com.sms.model.*;
import com.sms.service.FeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/26/2017.
 */
@Service
public class FeeServiceImpl implements FeeService {
		@Autowired
		private FeeAllocationDao feeAllocationDao;
		@Autowired
		private FeesCategoryDao feesCategoryDao;
		@Autowired
		private FeeSubcategoryDao feeSubcategoryDao;
		@Autowired
		private FeeApplicationLevelDao feeApplicationLevelDao;
		@Autowired
		private FeeDatesDao feeDatesDao;

		@Autowired
		private FeeWaiverDao feeWaiverDao;

		@Override
		public FeeAllocationBean addFeeAllocaton(FeeAllocationBean feeAllocationBean) {
				return feeAllocationDao.addFeeAllocaton(feeAllocationBean);
		}

		@Override
		public FeeAllocationBean updateFeeAllocaton(FeeAllocationBean feeAllocationBean) {
				return feeAllocationDao.updateFeeAllocaton(feeAllocationBean);
		}

		@Override
		public boolean deleteFeeAllocation(int id) {
				return feeAllocationDao.deleteFeeAllocation(id);
		}

		@Override
		public List<FeeAllocationBean> getAllFeeAllocations() {
				return feeAllocationDao.getAllFeeAllocations();
		}

		@Override
		public List<FeeAllocationBean> getAllActiveFeeAllocations() {
				return feeAllocationDao.getAllActiveFeeAllocations();
		}

		@Override
		public FeeAllocationBean getFeeAllocationById(int id) {
				return feeAllocationDao.getFeeAllocationById(id);
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByFeeSubCategoryId(int subCategoryId) {
				return feeAllocationDao.getFeeAllocationByFeeSubCategoryId(subCategoryId);
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByFeeCategoryId(int categoryId) {
				return feeAllocationDao.getFeeAllocationByFeeCategoryId(categoryId);
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByApplicationLevelId(int applicationLevelId) {
				return feeAllocationDao.getFeeAllocationByApplicationLevelId(applicationLevelId);
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByApplicationLevelIdSubCategoryId(int applicationLevelId, int subCategoryId) {
				return feeAllocationDao.getFeeAllocationByApplicationLevelIdSubCategoryId(applicationLevelId, subCategoryId);
		}

		@Override
		public FeeApplicationLevelBean createFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean) {
				return feeApplicationLevelDao.createFeeApplicationLevel(feeApplicationLevelBean);
		}

		@Override
		public FeeApplicationLevelBean updateFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean) {
				return feeApplicationLevelDao.updateFeeApplicationLevel(feeApplicationLevelBean);
		}

		@Override
		public boolean deleteFeeApplicationLevel(int levelId) {
				return feeApplicationLevelDao.deleteFeeApplicationLevel(levelId);
		}

		@Override
		public List<FeeApplicationLevelBean> getAllFeeApplicationLevels() {
				return feeApplicationLevelDao.getAllFeeApplicationLevels();
		}

		@Override
		public FeeApplicationLevelBean getFeeApplicationLevelByLevelId(int levelId) {
				return feeApplicationLevelDao.getFeeApplicationLevelByLevelId(levelId);
		}

		@Override
		public List<FeeApplicationLevelBean> getAllActiveFeeApplicationLevels() {
				return feeApplicationLevelDao.getAllActiveFeeApplicationLevels();
		}

		@Override
		public List<FeeDatesBean> getAllFeeDates() {
				return feeDatesDao.getAllFeeDates();
		}

		@Override
		public List<FeeDatesBean> getAllActiveFeeDates() {
				return feeDatesDao.getAllActiveFeeDates();
		}

		@Override
		public List<FeeDatesBean> getAllFeeDatesBySubCategoryId(int subCategoryId) {
				return feeDatesDao.getAllFeeDatesBySubCategoryId(subCategoryId);
		}

		@Override
		public FeeDatesBean createFeedate(FeeDatesBean feeDatesBean) {
				return feeDatesDao.createFeedate(feeDatesBean);
		}

		@Override
		public FeeDatesBean updateFeedate(FeeDatesBean feeDatesBean) {
				return feeDatesDao.updateFeedate(feeDatesBean);
		}

		@Override
		public boolean deleteFeeDates(int id) {
				return feeDatesDao.deleteFeeDates(id);
		}

		@Override
		public List<FeesCategoryBean> getAllFeeCategories() {
				return feesCategoryDao.getAllFeeCategories();
		}

		@Override
		public FeesCategoryBean createFeeCategory(FeesCategoryBean feesCategoryBean) {
				return feesCategoryDao.createFeeCategory(feesCategoryBean);
		}

		@Override
		public FeesCategoryBean updateFeeCategory(FeesCategoryBean feesCategoryBean) {
				return feesCategoryDao.updateFeeCategory(feesCategoryBean);
		}

		@Override
		public boolean deleteFeeCategory(int id) {
				return feesCategoryDao.deleteFeeCategory(id);
		}

		@Override
		public FeesCategoryBean getFeeCategoryById(int id) {
				return feesCategoryDao.getFeeCategoryById(id);
		}

		@Override
		public FeeSubcategoryBean createFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean) {
				return feeSubcategoryDao.createFeeSubCategory(feeSubcategoryBean);
		}

		@Override
		public FeeSubcategoryBean updateFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean) {
				return feeSubcategoryDao.updateFeeSubCategory(feeSubcategoryBean);
		}

		@Override
		public boolean deleteFeeSubCategory(int id) {
				return feeSubcategoryDao.deleteFeeSubCategory(id);
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategories() {
				return feeSubcategoryDao.getAllFeeSubCategories();
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategoriesByCategoryId(int id) {
				return feeSubcategoryDao.getAllFeeSubCategoriesByCategoryId(id);
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategoriesByName(String name) {
				return feeSubcategoryDao.getAllFeeSubCategoriesByName(name);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaivers() {
				return feeWaiverDao.getAllFeeWaivers();
		}

		@Override
		public FeeWaiverBean createFeeWaiver(FeeWaiverBean feeWaiverBean) {
				return feeWaiverDao.createFeeWaiver(feeWaiverBean);
		}

		@Override
		public FeeWaiverBean updateFeeWaiver(FeeWaiverBean feeWaiverBean) {
				return feeWaiverDao.updateFeeWaiver(feeWaiverBean);
		}

		@Override
		public boolean deleteFeeWaiver(int id) {
				return feeWaiverDao.deleteFeeWaiver(id);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryId(int categoryId) {
				return feeWaiverDao.getAllFeeWaiversByFeeSubCategoryId(categoryId);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByRateType(String rateType) {
				return feeWaiverDao.getAllFeeWaiversByRateType(rateType);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByWaiverType(String waiverType) {
				return feeWaiverDao.getAllFeeWaiversByWaiverType(waiverType);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryIdFeeSubCategoryId(int categoryId, int subCategoryId) {
				return feeWaiverDao.getAllFeeWaiversByFeeCategoryIdFeeSubCategoryId(categoryId, subCategoryId);
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeSubCategoryId(int subCategoryId) {
				return feeWaiverDao.getAllFeeWaiversByFeeSubCategoryId(subCategoryId);
		}
}
