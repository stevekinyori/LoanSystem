package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.StudentDao;
import com.sms.model.StudentBean;
import com.sms.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class StudentServiceImpl implements StudentService {
		@Autowired
		private StudentDao studentDao;
		@Override
		public StudentBean createStudent(StudentBean studentBean) {
				return studentDao.createStudent(studentBean);
		}

		@Override
		public StudentBean updateStudent(StudentBean studentBean) {
				return studentDao.updateStudent(studentBean);
		}

		@Override
		public boolean deleteStudent(int studentId) {
				return studentDao.deleteStudent(studentId);
		}

		@Override
		public List<StudentBean> getAllStudents() {
				return studentDao.getAllStudents();
		}

		@Override
		public List<StudentBean> getStudentsByClassId(int classId) {
				return studentDao.getStudentsByClassId(classId);
		}

		@Override
		public List<StudentBean> getStudentsByStreamId(int streamId) {
				return studentDao.getStudentsByStreamId(streamId);
		}
}
