package com.sms.service.ServiceImpl;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.dao.interfaces.SchoolDao;
import com.sms.model.SchoolBean;
import com.sms.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Steve on 1/13/2017.
 */
@Service
public class SchoolServiceImpl implements SchoolService {

		@Autowired
		private SchoolDao schoolDao;

		@Override
		public SchoolBean createSchool(SchoolBean schoolBean) throws SmsCustomException {
				return schoolDao.createSchool(schoolBean);
		}

		@Override
		public SchoolBean updateSchoolDetails(SchoolBean schoolBean) throws SmsCustomException {
				return schoolDao.updateSchoolDetails(schoolBean);
		}

		@Override
		public SchoolBean getSchoolDetails() throws SmsCustomException {
				return schoolDao.getSchoolDetails();
		}
}
