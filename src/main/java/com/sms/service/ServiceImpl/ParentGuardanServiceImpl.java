package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.GuardianDao;
import com.sms.dao.interfaces.ParentDao;
import com.sms.model.GuardianBean;
import com.sms.model.ParentBean;
import com.sms.service.ParentGuardianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class ParentGuardanServiceImpl implements ParentGuardianService {
		@Autowired
		private GuardianDao guardianDao;
		@Autowired
		private ParentDao parentDao;
		@Override
		public List<GuardianBean> getAllGuardians() {
				return guardianDao.getAllGuardians();
		}

		@Override
		public GuardianBean createGuardian(GuardianBean guardianBean) {
				return guardianDao.createGuardian(guardianBean);
		}

		@Override
		public GuardianBean updateGuardian(GuardianBean guardianBean) {
				return guardianDao.updateGuardian(guardianBean);
		}

		@Override
		public boolean deleteGuardian(int guardianId) {
				return guardianDao.deleteGuardian(guardianId);
		}

		@Override
		public List<GuardianBean> getGuardianByStudentId(int studentId) {
				return guardianDao.getGuardianByStudentId(studentId);
		}

		@Override
		public ParentBean createParent(ParentBean parentBean) {
				return parentDao.createParent(parentBean);
		}

		@Override
		public ParentBean updateParent(ParentBean parentBean) {
				return parentDao.updateParent(parentBean);
		}

		@Override
		public boolean deleteParentRecord(int id) {
				return parentDao.deleteParentRecord(id);
		}

		@Override
		public List<ParentBean> getAllParentsBystudentId(int studentId) {
				return parentDao.getAllParentsBystudentId(studentId);
		}

		@Override
		public List<ParentBean> getAllParents() {
				return parentDao.getAllParents();
		}
}
