package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.SubjectDao;
import com.sms.model.SubjectBean;
import com.sms.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class SubjectServiceImpl implements SubjectService {
		@Autowired
		private SubjectDao subjectDao;
		@Override
		public SubjectBean createSubject(SubjectBean subjectBean) {
				return subjectDao.createSubject(subjectBean);
		}

		@Override
		public SubjectBean updateSubject(SubjectBean subjectBean) {
				return subjectDao.updateSubject(subjectBean);
		}

		@Override
		public boolean deleteSubject(int subjectId) {
				return subjectDao.deleteSubject(subjectId);
		}

		@Override
		public List<SubjectBean> getAllSubjects() {
				return subjectDao.getAllSubjects();
		}

		@Override
		public List<SubjectBean> getSubjectsByClassId(int classId) {
				return subjectDao.getSubjectsByClassId(classId);
		}

		@Override
		public List<SubjectBean> getSubjectsByStreamId(int streamId) {
				return subjectDao.getSubjectsByStreamId(streamId);
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectName(String subjectName) {
				return subjectDao.getSubjectsBySubjectName(subjectName);
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectID(int subjectId) {
				return subjectDao.getSubjectsBySubjectID(subjectId);
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectCode(String subjectCode) {
				return subjectDao.getSubjectsBySubjectCode(subjectCode);
		}
}
