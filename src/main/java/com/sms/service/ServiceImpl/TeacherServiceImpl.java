package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.TeacherDao;
import com.sms.model.TeacherBean;
import com.sms.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class TeacherServiceImpl implements TeacherService {
		@Autowired
		private TeacherDao teacherDao;
		@Override
		public TeacherBean createTeacher(TeacherBean teacherBean) {
				return teacherDao.createTeacher(teacherBean);
		}

		@Override
		public TeacherBean updateTeacher(TeacherBean teacherBean) {
				return teacherDao.updateTeacher(teacherBean);
		}

		@Override
		public boolean deleteTeacher(int teacherId) {
				return teacherDao.deleteTeacher(teacherId);
		}

		@Override
		public List<TeacherBean> getAllTeachers() {
				return teacherDao.getAllTeachers();
		}

		@Override
		public TeacherBean getTeacherByTeacherId(int teacherId) {
				return teacherDao.getTeacherByTeacherId(teacherId);
		}

		@Override
		public List<TeacherBean> getTeacherBySubjectId(int subjectId) {
				return teacherDao.getTeacherBySubjectId(subjectId);
		}

		@Override
		public TeacherBean getTeacherByClassId(int classId) {
				return teacherDao.getTeacherByClassId(classId);
		}
}
