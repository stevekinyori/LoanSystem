package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.ExamDao;
import com.sms.dao.interfaces.ExamResultDao;
import com.sms.dao.interfaces.ExamSettingsDao;
import com.sms.model.ExamBean;
import com.sms.model.ExamResultBean;
import com.sms.model.ExamSettingsBean;
import com.sms.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/26/2017.
 */
@Service
public class ExamServiceImpl implements ExamService {
		@Autowired
		private ExamDao examDao;

		@Autowired
		private ExamResultDao examResultDao;
		@Autowired
		private ExamSettingsDao examSettingsDao;


		@Override
		public List<ExamBean> getAllSchoolExams() {
				return examDao.getAllSchoolExams();
		}

		@Override
		public List<ExamBean> getAllSchoolExamsByStatus(String status) {
				return examDao.getAllSchoolExamsByStatus(status);
		}

		@Override
		public ExamBean createExam(ExamBean examBean) {
				return examDao.createExam(examBean);
		}

		@Override
		public ExamBean updateExam(ExamBean examBean) {
				return examDao.updateExam(examBean);
		}

		@Override
		public boolean deleteExam(int examId) {
				return examDao.deleteExam(examId);
		}

		@Override
		public ExamResultBean addExamResult(ExamResultBean examResultBean) {
				return examResultDao.addExamResult(examResultBean);
		}

		@Override
		public ExamResultBean updateExamResult(ExamResultBean examResultBean) {
				return examResultDao.updateExamResult(examResultBean);
		}

		@Override
		public boolean deleteExamResult(int id) {
				return examResultDao.deleteExamResult(id);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamId(int examId) {
				return examResultDao.getExamResultsByExamId(examId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdTermId(int examId, int termId) {
				return examResultDao.getExamResultsByExamIdTermId(examId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassId(int examId, int classId) {
				return examResultDao.getExamResultsByExamIdClassId(examId, classId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdTermId(int examId, int classId, int termId) {
				return examResultDao.getExamResultsByExamIdClassIdTermId(examId, classId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentId(int examId, int classId, int studentId) {
				return examResultDao.getExamResultsByExamIdClassIdStudentId(examId, classId, studentId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdTermId(int examId, int classId, int studentId, int termId) {
				return examResultDao.getExamResultsByExamIdClassIdStudentIdTermId(examId, classId, studentId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectId(int examId, int classId, int studentId, int subjectId) {
				return examResultDao.getExamResultsByExamIdClassIdStudentIdSubjectId(examId, classId, studentId, subjectId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectIdTermId(int examId, int classId, int studentId, int subjectId, int termId) {
				return examResultDao.getExamResultsByExamIdClassIdStudentIdSubjectIdTermId(examId, classId, studentId, subjectId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByStudentId(int studentId) {
				return examResultDao.getExamResultsByStudentId(studentId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassId(int classId) {
				return examResultDao.getExamResultsByClassId(classId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdStudentId(int classId, int studentId) {
				return examResultDao.getExamResultsByClassIdStudentId(classId, studentId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdSubjectId(int classId, int subjectId) {
				return examResultDao.getExamResultsByClassIdSubjectId(classId, subjectId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdSubjectIdTermId(int classId, int subjectId, int termId) {
				return examResultDao.getExamResultsByClassIdSubjectIdTermId(classId, subjectId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdTermId(int classId, int termId) {
				return examResultDao.getExamResultsByClassIdTermId(classId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByStudentIdTermId(int studentId, int termId) {
				return examResultDao.getExamResultsByClassIdSubjectId(studentId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjectId(int subjectId) {
				return examResultDao.getExamResultsBySubjectId(subjectId);
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjetIdTermId(int subjectId, int termId) {
				return examResultDao.getExamResultsBySubjetIdTermId(subjectId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjectIdStudentId(int subjectId, int studentId) {
				return examResultDao.getExamResultsBySubjectIdStudentId(subjectId, studentId);
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjetIdStudentIdTermId(int subjectId, int studentId, int termId) {
				return examResultDao.getExamResultsBySubjetIdStudentIdTermId(subjectId, studentId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdStudentIdTermId(int classId, int studentId, int termId) {
				return examResultDao.getExamResultsByClassIdStudentIdTermId(classId, studentId, termId);
		}

		@Override
		public List<ExamResultBean> getExamResultsByTermId(int termId) {
				return examResultDao.getExamResultsByTermId(termId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettings() {
				return examSettingsDao.getAllExamSettings();
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByExamId(int examId) {
				return examSettingsDao.getAllExamSettingsByExamId(examId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByTermId(int termId) {
				return examSettingsDao.getAllExamSettingsByTermId(termId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassId(int classId) {
				return examSettingsDao.getAllExamSettingsByClassId(classId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsBySubjectId(int subjectId) {
				return examSettingsDao.getAllExamSettingsBySubjectId(subjectId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermId(int classId, int termId) {
				return examSettingsDao.getAllExamSettingsByClassIdTermId(classId, termId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamId(int classId, int termId, int examId) {
				return examSettingsDao.getAllExamSettingsByClassIdTermIdExamId(classId, termId, examId);
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamIdSubjectId(int classId, int termId, int examId, int subjectId) {
				return examSettingsDao.getAllExamSettingsByClassIdTermIdExamIdSubjectId(classId, termId, examId, subjectId);
		}
}
