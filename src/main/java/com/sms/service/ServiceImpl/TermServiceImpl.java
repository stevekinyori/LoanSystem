package com.sms.service.ServiceImpl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.AcademicYearDao;
import com.sms.dao.interfaces.TermDao;
import com.sms.model.AcademicYearBean;
import com.sms.model.TermBean;
import com.sms.service.TermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class TermServiceImpl implements TermService {
		@Autowired
		private TermDao termDao;
		@Autowired
		private AcademicYearDao academicYearDao;
		@Override
		public TermBean createTerm(TermBean termBean) {
				return termDao.createTerm(termBean);
		}

		@Override
		public TermBean updateTerm(TermBean termBean) {
				return termDao.updateTerm(termBean);
		}

		@Override
		public boolean deleteTermbean(int termId) {
				return termDao.deleteTermbean(termId);
		}

		@Override
		public List<TermBean> getAllTerms() {
				return termDao.getAllTerms();
		}

		@Override
		public TermBean getTermByTermId(int termId) {
				return termDao.getTermByTermId(termId);
		}

		@Override
		public List<TermBean> getTermByAcademicYearId(int academicYearId) {
				return termDao.getTermByAcademicYearId(academicYearId);
		}

		@Override
		public List<TermBean> getTermsBetweenDates(Date wef, Date wet) {
				return termDao.getTermsBetweenDates(wef, wet);
		}

		@Override
		public List<AcademicYearBean> getAcademicYearsByTermId(int termId) {
				return termDao.getAcademicYearsByTermId(termId);
		}

		@Override
		public List<AcademicYearBean> getAllAcademicyears() {
				return academicYearDao.getAllAcademicyears();
		}

		@Override
		public AcademicYearBean createAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions {
				return academicYearDao.createAcademicYear(academicYearBean);
		}

		@Override
		public boolean deleteAcademicYear(int id) throws SmsDatabaseExceptions {
				return academicYearDao.deleteAcademicYear(id);
		}

		@Override
		public AcademicYearBean updateAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions {
				return academicYearDao.updateAcademicYear(academicYearBean);
		}

		@Override
		public AcademicYearBean getAcademicYearById(Integer id) throws SmsDatabaseExceptions {
				return academicYearDao.getAcademicYearById(id);
		}

		@Override
		public List<AcademicYearBean> getAcademicYearsBetweenTwoDates(Date startDate, Date endDate) throws SmsDatabaseExceptions {
				return academicYearDao.getAcademicYearsBetweenTwoDates(startDate, endDate);
		}
}
