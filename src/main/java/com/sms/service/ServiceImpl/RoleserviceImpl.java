package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.RolePrivilegeDao;
import com.sms.dao.interfaces.UserRoleDao;
import com.sms.model.RolePrivilegeBean;
import com.sms.model.UserRoleBean;
import com.sms.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/11/2017.
 */
@Service
public class RoleserviceImpl implements RoleService {

		@Autowired
		private UserRoleDao userRoleDao;
		@Autowired
		private RolePrivilegeDao rolePrivilegeDao;

		@Override
		public List<UserRoleBean> getAllUserRoles() {
				return userRoleDao.getAllUserRoles();
		}

		@Override
		public List<RolePrivilegeBean> getAllPrivileges() {
				return rolePrivilegeDao.getAllRolePrivileges();
		}

		@Override
		public List<RolePrivilegeBean> getAllRolePrivileges() {
				return rolePrivilegeDao.getAllRolePrivileges();
		}

		@Override
		public List<RolePrivilegeBean> getAllRolePrivilegesByRoleId(int roleId) {
				return rolePrivilegeDao.getAllRolePrivilegesByRoleId(roleId);
		}

		@Override
		public RolePrivilegeBean addRolePrivilege(RolePrivilegeBean rolePrivilegeBean) {
				return rolePrivilegeDao.addRolePrivilege(rolePrivilegeBean);
		}

		@Override
		public RolePrivilegeBean updateRolePrivilege(RolePrivilegeBean rolePrivilegeBean) {
				return rolePrivilegeDao.updateRolePrivilege(rolePrivilegeBean);
		}

		@Override
		public Boolean deleteRolePrivilege(int id) {
				return rolePrivilegeDao.deleteRolePrivilege(id);
		}

		@Override
		public List<UserRoleBean> getAllUserRolesByUserId() {
				return userRoleDao.getAllUserRolesByUserId();
		}

		@Override
		public UserRoleBean createRole(UserRoleBean userRoleBean) {
				return userRoleDao.createRole(userRoleBean);
		}

		@Override
		public UserRoleBean updateUserRole(UserRoleBean userRoleBean) {
				return userRoleDao.updateUserRole(userRoleBean);
		}

		@Override
		public Boolean deleteUserRole(int id) {
				return userRoleDao.deleteUserRole(id);
		}
}
