package com.sms.service.ServiceImpl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.*;
import com.sms.model.*;
import com.sms.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/13/2017.
 */
@Service
public class ClassServiceImpl implements ClassService {

		@Autowired
		private ClassDao classDao;

		@Autowired
		private ClassHierarchyDao classHierarchyDao;
		@Autowired
		private ClassSubjectDao classSubjectDao;
		@Autowired
		private ClassTeacherDao classTeacherDao;
		@Autowired
		private StreamDao streamDao;

		@Override
		public List<ClassBean> getAllClasses() throws SmsDatabaseExceptions {
				return classDao.getAllClasses();
		}

		@Override
		public ClassBean updateClass(ClassBean classBean) throws SmsDatabaseExceptions {
				return classDao.updateClass(classBean);
		}

		@Override
		public ClassBean createClass(ClassBean classBean) throws SmsDatabaseExceptions {
				return classDao.createClass(classBean);
		}

		@Override
		public boolean deleteClass(int id) throws SmsDatabaseExceptions {
				return classDao.deleteClass(id);
		}

		@Override
		public List<ClassBean> getClassesByStatus(String status) {
				return null;
		}

		@Override
		public ClassBean getClassById(Integer classId) throws SmsDatabaseExceptions {
				return classDao.getClassById(classId);
		}

		@Override
		public List<ClassHierarchyBean> getClassHierarchies() throws SmsDatabaseExceptions {
				return classHierarchyDao.getClassHierarchies();
		}

		@Override
		public List<ClassHierarchyBean> getClassHierarchiesBylevelId(int levelId) throws SmsDatabaseExceptions {
				return classHierarchyDao.getClassHierarchiesBylevelId(levelId);
		}

		@Override
		public ClassHierarchyBean createClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions {
				return classHierarchyDao.createClassHierarchy(classHierarchyBean);
		}

		@Override
		public ClassHierarchyBean updateClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions {
				return classHierarchyDao.updateClassHierarchy(classHierarchyBean);
		}

		@Override
		public boolean deleteClassHierarchy(int id) throws SmsDatabaseExceptions {
				return classHierarchyDao.deleteClassHierarchy(id);
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjects() {
				return classSubjectDao.getAllClassSubjects();
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsBySubjectId(int subjectId) {
				return classSubjectDao.getAllClassSubjectsBySubjectId(subjectId);
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByClassId(int classId) {
				return classSubjectDao.getAllClassSubjectsBySubjectId(classId);
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByTeacherId(int teacherId) {
				return classSubjectDao.getAllClassSubjectsByTeacherId(teacherId);
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByTeacherIdClassId(int teacherId, int classId) {
				return classSubjectDao.getAllClassSubjectsByTeacherIdClassId(teacherId, classId);
		}

		@Override
		public ClassSubjectBean addClassSubject(ClassSubjectBean classSubjectBean) {
				return classSubjectDao.addClassSubject(classSubjectBean);
		}

		@Override
		public ClassSubjectBean updateClassSubject(ClassSubjectBean classSubjectBean) {
				return classSubjectDao.updateClassSubject(classSubjectBean);
		}

		@Override
		public boolean deleteClassSubject(int id) {
				return classSubjectDao.deleteClassSubject(id);
		}

		@Override
		public List<ClassTeacherBean> getAllClassTeachers() {
				return classTeacherDao.getAllClassTeachers();
		}

		@Override
		public List<ClassTeacherBean> getAllClassTeachersByClassId(int classId) {
				return classTeacherDao.getAllClassTeachersByClassId(classId);
		}

		@Override
		public ClassTeacherBean addClassTeacher(ClassTeacherBean classTeacherBean) {
				return classTeacherDao.addClassTeacher(classTeacherBean);
		}

		@Override
		public ClassTeacherBean updateClassTeacher(ClassTeacherBean classTeacherBean) {
				return classTeacherDao.updateClassTeacher(classTeacherBean);
		}

		@Override
		public boolean deleteClassTeacher(int id) {
				return classTeacherDao.deleteClassTeacher(id);
		}

		@Override
		public StreamBean createStream(StreamBean streamBean) {
				return streamDao.createStream(streamBean);
		}

		@Override
		public StreamBean updateStream(StreamBean streamBean) {
				return streamDao.updateStream(streamBean);
		}

		@Override
		public boolean deleteStream(int streamId) {
				return streamDao.deleteStream(streamId);
		}

		@Override
		public StreamBean getStreamById(int streamId) {
				return streamDao.getStreamById(streamId);
		}

		@Override
		public List<ClassBean> getAllClassesByStreamId(int streamId) {
				return streamDao.getAllClassesByStreamId(streamId);
		}

		@Override
		public List<StreamBean> getAllStreams() {
				return streamDao.getAllStreams();
		}
}
