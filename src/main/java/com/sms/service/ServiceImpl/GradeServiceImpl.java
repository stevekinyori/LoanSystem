package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.GradeScaleDao;
import com.sms.model.GradeScaleBean;
import com.sms.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/26/2017.
 */
@Service
public class GradeServiceImpl implements GradeService {
		@Autowired
		private GradeScaleDao gradeScaleDao;

		@Override
		public List<GradeScaleBean> getAllGradeScales() {
				return gradeScaleDao.getAllGradeScales();
		}

		@Override
		public GradeScaleBean createGradeScale(GradeScaleBean gradeScaleBean) {
				return gradeScaleDao.createGradeScale(gradeScaleBean);
		}

		@Override
		public GradeScaleBean updateGradeScale(GradeScaleBean gradeScaleBean) {
				return gradeScaleDao.updateGradeScale(gradeScaleBean);
		}

		@Override
		public boolean deleteGradeScale(int id) {
				return gradeScaleDao.deleteGradeScale(id);
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByClassId(int classId) {
				return gradeScaleDao.getGradeScalesByClassId(classId);
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByExamId(int examId) {
				return gradeScaleDao.getGradeScalesByExamId(examId);
		}

		@Override
		public List<GradeScaleBean> getGradeScalesBySubjectId(int subjectId) {
				return gradeScaleDao.getGradeScalesBySubjectId(subjectId);
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByClassIdExamIdSubjectId(int classId, int examId, int subjectId) {
				return gradeScaleDao.getGradeScalesByClassIdExamIdSubjectId(classId, examId, subjectId);
		}
}
