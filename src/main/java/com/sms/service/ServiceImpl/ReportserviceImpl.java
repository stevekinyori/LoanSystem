package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.Reportsdao;
import com.sms.model.SystemReportBean;
import com.sms.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Service
public class ReportserviceImpl implements ReportService {
		@Autowired
		private Reportsdao reportsdao;
		@Override
		public SystemReportBean createReport(SystemReportBean systemReportBean) {
				return reportsdao.createReport(systemReportBean);
		}

		@Override
		public SystemReportBean updateReport(SystemReportBean systemReportBean) {
				return reportsdao.updateReport(systemReportBean);
		}

		@Override
		public boolean deleteSystemReport(int reportId) {
				return reportsdao.deleteSystemReport(reportId);
		}

		@Override
		public List<SystemReportBean> getAllSystemReports() {
				return reportsdao.getAllSystemReports();
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportId(int reportId) {
				return reportsdao.getAllSystemReportsByReportId(reportId);
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportCode(int reportCode) {
				return reportsdao.getAllSystemReportsByReportCode(reportCode);
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportName(int reportName) {
				return reportsdao.getAllSystemReportsByReportName(reportName);
		}
}
