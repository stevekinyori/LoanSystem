package com.sms.service.ServiceImpl;

import com.sms.dao.interfaces.LibBookDao;
import com.sms.dao.interfaces.LibCategoryDao;
import com.sms.dao.interfaces.LibIssuedBookDao;
import com.sms.model.LibBookBean;
import com.sms.model.LibCategoryBean;
import com.sms.model.LibIssuedBookBean;
import com.sms.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Steve on 1/26/2017.
 */
@Service
public class LibraryServiceImpl implements LibraryService {
		@Autowired
		private LibBookDao libBookDao;
		@Autowired
		private LibCategoryDao libCategoryDao;
		@Autowired
		private LibIssuedBookDao libIssuedBookDao;

		@Override
		public List<LibBookBean> getAllLibBooks() {
				return libBookDao.getAllLibBooks();
		}

		@Override
		public LibBookBean createLibBook(LibBookBean libBookBean) {
				return libBookDao.createLibBook(libBookBean);
		}

		@Override
		public LibBookBean updateLibBook(LibBookBean libBookBean) {
				return libBookDao.updateLibBook(libBookBean);
		}

		@Override
		public boolean deleteLibBook(int bookId) {
				return libBookDao.deleteLibBook(bookId);
		}

		@Override
		public LibBookBean getLibBookByIsbnNo(int isbNO) {
				return libBookDao.getLibBookByIsbnNo(isbNO);
		}

		@Override
		public List<LibBookBean> getLibBookByCategoryId(int categoryId) {
				return libBookDao.getLibBookByCategoryId(categoryId);
		}

		@Override
		public LibCategoryBean createLibCategory(LibCategoryBean libCategoryBean) {
				return libCategoryDao.createLibCategory(libCategoryBean);
		}

		@Override
		public LibCategoryBean updateLibCategory(LibCategoryBean libCategoryBean) {
				return libCategoryDao.updateLibCategory(libCategoryBean);
		}

		@Override
		public boolean deleteLibCategory(int id) {
				return libCategoryDao.deleteLibCategory(id);
		}

		@Override
		public List<LibCategoryBean> getAllLibraryCategories() {
				return libCategoryDao.getAllLibraryCategories();
		}

		@Override
		public LibCategoryBean getAllLibraryCategoryByCategoryId(int categoryId) {
				return libCategoryDao.getAllLibraryCategoryByCategoryId(categoryId);
		}

		@Override
		public LibCategoryBean getAllLibraryCategoryByCategoryCode(int categoryCode) {
				return libCategoryDao.getAllLibraryCategoryByCategoryCode(categoryCode);
		}

		@Override
		public LibIssuedBookBean issueBook(LibIssuedBookBean libIssuedBookBean) {
				return libIssuedBookDao.issueBook(libIssuedBookBean);
		}

		@Override
		public LibIssuedBookBean updateIssuedBook(LibIssuedBookBean libIssuedBookBean) {
				return libIssuedBookDao.updateIssuedBook(libIssuedBookBean);
		}

		@Override
		public boolean deleteIssuedBookById(int id) {
				return libIssuedBookDao.deleteIssuedBookById(id);
		}

		@Override
		public List<LibIssuedBookBean> getAllIssuedBooks() {
				return libIssuedBookDao.getAllIssuedBooks();
		}

		@Override
		public List<LibBookBean> getAllAvailableBooks() {
				return libIssuedBookDao.getAllAvailableBooks();
		}

		@Override
		public List<LibIssuedBookBean> getAllOverdueBooks() {
				return libIssuedBookDao.getAllOverdueBooks();
		}
}
