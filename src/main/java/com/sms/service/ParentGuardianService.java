package com.sms.service;

import com.sms.model.GuardianBean;
import com.sms.model.ParentBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface ParentGuardianService {
		List<GuardianBean> getAllGuardians();

		GuardianBean createGuardian(GuardianBean guardianBean);

		GuardianBean updateGuardian(GuardianBean guardianBean);

		boolean deleteGuardian(int guardianId);

		List<GuardianBean> getGuardianByStudentId(int studentId);

		ParentBean createParent(ParentBean parentBean);

		ParentBean updateParent(ParentBean parentBean);

		boolean deleteParentRecord(int id);

		List<ParentBean> getAllParentsBystudentId(int studentId);

		List<ParentBean> getAllParents();
}
