package com.sms.service;

import com.sms.model.LibBookBean;
import com.sms.model.LibCategoryBean;
import com.sms.model.LibIssuedBookBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/25/2017.
 */
@Component
public interface LibraryService {
		List<LibBookBean> getAllLibBooks();

		LibBookBean createLibBook(LibBookBean libBookBean);

		LibBookBean updateLibBook(LibBookBean libBookBean);

		boolean deleteLibBook(int bookId);

		LibBookBean getLibBookByIsbnNo(int isbNO);

		List<LibBookBean> getLibBookByCategoryId(int categoryId);

		LibCategoryBean createLibCategory(LibCategoryBean libCategoryBean);

		LibCategoryBean updateLibCategory(LibCategoryBean libCategoryBean);

		boolean deleteLibCategory(int id);

		List<LibCategoryBean> getAllLibraryCategories();

		LibCategoryBean getAllLibraryCategoryByCategoryId(int categoryId);

		LibCategoryBean getAllLibraryCategoryByCategoryCode(int categoryCode);

		LibIssuedBookBean issueBook(LibIssuedBookBean libIssuedBookBean);

		LibIssuedBookBean updateIssuedBook(LibIssuedBookBean libIssuedBookBean);

		boolean deleteIssuedBookById(int id);

		List<LibIssuedBookBean> getAllIssuedBooks();

		List<LibBookBean> getAllAvailableBooks();

		List<LibIssuedBookBean> getAllOverdueBooks();
}
