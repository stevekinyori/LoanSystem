package com.sms.dao.interfaces;

import com.sms.model.ParentBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ParentDao {
		ParentBean createParent(ParentBean parentBean);

		ParentBean updateParent(ParentBean parentBean);

		boolean deleteParentRecord(int id);

		List<ParentBean> getAllParentsBystudentId(int studentId);

		List<ParentBean> getAllParents();
}