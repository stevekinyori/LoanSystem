package com.sms.dao.interfaces;

import com.sms.model.LibBookBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface LibBookDao {
		List<LibBookBean> getAllLibBooks();

		LibBookBean createLibBook(LibBookBean libBookBean);

		LibBookBean updateLibBook(LibBookBean libBookBean);

		boolean deleteLibBook(int bookId);

		LibBookBean getLibBookByIsbnNo(int isbNO);

		List<LibBookBean> getLibBookByCategoryId(int categoryId);

}


