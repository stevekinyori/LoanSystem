package com.sms.dao.interfaces;

import com.sms.model.UserRoleBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface UserRoleDao {
		List<UserRoleBean> getAllUserRoles();

		List<UserRoleBean> getAllUserRolesByUserId();

		UserRoleBean createRole(UserRoleBean userRoleBean);

		UserRoleBean updateUserRole(UserRoleBean userRoleBean);

		Boolean deleteUserRole(int id);
}