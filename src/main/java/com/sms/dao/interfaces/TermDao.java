package com.sms.dao.interfaces;

import com.sms.model.AcademicYearBean;
import com.sms.model.TermBean;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface TermDao {
		TermBean createTerm(TermBean termBean);

		TermBean updateTerm(TermBean termBean);

		boolean deleteTermbean(int termId);

		List<TermBean> getAllTerms();

		TermBean getTermByTermId(int termId);

		List<TermBean> getTermByAcademicYearId(int academicYearId);

		List<TermBean> getTermsBetweenDates(Date wef, Date wet);

		List<AcademicYearBean> getAcademicYearsByTermId(int termId);
}

