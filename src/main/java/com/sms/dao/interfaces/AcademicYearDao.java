package com.sms.dao.interfaces;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.AcademicYearBean;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Component
public interface AcademicYearDao {
		List<AcademicYearBean> getAllAcademicyears();

		AcademicYearBean createAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions;

		boolean deleteAcademicYear(int id) throws SmsDatabaseExceptions;

		AcademicYearBean updateAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions;

		AcademicYearBean getAcademicYearById(Integer id) throws SmsDatabaseExceptions;

		List<AcademicYearBean> getAcademicYearsBetweenTwoDates(Date startDate, Date endDate) throws SmsDatabaseExceptions;
}
