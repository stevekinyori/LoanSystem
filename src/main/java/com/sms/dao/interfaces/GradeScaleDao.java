package com.sms.dao.interfaces;

import com.sms.model.GradeScaleBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface GradeScaleDao {
		List<GradeScaleBean> getAllGradeScales();

		GradeScaleBean createGradeScale(GradeScaleBean gradeScaleBean);

		GradeScaleBean updateGradeScale(GradeScaleBean gradeScaleBean);

		boolean deleteGradeScale(int id);

		List<GradeScaleBean> getGradeScalesByClassId(int classId);

		List<GradeScaleBean> getGradeScalesByExamId(int examId);

		List<GradeScaleBean> getGradeScalesBySubjectId(int subjectId);

		List<GradeScaleBean> getGradeScalesByClassIdExamIdSubjectId(int classId, int examId, int subjectId);
}

