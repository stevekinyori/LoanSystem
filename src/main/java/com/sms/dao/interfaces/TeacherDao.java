package com.sms.dao.interfaces;

import com.sms.model.TeacherBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface TeacherDao {
		TeacherBean createTeacher(TeacherBean teacherBean);

		TeacherBean updateTeacher(TeacherBean teacherBean);

		boolean deleteTeacher(int teacherId);

		List<TeacherBean> getAllTeachers();

		TeacherBean getTeacherByTeacherId(int teacherId);

		List<TeacherBean> getTeacherBySubjectId(int subjectId);

		TeacherBean getTeacherByClassId(int classId);

}
