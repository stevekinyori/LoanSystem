package com.sms.dao.interfaces;

import com.sms.model.SubjectBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface SubjectDao {
		SubjectBean createSubject(SubjectBean subjectBean);

		SubjectBean updateSubject(SubjectBean subjectBean);

		boolean deleteSubject(int subjectId);

		List<SubjectBean> getAllSubjects();

		List<SubjectBean> getSubjectsByClassId(int classId);

		List<SubjectBean> getSubjectsByStreamId(int streamId);

		List<SubjectBean> getSubjectsBySubjectName(String subjectName);

		List<SubjectBean> getSubjectsBySubjectID(int subjectId);

		List<SubjectBean> getSubjectsBySubjectCode(String subjectCode);
}
