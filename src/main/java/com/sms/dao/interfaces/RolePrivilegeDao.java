package com.sms.dao.interfaces;

import com.sms.model.RolePrivilegeBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Component
public interface RolePrivilegeDao {
		List<RolePrivilegeBean> getAllRolePrivileges();

		List<RolePrivilegeBean> getAllRolePrivilegesByRoleId(int roleId);

		RolePrivilegeBean addRolePrivilege(RolePrivilegeBean rolePrivilegeBean);

		RolePrivilegeBean updateRolePrivilege(RolePrivilegeBean rolePrivilegeBean);

		Boolean deleteRolePrivilege(int id);
}
