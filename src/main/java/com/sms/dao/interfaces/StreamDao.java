package com.sms.dao.interfaces;

import com.sms.model.ClassBean;
import com.sms.model.StreamBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface StreamDao {
		StreamBean createStream(StreamBean streamBean);

		StreamBean updateStream(StreamBean streamBean);

		boolean deleteStream(int streamId);

		StreamBean getStreamById(int streamId);

		List<ClassBean> getAllClassesByStreamId(int streamId);

		List<StreamBean> getAllStreams();
}
