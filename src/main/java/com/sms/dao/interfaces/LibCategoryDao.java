package com.sms.dao.interfaces;

import com.sms.model.LibCategoryBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface LibCategoryDao {
		LibCategoryBean createLibCategory(LibCategoryBean libCategoryBean);

		LibCategoryBean updateLibCategory(LibCategoryBean libCategoryBean);

		boolean deleteLibCategory(int id);

		List<LibCategoryBean> getAllLibraryCategories();

		LibCategoryBean getAllLibraryCategoryByCategoryId(int categoryId);

		LibCategoryBean getAllLibraryCategoryByCategoryCode(int categoryCode);
}
