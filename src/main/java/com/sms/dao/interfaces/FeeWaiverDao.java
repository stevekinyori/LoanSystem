package com.sms.dao.interfaces;

import com.sms.model.FeeWaiverBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeeWaiverDao {

		List<FeeWaiverBean> getAllFeeWaivers();

		FeeWaiverBean createFeeWaiver(FeeWaiverBean feeWaiverBean);

		FeeWaiverBean updateFeeWaiver(FeeWaiverBean feeWaiverBean);

		boolean deleteFeeWaiver(int id);

		List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryId(int categoryId);

		List<FeeWaiverBean> getAllFeeWaiversByRateType(String rateType);

		List<FeeWaiverBean> getAllFeeWaiversByWaiverType(String waiverType);

		List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryIdFeeSubCategoryId(int categoryId, int subCategoryId);

		List<FeeWaiverBean> getAllFeeWaiversByFeeSubCategoryId(int subCategoryId);

}

