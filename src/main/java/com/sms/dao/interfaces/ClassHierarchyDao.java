package com.sms.dao.interfaces;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.ClassHierarchyBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ClassHierarchyDao {
		List<ClassHierarchyBean> getClassHierarchies() throws SmsDatabaseExceptions;

		List<ClassHierarchyBean> getClassHierarchiesBylevelId(int levelId) throws SmsDatabaseExceptions;

		ClassHierarchyBean createClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions;

		ClassHierarchyBean updateClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions;

		boolean deleteClassHierarchy(int id) throws SmsDatabaseExceptions;
}
