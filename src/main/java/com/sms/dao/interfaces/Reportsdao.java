package com.sms.dao.interfaces;

import com.sms.model.SystemReportBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface Reportsdao {
		SystemReportBean createReport(SystemReportBean systemReportBean);

		SystemReportBean updateReport(SystemReportBean systemReportBean);

		boolean deleteSystemReport(int reportId);

		List<SystemReportBean> getAllSystemReports();

		List<SystemReportBean> getAllSystemReportsByReportId(int reportId);

		List<SystemReportBean> getAllSystemReportsByReportCode(int reportCode);

		List<SystemReportBean> getAllSystemReportsByReportName(int reportName);

}

