package com.sms.dao.interfaces;

import com.sms.model.FeeDatesBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeeDatesDao {
		List<FeeDatesBean> getAllFeeDates();

		List<FeeDatesBean> getAllActiveFeeDates();

		List<FeeDatesBean> getAllFeeDatesBySubCategoryId(int subCategoryId);

		FeeDatesBean createFeedate(FeeDatesBean feeDatesBean);

		FeeDatesBean updateFeedate(FeeDatesBean feeDatesBean);

		boolean deleteFeeDates(int id);
}
