package com.sms.dao.interfaces;

import com.sms.model.LibBookBean;
import com.sms.model.LibIssuedBookBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface LibIssuedBookDao {
		LibIssuedBookBean issueBook(LibIssuedBookBean libIssuedBookBean);

		LibIssuedBookBean updateIssuedBook(LibIssuedBookBean libIssuedBookBean);

//		LibIssuedBookBean returnLibIssuedBook(LibIssuedBookBean libIssuedBookBean);

		boolean deleteIssuedBookById(int id);

//		boolean deleteReturnedBookById(int id);

		List<LibIssuedBookBean> getAllIssuedBooks();

		List<LibBookBean> getAllAvailableBooks();

		List<LibIssuedBookBean> getAllOverdueBooks();
}

