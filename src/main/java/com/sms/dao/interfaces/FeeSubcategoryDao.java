package com.sms.dao.interfaces;

import com.sms.model.FeeSubcategoryBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeeSubcategoryDao {
		FeeSubcategoryBean createFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean);

		FeeSubcategoryBean updateFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean);

		boolean deleteFeeSubCategory(int id);

		List<FeeSubcategoryBean> getAllFeeSubCategories();

		List<FeeSubcategoryBean> getAllFeeSubCategoriesByCategoryId(int id);

		List<FeeSubcategoryBean> getAllFeeSubCategoriesByName(String name);
}
