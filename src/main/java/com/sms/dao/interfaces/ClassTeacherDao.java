package com.sms.dao.interfaces;

import com.sms.model.ClassTeacherBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ClassTeacherDao {
		List<ClassTeacherBean> getAllClassTeachers();

		List<ClassTeacherBean> getAllClassTeachersByClassId(int classId);

		ClassTeacherBean addClassTeacher(ClassTeacherBean classTeacherBean);

		ClassTeacherBean updateClassTeacher(ClassTeacherBean classTeacherBean);

		boolean deleteClassTeacher(int id);
}
