package com.sms.dao.interfaces;

import com.sms.model.FeeApplicationLevelBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeeApplicationLevelDao {
		FeeApplicationLevelBean createFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean);

		FeeApplicationLevelBean updateFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean);

		boolean deleteFeeApplicationLevel(int levelId);

		List<FeeApplicationLevelBean> getAllFeeApplicationLevels();

		FeeApplicationLevelBean getFeeApplicationLevelByLevelId(int levelId);

		List<FeeApplicationLevelBean> getAllActiveFeeApplicationLevels();
}
