package com.sms.dao.interfaces;

import com.sms.model.ClassSubjectBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ClassSubjectDao {
		List<ClassSubjectBean> getAllClassSubjects();

		List<ClassSubjectBean> getAllClassSubjectsBySubjectId(int subjectId);

		List<ClassSubjectBean> getAllClassSubjectsByClassId(int classId);

		List<ClassSubjectBean> getAllClassSubjectsByTeacherId(int teacherId);

		List<ClassSubjectBean> getAllClassSubjectsByTeacherIdClassId(int teacherId, int classId);

		ClassSubjectBean addClassSubject(ClassSubjectBean classSubjectBean);

		ClassSubjectBean updateClassSubject(ClassSubjectBean classSubjectBean);

		boolean deleteClassSubject(int id);
}

