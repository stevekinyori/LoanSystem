package com.sms.dao.interfaces;

import com.sms.model.FeesCategoryBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeesCategoryDao {
		List<FeesCategoryBean> getAllFeeCategories();

		FeesCategoryBean createFeeCategory(FeesCategoryBean feesCategoryBean);

		FeesCategoryBean updateFeeCategory(FeesCategoryBean feesCategoryBean);

		boolean deleteFeeCategory(int id);

		FeesCategoryBean getFeeCategoryById(int id);
}
