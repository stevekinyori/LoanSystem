package com.sms.dao.interfaces;

import com.sms.model.GuardianBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface GuardianDao {
		List<GuardianBean> getAllGuardians();

		GuardianBean createGuardian(GuardianBean guardianBean);

		GuardianBean updateGuardian(GuardianBean guardianBean);

		boolean deleteGuardian(int guardianId);

		List<GuardianBean> getGuardianByStudentId(int studentId);
}
