package com.sms.dao.interfaces;

import com.sms.model.ExamBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ExamDao {
		List<ExamBean> getAllSchoolExams();

		List<ExamBean> getAllSchoolExamsByStatus(String status);

		ExamBean createExam(ExamBean examBean);

		ExamBean updateExam(ExamBean examBean);

		boolean deleteExam(int examId);
}

