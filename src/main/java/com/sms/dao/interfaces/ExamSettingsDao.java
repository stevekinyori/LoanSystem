package com.sms.dao.interfaces;

import com.sms.model.ExamSettingsBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface ExamSettingsDao {
		List<ExamSettingsBean> getAllExamSettings();

		List<ExamSettingsBean> getAllExamSettingsByExamId(int examId);

		List<ExamSettingsBean> getAllExamSettingsByTermId(int termId);

		List<ExamSettingsBean> getAllExamSettingsByClassId(int classId);

		List<ExamSettingsBean> getAllExamSettingsBySubjectId(int subjectId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermId(int classId, int termId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamId(int classId, int termId, int examId);

		List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamIdSubjectId(int classId, int termId, int examId, int subjectId);
}
