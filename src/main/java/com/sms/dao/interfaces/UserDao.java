package com.sms.dao.interfaces;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.ClassBean;
import com.sms.model.Common.UserDetails;
import com.sms.model.UserBean;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by Steve on 11/25/2016.
 */
@Component
public interface UserDao {
		List<UserBean> getAllUsers();

		UserBean getUserByUsername(String userName);

		UserBean getUserById(int Id);

		ClassBean getUserClass(BigDecimal userId);

		Boolean validateUserPassword(String Username, String Password) throws NoSuchAlgorithmException, SmsDatabaseExceptions;

		Boolean validateUserToken(String Username, String token);

		UserBean createUser(UserBean userBean) throws NoSuchAlgorithmException;

		UserDetails loadUserByUsername(String username);

		boolean deleteUser(int id);

		UserBean updateUser(UserBean userBean);
}
