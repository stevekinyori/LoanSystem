package com.sms.dao.interfaces;

import com.sms.model.StudentBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface StudentDao {
		StudentBean createStudent(StudentBean studentBean);

		StudentBean updateStudent(StudentBean studentBean);

		boolean deleteStudent(int studentId);

		List<StudentBean> getAllStudents();

		List<StudentBean> getStudentsByClassId(int classId);

		List<StudentBean> getStudentsByStreamId(int streamId);
}
