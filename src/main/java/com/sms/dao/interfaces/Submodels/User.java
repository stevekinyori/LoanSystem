package com.sms.dao.interfaces.Submodels;

/**
 * Created by Steve on 1/11/2017.
 */
public class User {
		private String password;
		private String username;

		public String getPassword() {
				return password;
		}

		public void setPassword(String password) {
				this.password = password;
		}

		public String getUsername() {
				return username;
		}

		public void setUsername(String username) {
				this.username = username;
		}

}
