package com.sms.dao.interfaces;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.model.ClassBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Component
public interface ClassDao {
		List<ClassBean> getAllClasses() throws SmsDatabaseExceptions;

		ClassBean updateClass(ClassBean classBean) throws SmsDatabaseExceptions;

		ClassBean createClass(ClassBean classBean) throws SmsDatabaseExceptions;

		boolean deleteClass(int id) throws SmsDatabaseExceptions;

		List<ClassBean> getClassesByStatus(String status) throws SmsCustomException, SmsDatabaseExceptions;

		ClassBean getClassById(Integer classId) throws SmsDatabaseExceptions;
}
