package com.sms.dao.interfaces;

import com.sms.model.PrivilegeBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface PrivilegeDao {
		List<PrivilegeBean> getAllPrivileges();

		PrivilegeBean createPrivilege(PrivilegeBean privilegeBean);

		PrivilegeBean updatePrivilege(PrivilegeBean privilegeBean);

		Boolean deletePrivilege(int id);
}
