package com.sms.dao.interfaces;

import com.sms.model.FeeAllocationBean;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
public interface FeeAllocationDao {
		FeeAllocationBean addFeeAllocaton(FeeAllocationBean feeAllocationBean);

		FeeAllocationBean updateFeeAllocaton(FeeAllocationBean feeAllocationBean);

		boolean deleteFeeAllocation(int id);

		List<FeeAllocationBean> getAllFeeAllocations();

		List<FeeAllocationBean> getAllActiveFeeAllocations();

		FeeAllocationBean getFeeAllocationById(int id);

		List<FeeAllocationBean> getFeeAllocationByFeeSubCategoryId(int subCategoryId);

		List<FeeAllocationBean> getFeeAllocationByFeeCategoryId(int categoryId);

		List<FeeAllocationBean> getFeeAllocationByApplicationLevelId(int applicationLevelId);

		List<FeeAllocationBean> getFeeAllocationByApplicationLevelIdSubCategoryId(int applicationLevelId, int subCategoryId);
}
