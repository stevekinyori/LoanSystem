package com.sms.dao.impl;

import com.sms.dao.interfaces.LibCategoryDao;
import com.sms.model.LibCategoryBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class LibCategoryDaoImpl implements LibCategoryDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(LibCategoryDaoImpl.class);

		@Override
		public LibCategoryBean createLibCategory(LibCategoryBean libCategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(libCategoryBean);
						LibCategoryBean category = (LibCategoryBean) session.get(LibCategoryBean.class, id);
						tx.commit();
						return category;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibCategoryBean updateLibCategory(LibCategoryBean libCategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(libCategoryBean);
						LibCategoryBean category = (LibCategoryBean) session.get(LibCategoryBean.class, libCategoryBean.getId());
						tx.commit();
						return category;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteLibCategory(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						LibCategoryBean category = (LibCategoryBean) session.get(LibCategoryBean.class, id);
						session.update(category);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<LibCategoryBean> getAllLibraryCategories() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibCategoryBean> categories = session.createQuery("from LibCategoryBean ").list();
						tx.commit();
						return categories;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibCategoryBean getAllLibraryCategoryByCategoryId(int categoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibCategoryBean> categories;
						Query query = session.createQuery("from LibCategoryBean where id =:providedId");
						query.setParameter("providedId", categoryId);
						categories = query.list();
						tx.commit();
						if (categories.size() > 0) {
								return categories.get(0);
						}
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibCategoryBean getAllLibraryCategoryByCategoryCode(int categoryCode) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibCategoryBean> categories;
						Query query = session.createQuery("from LibCategoryBean where code =:providedCode");
						query.setParameter("providedCode", categoryCode);
						categories = query.list();
						tx.commit();
						if (categories.size() > 0) {
								return categories.get(0);
						}
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
