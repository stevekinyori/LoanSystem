package com.sms.dao.impl;

import com.sms.dao.interfaces.ClassSubjectDao;
import com.sms.model.ClassSubjectBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ClassSubjectDaoImpl implements ClassSubjectDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ClassSubjectDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ClassSubjectBean> getAllClassSubjects() {
				session = factory.openSession();
				List<ClassSubjectBean> classSubjectBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						classSubjectBeanList = session.createQuery("from ClassSubjectBean ").list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classSubjectBeanList;
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsBySubjectId(int subjectId) {
				session = factory.openSession();
				List<ClassSubjectBean> classSubjectBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from ClassSubjectBean  where subjectId = :providedSubjectId");
						query.setParameter("providedSubjectId", subjectId);
						classSubjectBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classSubjectBeanList;
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByClassId(int classId) {
				session = factory.openSession();
				List<ClassSubjectBean> classSubjectBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from ClassSubjectBean  where classId = :providedClassId");
						query.setParameter("providedClassId", classId);
						classSubjectBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classSubjectBeanList;
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByTeacherId(int teacherId) {
				session = factory.openSession();
				List<ClassSubjectBean> classSubjectBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from ClassSubjectBean  where teacherId = :providedTeacherId");
						query.setParameter("providedTeacherId", teacherId);
						classSubjectBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classSubjectBeanList;
		}

		@Override
		public List<ClassSubjectBean> getAllClassSubjectsByTeacherIdClassId(int teacherId, int classId) {
				session = factory.openSession();
				List<ClassSubjectBean> classSubjectBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from ClassSubjectBean " +
										"where teacherId = :providedTeacherId and classId= :providedClassId");
						query.setParameter("providedTeacherId", teacherId);
						query.setParameter("providedClassId", classId);
						classSubjectBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classSubjectBeanList;
		}

		@Override
		public ClassSubjectBean addClassSubject(ClassSubjectBean classSubjectBean) {
				session = factory.openSession();
				ClassSubjectBean result = new ClassSubjectBean();
				try {
						Transaction tx = session.beginTransaction();
						int id = (int) session.save(classSubjectBean);
						result = (ClassSubjectBean) session.get(ClassSubjectBean.class, id);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return result;
		}

		@Override
		public ClassSubjectBean updateClassSubject(ClassSubjectBean classSubjectBean) {
				session = factory.openSession();
				ClassSubjectBean result = new ClassSubjectBean();
				try {
						Transaction tx = session.beginTransaction();
						session.update(classSubjectBean);
						result = (ClassSubjectBean) session.get(ClassSubjectBean.class, classSubjectBean.getId());
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return result;
		}

		@Override
		public boolean deleteClassSubject(int id) {
				session = factory.openSession();
				ClassSubjectBean deleteBean;
				try {
						Transaction tx = session.beginTransaction();
						deleteBean = (ClassSubjectBean) session.get(ClassSubjectBean.class, id);
						session.delete(deleteBean);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return true;
		}
}
