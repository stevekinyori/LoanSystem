package com.sms.dao.impl;

import com.sms.dao.interfaces.ParentDao;
import com.sms.model.ParentBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ParentDaoImpl implements ParentDao {

		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(ParentDaoImpl.class);

		@Override
		public ParentBean createParent(ParentBean parentBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(parentBean);
						ParentBean result = (ParentBean) session.get(ParentBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public ParentBean updateParent(ParentBean parentBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(parentBean);
						ParentBean result = (ParentBean) session.get(ParentBean.class, parentBean.getId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}


		@Override
		public boolean deleteParentRecord(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						ParentBean result = (ParentBean) session.get(ParentBean.class, id);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<ParentBean> getAllParentsBystudentId(int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<ParentBean> parents = session.createQuery("from ParentBean where studentId =:providedId")
										.setParameter("providedId", studentId).list();
						tx.commit();
						return parents;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ParentBean> getAllParents() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<ParentBean> parents = session.createQuery("from ParentBean").list();
						tx.commit();
						return parents;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
