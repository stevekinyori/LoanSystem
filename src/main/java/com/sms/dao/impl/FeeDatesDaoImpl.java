package com.sms.dao.impl;

import com.sms.dao.interfaces.FeeDatesDao;
import com.sms.model.FeeDatesBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeeDatesDaoImpl implements FeeDatesDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FeeDatesDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<FeeDatesBean> getAllFeeDates() {
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						List<FeeDatesBean> feeDatesBeanList = session.createQuery("from FeeDatesBean ").list();
						tx.commit();
						return feeDatesBeanList;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeDatesBean> getAllActiveFeeDates() {
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						List<FeeDatesBean> feeDatesBeanList;
						Query query = session.createQuery("from FeeDatesBean where status = :providedStatus");
						query.setParameter("providedStatus", "ACTIVE");
						feeDatesBeanList = query.list();
						tx.commit();
						return feeDatesBeanList;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeDatesBean> getAllFeeDatesBySubCategoryId(int subCategoryId) {
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						List<FeeDatesBean> feeDatesBeanList;
						Query query = session.createQuery("from FeeDatesBean where subCategoryId = :providedSubCategoryId");
						query.setParameter("providedSubCategoryId", subCategoryId);
						feeDatesBeanList = query.list();
						tx.commit();
						return feeDatesBeanList;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeeDatesBean createFeedate(FeeDatesBean feeDatesBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feeDatesBean);
						FeeDatesBean feebean = (FeeDatesBean) session.get(FeeDatesBean.class, id);
						tx.commit();
						return feebean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public FeeDatesBean updateFeedate(FeeDatesBean feeDatesBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feeDatesBean);
						FeeDatesBean feebean = (FeeDatesBean) session.get(FeeDatesBean.class, feeDatesBean.getId());
						tx.commit();
						return feebean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteFeeDates(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeDatesBean feebean = (FeeDatesBean) session.get(FeeDatesBean.class, id);
						session.delete(feebean);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}
}
