package com.sms.dao.impl;

import com.sms.dao.interfaces.FeeWaiverDao;
import com.sms.model.FeeWaiverBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeeWaiverDaoImpl implements FeeWaiverDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(FeeWaiverDaoImpl.class);

		@Override
		public List<FeeWaiverBean> getAllFeeWaivers() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeWaiverBean> waivers = session.createQuery("from FeeWaiverBean ").list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeeWaiverBean createFeeWaiver(FeeWaiverBean feeWaiverBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feeWaiverBean);
						FeeWaiverBean waiver = (FeeWaiverBean) session.get(FeeWaiverBean.class, id);
						tx.commit();
						return waiver;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeeWaiverBean updateFeeWaiver(FeeWaiverBean feeWaiverBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feeWaiverBean);
						FeeWaiverBean waiver = (FeeWaiverBean) session.get(FeeWaiverBean.class, feeWaiverBean.getId());
						tx.commit();
						return waiver;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public boolean deleteFeeWaiver(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeWaiverBean waiver = (FeeWaiverBean) session.get(FeeWaiverBean.class, id);
						session.delete(waiver);
						tx.commit();
						return true;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return false;
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryId(int categoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeWaiverBean  where feeCategoryId =: providedId");
						query.setParameter("providedId", categoryId);
						List<FeeWaiverBean> waivers = query.list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByRateType(String rateType) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeWaiverBean  where rateType =: providedType");
						query.setParameter("providedType", rateType);
						List<FeeWaiverBean> waivers = query.list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByWaiverType(String waiverType) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeWaiverBean  where waiverType =: providedType");
						query.setParameter("providedType", waiverType);
						List<FeeWaiverBean> waivers = query.list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeCategoryIdFeeSubCategoryId(int categoryId, int subCategoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeWaiverBean  where feeCategoryId =: providedCategoryId and feeSubCategoryId = :providedSubcategoryId");
						query.setParameter("providedCategoryId", categoryId);
						query.setParameter("providedSubcategoryId", subCategoryId);
						List<FeeWaiverBean> waivers = query.list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public List<FeeWaiverBean> getAllFeeWaiversByFeeSubCategoryId(int subCategoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeWaiverBean  where feeSubCategoryId = :providedSubcategoryId");
						query.setParameter("providedSubcategoryId", subCategoryId);
						List<FeeWaiverBean> waivers = query.list();
						tx.commit();
						return waivers;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}
}
