package com.sms.dao.impl;

import com.sms.dao.interfaces.FeeAllocationDao;
import com.sms.model.FeeAllocationBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeeAllocationDaoImpl implements FeeAllocationDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FeeAllocationDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public FeeAllocationBean addFeeAllocaton(FeeAllocationBean feeAllocationBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feeAllocationBean);
						FeeAllocationBean allocationBean = (FeeAllocationBean) session.get(FeeAllocationBean.class, id);
						tx.commit();
						return allocationBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public FeeAllocationBean updateFeeAllocaton(FeeAllocationBean feeAllocationBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feeAllocationBean);
						FeeAllocationBean allocationBean = (FeeAllocationBean)
										session.get(FeeAllocationBean.class, feeAllocationBean.getId());
						tx.commit();
						return allocationBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteFeeAllocation(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeAllocationBean allocationBean = (FeeAllocationBean)
										session.get(FeeAllocationBean.class, id);
						session.update(allocationBean.getId());
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<FeeAllocationBean> getAllFeeAllocations() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeAllocationBean> feeAllocationBeen = session.createQuery("from FeeAllocationBean ").list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeAllocationBean> getAllActiveFeeAllocations() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeAllocationBean where status =: providedStatus");
						query.setParameter("providedStatus", "ACTIVE");
						List<FeeAllocationBean> feeAllocationBeen = query.list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public FeeAllocationBean getFeeAllocationById(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeAllocationBean allocationBean = (FeeAllocationBean)
										session.get(FeeAllocationBean.class, id);
						tx.commit();
						return allocationBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByFeeSubCategoryId(int subCategoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeAllocationBean where feeSubCategory =: providedSubCategoryId");
						query.setParameter("providedSubCategoryId", subCategoryId);
						List<FeeAllocationBean> feeAllocationBeen = query.list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByFeeCategoryId(int categoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeAllocationBean where feeCategory =: providedCategoryId");
						query.setParameter("providedCategoryId", categoryId);
						List<FeeAllocationBean> feeAllocationBeen = query.list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByApplicationLevelId(int applicationLevelId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeAllocationBean where applicationlevel =: providedApplicationLevel");
						query.setParameter("providedApplicationLevel", applicationLevelId);
						List<FeeAllocationBean> feeAllocationBeen = query.list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeAllocationBean> getFeeAllocationByApplicationLevelIdSubCategoryId(int applicationLevelId, int subCategoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from FeeAllocationBean where applicationlevel =: providedApplicationLevel and feeSubCategory =:providedSubCategoryId");
						query.setParameter("providedApplicationLevel", applicationLevelId);
						query.setParameter("providedSubCategoryId", subCategoryId);
						List<FeeAllocationBean> feeAllocationBeen = query.list();
						tx.commit();
						return feeAllocationBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
