package com.sms.dao.impl;

import com.sms.dao.interfaces.ExamSettingsDao;
import com.sms.model.ExamSettingsBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ExamSettingsDaoImpl implements ExamSettingsDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExamSettingsDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ExamSettingsBean> getAllExamSettings() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<ExamSettingsBean> list = session.createQuery("FROM ExamSettingsBean ").list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByExamId(int examId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where examId = : providedExamId");
						query.setParameter("providedExamId", examId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByTermId(int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where termId = : providedTermId");
						query.setParameter("providedTermId", termId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where classId = : providedClassId");
						query.setParameter("providedClassId", classId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsBySubjectId(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where subjectId = : providedSubjectId");
						query.setParameter("providedSubjectId", subjectId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermId(int classId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where classId = : providedClassId and termId =:providedTermId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedTermId", termId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamId(int classId, int termId, int examId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where classId = : providedClassId and termId =:providedTermId and examId =:providedExamId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedTermId", termId);
						query.setParameter("providedExamId", examId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamSettingsBean> getAllExamSettingsByClassIdTermIdExamIdSubjectId(int classId, int termId, int examId, int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("FROM ExamSettingsBean where classId = : providedClassId and termId =:providedTermId and examId =:providedExamId and subjectId =:providedSubjectId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedTermId", termId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedSubjectId", subjectId);
						List<ExamSettingsBean> list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
