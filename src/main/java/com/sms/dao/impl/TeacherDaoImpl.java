package com.sms.dao.impl;

import com.sms.dao.interfaces.TeacherDao;
import com.sms.model.TeacherBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class TeacherDaoImpl implements TeacherDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(TeacherDaoImpl.class);

		@Override
		public TeacherBean createTeacher(TeacherBean teacherBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(teacherBean);
						TeacherBean result = (TeacherBean) session.get(TeacherBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public TeacherBean updateTeacher(TeacherBean teacherBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(teacherBean);
						TeacherBean result = (TeacherBean) session.get(TeacherBean.class, teacherBean.getTeacherId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteTeacher(int teacherId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						TeacherBean result = (TeacherBean) session.get(TeacherBean.class, teacherId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<TeacherBean> getAllTeachers() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<TeacherBean> teacherBeenList = session.createQuery("from TeacherBean").list();
						tx.commit();
						return teacherBeenList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public TeacherBean getTeacherByTeacherId(int teacherId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						TeacherBean teacherBeen = (TeacherBean) session.get(TeacherBean.class, teacherId);
						tx.commit();
						return teacherBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<TeacherBean> getTeacherBySubjectId(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<TeacherBean> teacherBeenList = session.createQuery("from TeacherBean where teacherId in" +
										"(select teacherId from ClassSubjectBean where subjectId =:providedSubjectId)").list();
						tx.commit();
						return teacherBeenList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public TeacherBean getTeacherByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						TeacherBean teacher;
						Query query = session.createQuery("from TeacherBean where teacherId in" +
										"(select teacherId from ClassSubjectBean where classId =:providedClassId)")
										.setParameter("providedClassId", classId);
						List<TeacherBean> list = query.list();
						tx.commit();
						if (list.size() == 1) {
								return list.get(0);
						}
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
