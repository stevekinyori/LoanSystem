package com.sms.dao.impl;

import com.sms.dao.interfaces.UserRoleDao;
import com.sms.model.UserRoleBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class UserRoleDaoImpl implements UserRoleDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(UserRoleDaoImpl.class);

		@Override
		public List<UserRoleBean> getAllUserRoles() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				List<UserRoleBean> userRoles = session.createQuery("FROM UserRoleBean ").list();
				tx.commit();
				return userRoles;
		}

		@Override
		public List<UserRoleBean> getAllUserRolesByUserId() {
				return null;
		}

		@Override
		public UserRoleBean createRole(UserRoleBean userRoleBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(userRoleBean);
						UserRoleBean result = (UserRoleBean) session.get(UserRoleBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public UserRoleBean updateUserRole(UserRoleBean userRoleBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(userRoleBean);
						UserRoleBean result = (UserRoleBean) session.get(UserRoleBean.class, userRoleBean.getId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public Boolean deleteUserRole(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						UserRoleBean result = (UserRoleBean) session.get(UserRoleBean.class, id);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}
}
