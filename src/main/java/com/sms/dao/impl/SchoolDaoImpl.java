package com.sms.dao.impl;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.dao.interfaces.SchoolDao;
import com.sms.model.SchoolBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class SchoolDaoImpl implements SchoolDao {


		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(SchoolDaoImpl.class);

		@Override
		public SchoolBean createSchool(SchoolBean schoolBean) throws SmsCustomException {
				session = factory.openSession();
				SchoolBean bean;
				Transaction tx = session.beginTransaction();
				int id = (int) session.save(schoolBean);
				tx.commit();
				try {
						bean = (SchoolBean) session.get(SchoolBean.class, id);
						return bean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}

				return null;
		}

		@Override
		public SchoolBean updateSchoolDetails(SchoolBean schoolBean) throws SmsCustomException {
				session = factory.openSession();
				SchoolBean bean = new SchoolBean();
				Transaction tx = session.beginTransaction();
				session.update(schoolBean);
				tx.commit();
				try {
						List<SchoolBean> schools = session.createQuery("from SchoolBean ").list();
						if (!schools.isEmpty()) {
								bean = schools.get(1);
						}
				} catch (HibernateException ex) {
						logger.trace(ex);
				}

				return bean;
		}

		@Override
		public SchoolBean getSchoolDetails() throws SmsCustomException {
				session = factory.openSession();
				SchoolBean schoolBean = new SchoolBean();
				try {
						List<SchoolBean> Schools = session.createQuery("from SchoolBean").list();
						if (Schools.isEmpty()) {
								return schoolBean;
						} else {
								return Schools.get(1);
						}
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
