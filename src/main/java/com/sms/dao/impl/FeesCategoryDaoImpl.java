package com.sms.dao.impl;

import com.sms.dao.interfaces.FeesCategoryDao;
import com.sms.model.FeesCategoryBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeesCategoryDaoImpl implements FeesCategoryDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(FeesCategoryDaoImpl.class);

		@Override
		public List<FeesCategoryBean> getAllFeeCategories() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeesCategoryBean> list = session.createQuery("from FeesCategoryBean ").list();
						tx.commit();
						return list;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeesCategoryBean createFeeCategory(FeesCategoryBean feesCategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feesCategoryBean);
						FeesCategoryBean feesCategoryBean1 = (FeesCategoryBean) session.get(FeesCategoryBean.class, id);
						tx.commit();
						return feesCategoryBean1;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeesCategoryBean updateFeeCategory(FeesCategoryBean feesCategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feesCategoryBean);
						FeesCategoryBean feesCategoryBean1 = (FeesCategoryBean) session.get(FeesCategoryBean.class, feesCategoryBean.getId());
						tx.commit();
						return feesCategoryBean1;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public boolean deleteFeeCategory(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeesCategoryBean feesCategoryBean = (FeesCategoryBean) session.get(FeesCategoryBean.class, id);
						session.delete(feesCategoryBean);
						tx.commit();
						return true;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return false;
		}

		@Override
		public FeesCategoryBean getFeeCategoryById(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeesCategoryBean feesCategoryBean = (FeesCategoryBean) session.get(FeesCategoryBean.class, id);
						tx.commit();
						return feesCategoryBean;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}
}
