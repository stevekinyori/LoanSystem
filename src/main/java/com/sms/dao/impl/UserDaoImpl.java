package com.sms.dao.impl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.UserDao;
import com.sms.model.ClassBean;
import com.sms.model.Common.UserDetails;
import com.sms.model.UserBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 11/25/2016.
 */
@Repository
@SuppressWarnings("unchecked")
public class UserDaoImpl implements UserDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<UserBean> getAllUsers() {
				session = factory.openSession();
				return (List<UserBean>) session.createQuery("FROM UserBean ").list();

		}

		@Override
		public UserBean getUserByUsername(String userName) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				Query query = session.createQuery("FROM UserBean where username=:usernameProvided");
				query.setParameter("usernameProvided", userName);
				tx.commit();
				factory.close();
				return null;
		}

		@Override
		public UserBean getUserById(int Id) {
				session = factory.openSession();
				Transaction ts = session.beginTransaction();
				try {
						return (UserBean) session.get(UserBean.class, Id);
				} catch (HibernateException ex) {
						return null;
				}

		}

		@Override
		public ClassBean getUserClass(BigDecimal userId) {
				return null;
		}

		@Override
		public Boolean validateUserPassword(String Username, String Password) throws NoSuchAlgorithmException, SmsDatabaseExceptions {
				session = factory.openSession();
				MessageDigest messageDigest = MessageDigest.getInstance("MD5");
				messageDigest.update(Password.getBytes());
				byte[] digest = messageDigest.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
						sb.append(Integer.toHexString(b & 0xff));
				}
				String providedPass = sb.toString();
				Transaction tx = session.beginTransaction();
				Query query = session.createQuery("Select username FROM UserBean where username = :usernameProvided and password =:providedPass");
				query.setParameter("usernameProvided", Username);
				query.setParameter("providedPass", providedPass);
				String userNameResult;
				List<String> users = query.list();
				tx.commit();
				if (users.size() == 1) {
						userNameResult = users.get(0);
						return Username.equalsIgnoreCase(userNameResult);
				}


				return false;
		}

		@Override
		public Boolean validateUserToken(String Username, String token) {
				return null;
		}

		@Override
		public UserBean createUser(UserBean userBean) throws NoSuchAlgorithmException {
				session = factory.openSession();
				MessageDigest messageDigest = MessageDigest.getInstance("MD5");
				messageDigest.update(userBean.getPassword().getBytes());
				byte[] digest = messageDigest.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
						sb.append(Integer.toHexString(b & 0xff));
				}
				userBean.setPassword(sb.toString());
				try {
						Transaction tx = session.beginTransaction();
						userBean.setDateCreated(new Date());
						int id = (int) session.save(userBean);
						tx.commit();
						return (UserBean) session.get(UserBean.class, id);
				} catch (Exception e) {
						return null;
				}
		}

		@Override
		public UserDetails loadUserByUsername(String username) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				Query query = session.createQuery("FROM UserBean where username=:usernameProvided");
				query.setParameter("usernameProvided", username);
				tx.commit();
				UserDetails userDetails = new UserDetails();
				UserBean userBean = (UserBean) query;
				userDetails.setPassword(userBean.getPassword());
				userDetails.setUsername(userBean.getUsername());
				userDetails.setUserRoles(userBean.getUserRolesByRoleId());
				return userDetails;
		}

		@Override
		public boolean deleteUser(int id) {
				session = factory.openSession();
				try {
						Transaction tx = session.beginTransaction();
						UserBean user = (UserBean) session.get(UserBean.class, id);
						if (user != null) {
								session.delete(user);
								tx.commit();

						} else {
								return false;
						}

						return true;
				} catch (HibernateException ex) {
						ex.printStackTrace();
				}
				return false;
		}

		@Override
		public UserBean updateUser(UserBean userBean) {
				session = factory.openSession();
				MessageDigest messageDigest = null;
				try {
						messageDigest = MessageDigest.getInstance("MD5");
				} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
				}
				if (messageDigest != null) {
						messageDigest.update(userBean.getPassword().getBytes());
				}
				byte[] digest = new byte[0];
				if (messageDigest != null) {
						digest = messageDigest.digest();
				}
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
						sb.append(Integer.toHexString(b & 0xff));
				}
				userBean.setPassword(sb.toString());
				UserBean userBean1 = new UserBean();
				try {
						Transaction tx = session.beginTransaction();
						session.update(userBean);
						tx.commit();
						userBean1 = (UserBean) session.get(UserBean.class, userBean.getId());

				} catch (HibernateException ex) {
						ex.printStackTrace();
				}
				return userBean1;
		}

}
