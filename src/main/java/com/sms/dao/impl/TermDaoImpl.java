package com.sms.dao.impl;

import com.sms.dao.interfaces.TermDao;
import com.sms.model.AcademicYearBean;
import com.sms.model.TermBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class TermDaoImpl implements TermDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(TermDaoImpl.class);

		@Override
		public TermBean createTerm(TermBean termBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(termBean);
						TermBean result = (TermBean) session.get(TermBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public TermBean updateTerm(TermBean termBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(termBean);
						TermBean result = (TermBean) session.get(TermBean.class, termBean.getTermId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteTermbean(int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						TermBean result = (TermBean) session.get(TermBean.class, termId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<TermBean> getAllTerms() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<TermBean> termBeanList = session.createQuery("from TermBean").list();
						tx.commit();
						return termBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public TermBean getTermByTermId(int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						TermBean termBean = (TermBean) session.get(TermBean.class, termId);
						tx.commit();
						return termBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<TermBean> getTermByAcademicYearId(int academicYearId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<TermBean> termBeanList = session.createQuery("from TermBean where academicYearId =: providedid")
										.setParameter("providedid", academicYearId).list();
						tx.commit();
						return termBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<TermBean> getTermsBetweenDates(Date wef, Date wet) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<TermBean> termBeanList = session.createQuery("from TermBean where wef between :wef and :wet or wet between :wef and :wet")
										.setParameter("wef", wef)
										.setParameter("wet", wet).list();
						tx.commit();
						return termBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<AcademicYearBean> getAcademicYearsByTermId(int termId) {
				return null;
		}
}
