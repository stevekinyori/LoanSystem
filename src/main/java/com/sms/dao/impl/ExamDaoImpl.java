package com.sms.dao.impl;

import com.sms.dao.interfaces.ExamDao;
import com.sms.model.ExamBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ExamDaoImpl implements ExamDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExamDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ExamBean> getAllSchoolExams() {
				session = factory.openSession();
				List<ExamBean> examBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						examBeanList = session.createQuery("from ExamBean ").list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return examBeanList;

		}

		@Override
		public List<ExamBean> getAllSchoolExamsByStatus(String status) {
				session = factory.openSession();
				List<ExamBean> examBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from ExamBean where status =: providedStatus");
						query.setParameter("providedStatus", status);
						examBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return examBeanList;
		}

		@Override
		public ExamBean createExam(ExamBean examBean) {
				session = factory.openSession();
				ExamBean exam = new ExamBean();
				try {
						Transaction tx = session.beginTransaction();
						int id = (int) session.save(examBean);
						exam = (ExamBean) session.get(ExamBean.class, id);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return exam;
		}

		@Override
		public ExamBean updateExam(ExamBean examBean) {
				session = factory.openSession();
				ExamBean exam = new ExamBean();
				try {
						Transaction tx = session.beginTransaction();
						session.update(examBean);
						exam = (ExamBean) session.get(ExamBean.class, examBean.getExamId());
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return exam;
		}

		@Override
		public boolean deleteExam(int examId) {
				session = factory.openSession();
				ExamBean deleteBean;
				try {
						Transaction tx = session.beginTransaction();
						deleteBean = (ExamBean) session.get(ExamBean.class, examId);
						session.delete(deleteBean);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return true;
		}
}
