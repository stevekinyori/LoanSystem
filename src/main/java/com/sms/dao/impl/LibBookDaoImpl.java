package com.sms.dao.impl;

import com.sms.dao.interfaces.LibBookDao;
import com.sms.model.LibBookBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class LibBookDaoImpl implements LibBookDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(LibBookDaoImpl.class);

		@Override
		public List<LibBookBean> getAllLibBooks() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibBookBean> bookList = session.createQuery("from LibBookBean ").list();
						tx.commit();
						return bookList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibBookBean createLibBook(LibBookBean libBookBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(libBookBean);
						LibBookBean book = (LibBookBean) session.get(LibBookBean.class, id);
						tx.commit();
						return book;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibBookBean updateLibBook(LibBookBean libBookBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(libBookBean);
						LibBookBean book = (LibBookBean) session.get(LibBookBean.class, libBookBean.getId());
						tx.commit();
						return book;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteLibBook(int bookId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						LibBookBean book = (LibBookBean) session.get(LibBookBean.class, bookId);
						session.update(book);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public LibBookBean getLibBookByIsbnNo(int isbNO) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibBookBean> bookList;
						Query query = session.createQuery("from LibBookBean where isbnNo =:providedIsbnNo");
						query.setParameter("providedIsbnNo", isbNO);
						bookList = query.list();
						tx.commit();
						return bookList.get(0);
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<LibBookBean> getLibBookByCategoryId(int categoryId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibBookBean> bookList;
						Query query = session.createQuery("from LibBookBean where categoryId =:providedCategoryId");
						query.setParameter("providedCategoryId", categoryId);
						bookList = query.list();
						tx.commit();
						return bookList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
