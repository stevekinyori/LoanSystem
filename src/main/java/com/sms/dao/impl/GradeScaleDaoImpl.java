package com.sms.dao.impl;

import com.sms.dao.interfaces.GradeScaleDao;
import com.sms.model.GradeScaleBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class GradeScaleDaoImpl implements GradeScaleDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(GradeScaleDaoImpl.class);

		@Override
		public List<GradeScaleBean> getAllGradeScales() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GradeScaleBean> scaleBeen = session.createQuery("from GradeScaleBean ").list();
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public GradeScaleBean createGradeScale(GradeScaleBean gradeScaleBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(gradeScaleBean);
						GradeScaleBean scaleBeen = (GradeScaleBean) session.get(GradeScaleBean.class, id);
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public GradeScaleBean updateGradeScale(GradeScaleBean gradeScaleBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(gradeScaleBean);
						GradeScaleBean scaleBeen = (GradeScaleBean) session.get(GradeScaleBean.class, gradeScaleBean.getId());
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteGradeScale(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						GradeScaleBean scaleBeen = (GradeScaleBean) session.get(GradeScaleBean.class, id);

						session.update(scaleBeen);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GradeScaleBean> scaleBeen;
						Query query = session.createQuery("from GradeScaleBean where classId =: providedClassId");
						query.setParameter("providedClassId", classId);
						scaleBeen = query.list();
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByExamId(int examId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GradeScaleBean> scaleBeen;
						Query query = session.createQuery("from GradeScaleBean where examId =: providedExamId");
						query.setParameter("providedExamId", examId);
						scaleBeen = query.list();
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<GradeScaleBean> getGradeScalesBySubjectId(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GradeScaleBean> scaleBeen;
						Query query = session.createQuery("from GradeScaleBean where subjectId =: providedSubjectId");
						query.setParameter("providedSubjectId", subjectId);
						scaleBeen = query.list();
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<GradeScaleBean> getGradeScalesByClassIdExamIdSubjectId(int classId, int examId, int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GradeScaleBean> scaleBeen;
						Query query = session.createQuery("from GradeScaleBean where subjectId =: providedSubjectId and classId =:providedClassId and examId =:providedExamId");
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						scaleBeen = query.list();
						tx.commit();
						return scaleBeen;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
