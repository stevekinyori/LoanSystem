package com.sms.dao.impl;

import com.sms.dao.interfaces.ExamResultDao;
import com.sms.model.ExamResultBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ExamResultDaoImpl implements ExamResultDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExamResultDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public ExamResultBean addExamResult(ExamResultBean examResultBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(examResultBean);
						ExamResultBean result = (ExamResultBean) session.get(ExamResultBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public ExamResultBean updateExamResult(ExamResultBean examResultBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(examResultBean);
						ExamResultBean result = (ExamResultBean) session.get(ExamResultBean.class, examResultBean.getId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteExamResult(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						ExamResultBean result = (ExamResultBean) session.get(ExamResultBean.class, id);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamId(int examId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId");
						query.setParameter("providedExamId", examId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdTermId(int examId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId and examsByExamId.termId =:providedTermId ");
						query.setParameter("providedTermId", termId);
						query.setParameter("providedExamId", examId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassId(int examId, int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId and classId =:providedClassId ");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdTermId(int examId, int classId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId and classId =:providedClassId " +
										"and examsByExamId.termId =:providedTermId ");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentId(int examId, int classId, int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId " +
										"and classId =:providedClassId and studentId =:providedStudentId ");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedStudentId", studentId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdTermId(int examId, int classId, int studentId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId " +
										"and classId =:providedClassId and studentId =:providedStudentId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectId(int examId, int classId, int studentId, int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId " +
										"and classId =:providedClassId and studentId =:providedStudentId and subjectId=:providedSubjectId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedSubjectId", subjectId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByExamIdClassIdStudentIdSubjectIdTermId(int examId, int classId, int studentId, int subjectId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examId = :providedExamId " +
										"and classId =:providedClassId and studentId =:providedStudentId and subjectId=:providedSubjectId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedExamId", examId);
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByStudentId(int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where studentId =:providedStudentId");
						query.setParameter("providedStudentId", studentId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId");
						query.setParameter("providedClassId", classId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdStudentId(int classId, int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId and studentId =:providedStudentId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedStudentId", studentId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdSubjectId(int classId, int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId and subjectId =:providedSubjectId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedSubjectId", subjectId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdSubjectIdTermId(int classId, int subjectId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId and subjectId =:providedSubjectId " +
										"and examsByExamId.termId =:providedTermId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdTermId(int classId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedClassId", classId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByStudentIdTermId(int studentId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where studentId =:providedStudentId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjectId(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where subjectId =:providedSubjectId");
						query.setParameter("providedSubjectId", subjectId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjetIdTermId(int subjectId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where subjectId =:providedSubjectId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjectIdStudentId(int subjectId, int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where subjectId =:providedSubjectId and studentId =:providedStudentId");
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedStudentId", studentId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsBySubjetIdStudentIdTermId(int subjectId, int studentId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where subjectId =:providedSubjectId " +
										"and studentId =:providedStudentId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedSubjectId", subjectId);
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByClassIdStudentIdTermId(int classId, int studentId, int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where classId =:providedClassId " +
										"and studentId =:providedStudentId and examsByExamId.termId =:providedTermId");
						query.setParameter("providedStudentId", studentId);
						query.setParameter("providedClassId", classId);
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ExamResultBean> getExamResultsByTermId(int termId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ExamResultBean  where examsByExamId.termId =:providedTermId");
						query.setParameter("providedTermId", termId);
						List<ExamResultBean> results = query.list();
						tx.commit();
						return results;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
