package com.sms.dao.impl;

import com.sms.ExceptionHandlers.SmsCustomException;
import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.ClassDao;
import com.sms.model.ClassBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ClassDaoImpl implements ClassDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ClassBean> getAllClasses() throws SmsDatabaseExceptions {
				List<ClassBean> classes;
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						classes = session.createQuery("FROM ClassBean ").list();
						tx.commit();
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error fetching class details", ex.getMessage());
				}
				return classes;
		}

		@Override
		public ClassBean updateClass(ClassBean classBean) throws SmsDatabaseExceptions {
				ClassBean myClass;
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						session.update(classBean);
						tx.commit();
						tx = session.beginTransaction();
						myClass = (ClassBean) session.get(ClassBean.class, classBean.getClassId());
						tx.commit();
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error updating class details", ex.getMessage());
				}
				return myClass;

		}

		@Override
		public ClassBean createClass(ClassBean classBean) throws SmsDatabaseExceptions {
				ClassBean newClass = new ClassBean();
				try {
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						session.save(classBean);
						tx.commit();
						newClass = (ClassBean) session.get(ClassBean.class, classBean.getClassId());
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error fetching class details", ex.getMessage());
				}
				return newClass;
		}

		@Override
		public boolean deleteClass(int id) throws SmsDatabaseExceptions {
				try {
						ClassBean classBean = getClassById(id);
						session = factory.openSession();
						Transaction tx = session.beginTransaction();
						session.delete(classBean);
						tx.commit();
						ClassBean classBean1 = getClassById(id);
						return classBean1 == null;
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error deleting class", ex.getMessage());
				}
		}

		@Override
		public List<ClassBean> getClassesByStatus(String status) throws SmsCustomException, SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query search = session.createQuery("FROM ClassBean where status like :param");
						search.setParameter("param", "%" + status + "%");
						List<ClassBean> classes = search.list();
						tx.commit();
						return classes;
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error running class query", ex.getMessage());
				}
		}

		@Override
		public ClassBean getClassById(Integer classId) throws SmsDatabaseExceptions {
				session = factory.openSession();
				ClassBean classBean;
				try {
						classBean = (ClassBean) session.get(ClassBean.class, classId);
				} catch (HibernateException ex) {
						ex.printStackTrace();
						throw new SmsDatabaseExceptions("Error fetching class details", ex.getMessage());
				}
				return classBean;
		}

}
