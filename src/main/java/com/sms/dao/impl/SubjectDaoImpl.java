package com.sms.dao.impl;

import com.sms.dao.interfaces.SubjectDao;
import com.sms.model.SubjectBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class SubjectDaoImpl implements SubjectDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(SubjectDaoImpl.class);

		@Override
		public SubjectBean createSubject(SubjectBean subjectBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(subjectBean);
						SubjectBean result = (SubjectBean) session.get(SubjectBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public SubjectBean updateSubject(SubjectBean subjectBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(subjectBean);
						SubjectBean result = (SubjectBean) session.get(SubjectBean.class, subjectBean.getSubjectId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteSubject(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						SubjectBean result = (SubjectBean) session.get(SubjectBean.class, subjectId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<SubjectBean> getAllSubjects() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean").list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SubjectBean> getSubjectsByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean where subjectId in " +
										"(select subjectId from ClassSubjectBean where classId =:providedClassId)")
										.setParameter("providedClassId", classId).list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SubjectBean> getSubjectsByStreamId(int streamId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean where subjectId in " +
										"(select subjectId from ClassSubjectBean where classId in " +
										"(select classId from ClassBean where streamId =:providedStreamId))")
										.setParameter("providedStreamId", streamId).list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectName(String subjectName) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean where subjectName = :providedName")
										.setParameter("providedName", subjectName).list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectID(int subjectId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean where subjectId = :providedId")
										.setParameter("providedId", subjectId).list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SubjectBean> getSubjectsBySubjectCode(String subjectCode) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SubjectBean> subjectBeanList = session.createQuery("from SubjectBean where subjectCode = :providedCode")
										.setParameter("providedCode", subjectCode).list();
						tx.commit();
						return subjectBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
