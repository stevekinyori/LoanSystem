package com.sms.dao.impl;

import com.sms.dao.interfaces.RolePrivilegeDao;
import com.sms.model.RolePrivilegeBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class RolePrivilageDaoImpl implements RolePrivilegeDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(RolePrivilageDaoImpl.class);

		@Override
		public List<RolePrivilegeBean> getAllRolePrivileges() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<RolePrivilegeBean> rolePrivilegeBeanList = session.createQuery("from RolePrivilegeBean").list();
						tx.commit();
						return rolePrivilegeBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<RolePrivilegeBean> getAllRolePrivilegesByRoleId(int roleId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<RolePrivilegeBean> rolePrivilegeBeanList = session.createQuery("from RolePrivilegeBean where roleId =:providedRoleId")
										.setParameter("providedRoleId", roleId).list();
						tx.commit();
						return rolePrivilegeBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public RolePrivilegeBean addRolePrivilege(RolePrivilegeBean rolePrivilegeBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(rolePrivilegeBean);
						RolePrivilegeBean result = (RolePrivilegeBean) session.get(RolePrivilegeBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public RolePrivilegeBean updateRolePrivilege(RolePrivilegeBean rolePrivilegeBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(rolePrivilegeBean);
						RolePrivilegeBean result = (RolePrivilegeBean) session.get(RolePrivilegeBean.class, rolePrivilegeBean.getId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public Boolean deleteRolePrivilege(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						RolePrivilegeBean result = (RolePrivilegeBean) session.get(RolePrivilegeBean.class, id);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}
}
