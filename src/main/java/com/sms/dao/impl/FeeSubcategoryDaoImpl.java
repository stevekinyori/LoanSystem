package com.sms.dao.impl;

import com.sms.dao.interfaces.FeeSubcategoryDao;
import com.sms.model.FeeSubcategoryBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeeSubcategoryDaoImpl implements FeeSubcategoryDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(FeeSubcategoryDaoImpl.class);

		@Override
		public FeeSubcategoryBean createFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feeSubcategoryBean);
						FeeSubcategoryBean subcategoryBean = (FeeSubcategoryBean) session.get(FeeSubcategoryBean.class, id);
						tx.commit();
						return subcategoryBean;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public FeeSubcategoryBean updateFeeSubCategory(FeeSubcategoryBean feeSubcategoryBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feeSubcategoryBean);
						FeeSubcategoryBean subcategoryBean = (FeeSubcategoryBean) session.get(FeeSubcategoryBean.class, feeSubcategoryBean.getId());
						tx.commit();
						return subcategoryBean;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return null;
		}

		@Override
		public boolean deleteFeeSubCategory(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeSubcategoryBean subcategoryBean = (FeeSubcategoryBean) session.get(FeeSubcategoryBean.class, id);
						session.delete(subcategoryBean);
						tx.commit();
						return true;
				} catch (HibernateException e) {
						logger.trace(e);
				}
				return false;
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategories() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeSubcategoryBean> result = session.createQuery("FROM FeeSubcategoryBean ").list();
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategoriesByCategoryId(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeSubcategoryBean> result;
						Query query = session.createQuery("FROM FeeSubcategoryBean where categoryId=:providedCategoryId");
						query.setParameter("providedCategoryId", id);
						result = query.list();
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeSubcategoryBean> getAllFeeSubCategoriesByName(String name) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeSubcategoryBean> result;
						Query query = session.createQuery("FROM FeeSubcategoryBean where subcategoryName=:providedName");
						query.setParameter("providedName", name);
						result = query.list();
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
