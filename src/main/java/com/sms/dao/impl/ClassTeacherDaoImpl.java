package com.sms.dao.impl;

import com.sms.dao.interfaces.ClassTeacherDao;
import com.sms.model.ClassTeacherBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ClassTeacherDaoImpl implements ClassTeacherDao {

		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ClassTeacherDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ClassTeacherBean> getAllClassTeachers() {
				session = factory.openSession();
				List<ClassTeacherBean> classTeacherBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						classTeacherBeanList = session.createQuery("from ClassTeacherBean ").list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classTeacherBeanList;
		}

		@Override
		public List<ClassTeacherBean> getAllClassTeachersByClassId(int classId) {
				session = factory.openSession();
				List<ClassTeacherBean> classTeacherBeanList = new ArrayList<>();
				try {
						Transaction tx = session.beginTransaction();
						Query query = session.createQuery("from  ClassTeacherBean where classId = :providedClassId");
						query.setParameter("providedClassId", classId);
						classTeacherBeanList = query.list();
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return classTeacherBeanList;
		}

		@Override
		public ClassTeacherBean addClassTeacher(ClassTeacherBean classTeacherBean) {
				session = factory.openSession();
				ClassTeacherBean result = new ClassTeacherBean();
				try {
						Transaction tx = session.beginTransaction();
						int id = (int) session.save(classTeacherBean);
						result = (ClassTeacherBean) session.get(ClassTeacherBean.class, id);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return result;
		}

		@Override
		public ClassTeacherBean updateClassTeacher(ClassTeacherBean classTeacherBean) {
				session = factory.openSession();
				ClassTeacherBean result = new ClassTeacherBean();
				try {
						Transaction tx = session.beginTransaction();
						session.update(classTeacherBean);
						result = (ClassTeacherBean) session.createQuery("from ClassTeacherBean " +
										"where classId =:providedClasId and teacherId =: providedClassTeacher");
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return result;
		}

		@Override
		public boolean deleteClassTeacher(int id) {
				session = factory.openSession();
				ClassTeacherBean deleteBean;
				try {
						Transaction tx = session.beginTransaction();
						deleteBean = (ClassTeacherBean) session.get(ClassTeacherBean.class, id);
						session.delete(deleteBean);
						tx.commit();
				} catch (HibernateException he) {
						logger.trace(he);
				}
				return true;
		}

}
