package com.sms.dao.impl;

import com.sms.dao.interfaces.PrivilegeDao;
import com.sms.model.PrivilegeBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class PrivilegeDaoImpl implements PrivilegeDao {

		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(PrivilegeDaoImpl.class);

		@Override
		public List<PrivilegeBean> getAllPrivileges() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<PrivilegeBean> privilegeBeanList = session.createQuery("from PrivilegeBean").list();
						tx.commit();
						return privilegeBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public PrivilegeBean createPrivilege(PrivilegeBean privilegeBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(privilegeBean);
						PrivilegeBean result = (PrivilegeBean) session.get(PrivilegeBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public PrivilegeBean updatePrivilege(PrivilegeBean privilegeBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(privilegeBean);
						PrivilegeBean result = (PrivilegeBean) session.get(PrivilegeBean.class, privilegeBean.getId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public Boolean deletePrivilege(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						PrivilegeBean result = (PrivilegeBean) session.get(PrivilegeBean.class, id);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}
}
