package com.sms.dao.impl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.ClassHierarchyDao;
import com.sms.model.ClassHierarchyBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ClassHierarchyDaoImpl implements ClassHierarchyDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<ClassHierarchyBean> getClassHierarchies() throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<ClassHierarchyBean> classHierarchyBeanList = session.createQuery("from ClassHierarchyBean").list();

						tx.commit();
						return classHierarchyBeanList;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Error fetching class hierarchy records", ex.getMessage());
				}
		}

		@Override
		public List<ClassHierarchyBean> getClassHierarchiesBylevelId(int levelId) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						Query query = session.createQuery("from ClassHierarchyBean where levelId = :levelIdProvided");
						query.setParameter("levelIdProvided", levelId);
						List<ClassHierarchyBean> classHierarchyBeanList = query.list();
						tx.commit();
						return classHierarchyBeanList;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Error fetching class hierarchy records", ex.getMessage());
				}
		}

		@Override
		public ClassHierarchyBean createClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(classHierarchyBean);
						ClassHierarchyBean saved = (ClassHierarchyBean) session.get(ClassHierarchyDao.class, id);
						tx.commit();
						return saved;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Error creating class hierarchy records", ex.getMessage());
				}
		}

		@Override
		public ClassHierarchyBean updateClassHierarchy(ClassHierarchyBean classHierarchyBean) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(classHierarchyBean);
						ClassHierarchyBean updated = (ClassHierarchyBean) session.get(ClassHierarchyDao.class, classHierarchyBean.getHierachyId());
						tx.commit();
						return updated;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Error updating class hierarchy records", ex.getMessage());
				}
		}

		@Override
		public boolean deleteClassHierarchy(int id) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						ClassHierarchyBean toBeDeleted = (ClassHierarchyBean) session.get(ClassHierarchyDao.class, id);
						session.delete(toBeDeleted);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Error updating class hierarchy records", ex.getMessage());
				}
		}
}
