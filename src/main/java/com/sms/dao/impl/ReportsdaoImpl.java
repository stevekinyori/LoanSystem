package com.sms.dao.impl;

import com.sms.dao.interfaces.Reportsdao;
import com.sms.model.SystemReportBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class ReportsdaoImpl implements Reportsdao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(ReportsdaoImpl.class);

		@Override
		public SystemReportBean createReport(SystemReportBean systemReportBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(systemReportBean);
						SystemReportBean result = (SystemReportBean) session.get(SystemReportBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public SystemReportBean updateReport(SystemReportBean systemReportBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(systemReportBean);
						SystemReportBean result = (SystemReportBean) session.get(SystemReportBean.class, systemReportBean.getReportId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteSystemReport(int reportId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						SystemReportBean result = (SystemReportBean) session.get(SystemReportBean.class, reportId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<SystemReportBean> getAllSystemReports() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SystemReportBean> systemReportBeanList = session.createQuery("from SystemReportBean").list();
						tx.commit();
						return systemReportBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportId(int reportId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SystemReportBean> systemReportBeanList = session.createQuery("from SystemReportBean where reportId =:providedId")
										.setParameter("providedId", reportId).list();
						tx.commit();
						return systemReportBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportCode(int reportCode) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SystemReportBean> systemReportBeanList = session.createQuery("from SystemReportBean where reportCode =:providedCode")
										.setParameter("providedCode", reportCode).list();
						tx.commit();
						return systemReportBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<SystemReportBean> getAllSystemReportsByReportName(int reportName) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<SystemReportBean> systemReportBeanList = session.createQuery("from SystemReportBean where reportName =:providedName")
										.setParameter("providedName", reportName).list();
						tx.commit();
						return systemReportBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
