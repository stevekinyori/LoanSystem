package com.sms.dao.impl;

import com.sms.dao.interfaces.LibIssuedBookDao;
import com.sms.model.LibBookBean;
import com.sms.model.LibIssuedBookBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class LibIssuedBookDaoImpl implements LibIssuedBookDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(LibIssuedBookDaoImpl.class);

		@Override
		public LibIssuedBookBean issueBook(LibIssuedBookBean libIssuedBookBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(libIssuedBookBean);
						LibIssuedBookBean issuedBookBean = (LibIssuedBookBean) session.get(LibIssuedBookBean.class, id);
						tx.commit();
						return issuedBookBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public LibIssuedBookBean updateIssuedBook(LibIssuedBookBean libIssuedBookBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(libIssuedBookBean);
						LibIssuedBookBean updated = (LibIssuedBookBean) session.get(LibIssuedBookBean.class, libIssuedBookBean.getId());
						tx.commit();
						return updated;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
//
//		@Override
//		public LibIssuedBookBean returnLibIssuedBook(LibIssuedBookBean libIssuedBookBean) {
//				session = factory.openSession();
//				Transaction tx = session.beginTransaction();
//				try {
//						session.update(libIssuedBookBean);
//						LibIssuedBookBean returned = (LibIssuedBookBean) session.get(LibIssuedBookBean.class, libIssuedBookBean.getId());
//						tx.commit();
//						return returned;
//				} catch (HibernateException ex) {
//						logger.trace(ex);
//				}
//				return null;
//		}

		@Override
		public boolean deleteIssuedBookById(int id) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						LibIssuedBookBean toDelete = (LibIssuedBookBean) session.get(LibIssuedBookBean.class, id);
						session.delete(toDelete);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

//		@Override
//		public boolean deleteReturnedBookById(int id) {
//				return false;
//		}

		@Override
		public List<LibIssuedBookBean> getAllIssuedBooks() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibIssuedBookBean> list;

						Query query = session.createQuery("from LibIssuedBookBean where status =: porvidedStatus");
						list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}


		@Override
		public List<LibBookBean> getAllAvailableBooks() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibBookBean> list;
						Query query = session.createQuery("from LibBookBean where id not in " +
										"( select bookId from LibIssuedBookBean where status not in('RECEIVED'))");
						list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<LibIssuedBookBean> getAllOverdueBooks() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<LibIssuedBookBean> list;
						Query query = session.createQuery("FROM LibIssuedBookBean  WHERE status not  in ('RECEIVED') AND returnDate >:providedDate");
						query.setParameter("providedDate", new Date());
						list = query.list();
						tx.commit();
						return list;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
