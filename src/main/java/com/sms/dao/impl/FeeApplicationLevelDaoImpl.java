package com.sms.dao.impl;

import com.sms.dao.interfaces.FeeApplicationLevelDao;
import com.sms.model.FeeApplicationLevelBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class FeeApplicationLevelDaoImpl implements FeeApplicationLevelDao {
		org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FeeApplicationLevelDaoImpl.class);
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public FeeApplicationLevelBean createFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(feeApplicationLevelBean);
						FeeApplicationLevelBean applicationLevelBean = (FeeApplicationLevelBean) session.get(FeeApplicationLevelBean.class, id);
						tx.commit();
						return applicationLevelBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public FeeApplicationLevelBean updateFeeApplicationLevel(FeeApplicationLevelBean feeApplicationLevelBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(feeApplicationLevelBean);
						FeeApplicationLevelBean applicationLevelBean = (FeeApplicationLevelBean) session.get(FeeApplicationLevelBean.class, feeApplicationLevelBean.getLevelId());
						tx.commit();
						return applicationLevelBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteFeeApplicationLevel(int levelId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeApplicationLevelBean applicationLevelBean = (FeeApplicationLevelBean) session.get(FeeApplicationLevelBean.class, levelId);
						session.update(applicationLevelBean);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<FeeApplicationLevelBean> getAllFeeApplicationLevels() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeApplicationLevelBean> applicationLevels = session.createQuery("From FeeApplicationLevelBean ").list();
						tx.commit();
						return applicationLevels;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public FeeApplicationLevelBean getFeeApplicationLevelByLevelId(int levelId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						FeeApplicationLevelBean applicationLevel = (FeeApplicationLevelBean) session.get(FeeApplicationLevelBean.class, levelId);
						tx.commit();
						return applicationLevel;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<FeeApplicationLevelBean> getAllActiveFeeApplicationLevels() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<FeeApplicationLevelBean> applicationLevels;
						Query query = session.createQuery("From FeeApplicationLevelBean where status=:providedStatus");
						query.setParameter("providedStatus", "ACTIVE");
						applicationLevels = query.list();
						tx.commit();
						return applicationLevels;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
