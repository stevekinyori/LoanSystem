package com.sms.dao.impl;

import com.sms.dao.interfaces.GuardianDao;
import com.sms.model.GuardianBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class GuardianDaoImpl implements GuardianDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(GradeScaleDaoImpl.class);

		@Override
		public List<GuardianBean> getAllGuardians() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GuardianBean> guardianBeanList = session.createQuery("from GuardianBean").list();
						tx.commit();
						return guardianBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public GuardianBean createGuardian(GuardianBean guardianBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(guardianBean);
						GuardianBean bean = (GuardianBean) session.get(GuardianBean.class, id);
						tx.commit();
						return bean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public GuardianBean updateGuardian(GuardianBean guardianBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(guardianBean);
						GuardianBean bean = (GuardianBean) session.get(GuardianBean.class, guardianBean.getId());
						tx.commit();
						return bean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteGuardian(int guardianId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						GuardianBean bean = (GuardianBean) session.get(GuardianBean.class, guardianId);
						session.delete(bean);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<GuardianBean> getGuardianByStudentId(int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<GuardianBean> guardianBeanList;
						Query query = session.createQuery("from GuardianBean where  studentId =:providedStudentId");
						query.setParameter("providedStudentId", studentId);
						guardianBeanList = query.list();
						tx.commit();
						return guardianBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
