package com.sms.dao.impl;

import com.sms.dao.interfaces.StudentDao;
import com.sms.model.StudentBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class StudentDaoImpl implements StudentDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(StudentDaoImpl.class);

		@Override
		public StudentBean createStudent(StudentBean studentBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(studentBean);
						StudentBean result = (StudentBean) session.get(StudentBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public StudentBean updateStudent(StudentBean studentBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(studentBean);
						StudentBean result = (StudentBean) session.get(StudentBean.class, studentBean.getStudentId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteStudent(int studentId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						StudentBean result = (StudentBean) session.get(StudentBean.class, studentId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public List<StudentBean> getAllStudents() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<StudentBean> streamBeanList = session.createQuery("from StudentBean").list();
						tx.commit();
						return streamBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<StudentBean> getStudentsByClassId(int classId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<StudentBean> studentBeenlist = session.createQuery("from StudentBean where classesByEntryClass.classId =:providedClassId")
										.setParameter("providedClassId", classId).list();
						tx.commit();
						return studentBeenlist;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<StudentBean> getStudentsByStreamId(int streamId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<StudentBean> studentBeenlist = session.createQuery("from StudentBean where classesByEntryClass.streamId =:providedStreamId")
										.setParameter("providedStreamId", streamId).list();
						tx.commit();
						return studentBeenlist;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
