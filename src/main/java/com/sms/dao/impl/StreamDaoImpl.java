package com.sms.dao.impl;

import com.sms.dao.interfaces.StreamDao;
import com.sms.model.ClassBean;
import com.sms.model.StreamBean;
import com.sms.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@Repository
@SuppressWarnings("unchecked")
public class StreamDaoImpl implements StreamDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;
		private Logger logger = Logger.getLogger(StreamDaoImpl.class);

		@Override
		public StreamBean createStream(StreamBean streamBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(streamBean);
						StreamBean result = (StreamBean) session.get(StreamBean.class, id);
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public StreamBean updateStream(StreamBean streamBean) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(streamBean);
						StreamBean result = (StreamBean) session.get(StreamBean.class, streamBean.getStreamId());
						tx.commit();
						return result;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public boolean deleteStream(int streamId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						StreamBean result = (StreamBean) session.get(StreamBean.class, streamId);
						session.delete(result);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return false;
		}

		@Override
		public StreamBean getStreamById(int streamId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						StreamBean streamBean = (StreamBean) session.get(StreamBean.class, streamId);
						tx.commit();
						return streamBean;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<ClassBean> getAllClassesByStreamId(int streamId) {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<ClassBean> classBeanList = session.createQuery("from ClassBean where streamId =:providedStreamId")
										.setParameter("providedStreamId", streamId).list();
						tx.commit();
						return classBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}

		@Override
		public List<StreamBean> getAllStreams() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						List<StreamBean> streamBeanList = session.createQuery("from StreamBean").list();
						tx.commit();
						return streamBeanList;
				} catch (HibernateException ex) {
						logger.trace(ex);
				}
				return null;
		}
}
