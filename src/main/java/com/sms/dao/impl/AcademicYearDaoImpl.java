package com.sms.dao.impl;

import com.sms.ExceptionHandlers.SmsDatabaseExceptions;
import com.sms.dao.interfaces.AcademicYearDao;
import com.sms.model.AcademicYearBean;
import com.sms.util.HibernateUtil;
import org.hibernate.*;

import java.util.Date;
import java.util.List;

/**
 * Created by Steve on 1/8/2017.
 */
@SuppressWarnings("unchecked")
public class AcademicYearDaoImpl implements AcademicYearDao {
		private SessionFactory factory = HibernateUtil.getSessionFactory();
		private Session session;

		@Override
		public List<AcademicYearBean> getAllAcademicyears() {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				List<AcademicYearBean> academicYearBeanList = session.createQuery("from AcademicYearBean ").list();
				tx.commit();
				return academicYearBeanList;

		}

		@Override
		public AcademicYearBean createAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						int id = (int) session.save(academicYearBean);
						tx.commit();
						tx = session.beginTransaction();
						AcademicYearBean academicYearBean1 = (AcademicYearBean) session.get(AcademicYearBean.class, id);
						tx.commit();
						return academicYearBean1;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Database Error saving academic year details", ex.getLocalizedMessage());
				}
		}

		@Override
		public boolean deleteAcademicYear(int id) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						AcademicYearBean academicYearBean = (AcademicYearBean) session.get(AcademicYearBean.class, id);
						session.delete(academicYearBean);
						tx.commit();
						return true;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Database Error deleting academic year details", ex.getLocalizedMessage());
				}
		}

		@Override
		public AcademicYearBean updateAcademicYear(AcademicYearBean academicYearBean) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						session.update(academicYearBean);
						tx.commit();
						tx = session.beginTransaction();
						AcademicYearBean academicYearBean1 = (AcademicYearBean) session.get(AcademicYearBean.class, academicYearBean.getId());
						tx.commit();
						return academicYearBean1;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Database Error updating academic year details", ex.getLocalizedMessage());
				}
		}

		@Override
		public AcademicYearBean getAcademicYearById(Integer id) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {
						AcademicYearBean academicYearBean = (AcademicYearBean) session.get(AcademicYearBean.class, id);
						tx.commit();
						return academicYearBean;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Database Error updating fetching year details", ex.getLocalizedMessage());
				}
		}

		@Override
		public List<AcademicYearBean> getAcademicYearsBetweenTwoDates(Date startDate, Date endDate) throws SmsDatabaseExceptions {
				session = factory.openSession();
				Transaction tx = session.beginTransaction();
				try {

						Query search = session.createQuery("from AcademicYearBean where wef between" +
										" :startDate and :endDate " +
										"or wet " +
										"between :startDate and :endDate");
						search.setParameter("startDate", startDate);
						search.setParameter("endDate", endDate);
						List<AcademicYearBean> academicYearBeanList = search.list();
						tx.commit();
						return academicYearBeanList;
				} catch (HibernateException ex) {
						throw new SmsDatabaseExceptions("Database Error running query", ex.getLocalizedMessage());
				}
		}
}
